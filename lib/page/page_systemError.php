<?php
/**
 * Page not found
 *
 * @version 2021-05-13
 *
 * @return null
 *
 */

function page_systemError() {
	echo 'Erreur de système: '. dplu5_err_str();
}