<?php
/**
 *
 * @version 2021-02-12
 *
 * @return null
 *
 */

function page_gallery() {
ob_start();

$id = (isset($_GET['id']) ? $_GET['id'] : 'home');
$fileList =dplu5_file_dir2array(ROOTPATH . 'media', ['fileMask' => '^gallery-' . $id . '-']);

?>
<div class="gallery-slider dplu5-sliderType1">
	<?php foreach ( $fileList as $file ) { ?>
	<div class="gallery-slide display-table position-top-0 position-left-0 width-100p height-100p">
		<div class="display-tableCell verticalAlign-middle text-align-center">
			<div class="dplu5-lightbox-canvas js-fadeIn opacity-0 transition-all-400ms">
				<img src="/media/<?=$file?>">
			</div>
		</div>
	</div>
	<?php } ?>
</div>
<button type="button" class="dplu5-lightbox-close dplu5-lightbox-js-close"></button>
<?php
echo ob_get_clean();
}