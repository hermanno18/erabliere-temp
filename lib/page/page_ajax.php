<?php

/**
 * @version 2022-05-19
 *
 * @return string The HTML
 *
 */

function page_ajax()
{
	if ($_GET['a'] === 'updatePrice') {
		if (isset($_POST['data']) && is_array($_POST['data'])) {
			foreach ($_POST['data'] as $key => $val) {
				dplu5_mysql_simple_update(dbLink(), 'module_price', ['price' => $val], ['name' => $key]);
			}
		}
		dplu5_mysql_simple_update(dbLink(), 'setting', ['value' => date('Y-m-d')], ['name' => 'lastUpdate']);
	}
	if ($_GET['a'] === 'updateOpeningHours') {
		if (isset($_POST['data']) && is_array($_POST['data'])) {
			foreach ($_POST['data'] as $key => $val) {
				dplu5_mysql_simple_update(dbLink(), 'module_schedule', [$key => $val], ['name' => $_GET['s'], 'lng' => dplu5_util_lng()]);
			}
		}
		dplu5_mysql_simple_update(dbLink(), 'setting', ['value' => date('Y-m-d')], ['name' => 'lastUpdate']);
	}
	
	if (isset($_GET['wedding_contact']) && $_GET['wedding_contact'] === 'true') {
		// if (isset($_POST['data']) && is_array($_POST['data'])) {
		// foreach ($_POST['data'] as $key => $val) {
		// 	dplu5_mysql_simple_update(dbLink(), 'module_schedule', [$key => $val], ['name' => $_GET['s'], 'lng' => dplu5_util_lng()]);
		// }
		// Destinataire
		$datas = $_POST['data'];
		$destinataire = "info@erablierecharbonneau.qc.ca";

		// Sujet du message
		$sujet = "Nouvelle soumission de formulaire sur la page Mariage !";

		// Contenu du message
		$message = "
		<!DOCTYPE html>
			<html lang='fr'>
				<head>
					<meta charset='UTF-8'>
					<meta name='viewport' content='width=device-width, initial-scale=1.0'>
					<title>".$sujet."</title>
				</head>
				<body>
					<p><strong>Objet :</strong> ".$sujet."</p>
					<br>
					<p>Bonjour ,</p>
					<br>
					<p> Veuillez trouver ci-dessous mes informations de contact et les détails de l'événement :</p>
					<ul>
						<li><strong>Nom :</strong> ".$datas['nom']."</li>
						<li><strong>Prénom :</strong> ".$datas['prenom']."</li>
						<li><strong>Téléphone :</strong> ".$datas['phone']."</li>
						<li><strong>Courriel :</strong> ".$datas['email']."</li>
						<li><strong>Date de l'événement :</strong> ".$datas['date']."</li>
						<li><strong>Nombre de convives :</strong> ".$datas['nb_invites']."</li>
						<li><strong>Message :</strong> ".$datas['message']."</li>
					</ul>
					<br>
					<p>Merci de me contacter dès que possible pour discuter des détails de ma réservation. Je suis impatient(e) de travailler avec vous pour faire de mon mariage un moment inoubliable.</p>
					<br>
					<p>Cordialement,</p>
					<p>".$datas['prenom']." ".$datas['nom']."</p>
				</body>
			</html>
		";

		// En-têtes
		$headers = "From: expediteur@example.com\r\n";
		$headers .= "Reply-To: expediteur@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		// Envoi de l'e-mail
		
		if (mail($destinataire, $sujet, $message, $headers)) {
			echo json_encode(array(
				"status" => "success",
				"message" => "Message sent with success, Thanks!",
			));
		} else {
			echo json_encode(array(
				"status" => "error",
				"message" => "Ooops! Something went wrong.",
			));
		}
		// }
	}
}
