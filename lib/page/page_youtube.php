<?php
/**
 * Page not found
 *
 * @version 2021-03-11
 *
 * @return null
 *
 */

function page_youtube() {
ob_start();
?>
<div class="youtube-slider dplu5-sliderType1 height-100p">
	<div class="youtube-slide display-table position-top-0 position-left-0 width-100p height-100p">
		<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle text-align-center">
			<div class="dplu5-lightbox-canvas width-100p max-width-1100">
				<div class="frame width-100p">
					<div class="frame-16x9">
						<div class="frame-inner">
							<iframe class="width-100p height-100p" src="https://www.youtube.com/embed/1rPtospD14I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="youtube-slide display-table position-top-0 position-left-0 width-100p height-100p">
		<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle text-align-center">
			<div class="dplu5-lightbox-canvas width-100p max-width-1100">
				<div class="frame width-100p">
					<div class="frame-16x9">
						<div class="frame-inner">
							<iframe class="width-100p height-100p" src="https://www.youtube.com/embed/9hwPU3EcFBc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="youtube-slide display-table position-top-0 position-left-0 width-100p height-100p">
		<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle text-align-center">
			<div class="dplu5-lightbox-canvas width-100p max-width-1100">
				<div class="frame width-100p">
					<div class="frame-16x9">
						<div class="frame-inner">
							<iframe width="560" height="315" src="https://www.youtube.com/embed/3sDgIyiBTKA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<button type="button" class="dplu5-lightbox-close dplu5-lightbox-js-close"></button>
<?php
echo ob_get_clean();
}