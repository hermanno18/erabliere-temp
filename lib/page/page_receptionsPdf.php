<?php
/**
 * @version 2023-05-11
 *
 * @return string The HTML
 *
 */

require ROOTPATH . 'vendor/autoload.php';

use Dompdf\Dompdf;

use function mod_pageMeta_get as meta;

function page_receptionsPdf() {

	ob_start();
	?>
	<!DOCTYPE html>
	<html>
		<head>
		<?=tpl_head()?>
		<title><?=meta('receptions', 'title')?></title>
		<style>
			.markdown li {
				list-style-type: disc;
			}
			.markdown h2 { color: #000 }
			body { background-color: #fff }
			.markdown li:before {
				display: none;
			}
			.background-color-red {
				background-color: #000;
			}
		</style>
		</head>
		<body class="background-color-white color-black">
			<div id="pdf-content" class="main position-relative b1-position-left-0 b1-margin-top-70 b1-margin-left-0 transition-all-400ms">
				<?=tpl_receptions_contentPdf()?>
			</div>
		</body>
	</html>
	<?php

	$output = dplu5_util_minifiyHtml(ob_get_clean());

	//echo $output;

	// instantiate and use the dompdf class
	$dompdf = new Dompdf();
	$dompdf->loadHtml($output);
	$dompdf->render();
	$dompdf->stream('menu-reception.pdf',array('Attachment'=>0));
}