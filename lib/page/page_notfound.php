<?php
/**
 * Page not found
 *
 * @version 2021-03-11
 *
 * @return null
 *
 */

function page_notfound() {
	dplu5_url_redirect301(getSetting('system', 'defaultPage'));
}