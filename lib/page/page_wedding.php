<?php
/**
 * @version 2021-03-09
 *
 * @return string The HTML
 *
 */


use function mod_pageMeta_get as meta;

function page_wedding() {

$cacheFile = ROOTPATH . 'cache/' . dplu5_util_lng() . '/' . getPageName() . '.html';
$cacheOutdated = filemtime($cacheFile) < strtotime(getSetting('system', 'lastUpdate') . ' 23:59:59');

if ( !$_SESSION['isLoggedIn'] ) {
	if ( file_exists($cacheFile) && !$cacheOutdated ) {
		readfile($cacheFile);
		die;
	}
}

ob_start();
?>
<!DOCTYPE html>
<html lang="<?=dplu5_util_lng()?>">
     <head>
		<?=tpl_head()?>
		<meta name="description" content="<?=meta('wedding', 'description')?>">
		<title><?=meta('wedding', 'title')?></title>
	</head>
	<body>
		<main class="main position-relative b1-position-left-0 margin-left-300 b1-margin-top-70 b1-margin-left-0 transition-all-400ms">
			<?=tpl_pageTitle('wedding')?>
			<?=tpl_wedding_content()?>
			<?=tpl_footer()?>
			<?=tpl_nav_mobile()?>
			<div class="js-toggle-nav overlay position-fixed position-top-0 position-left-0 visibility-hidden opacity-0 width-100p height-100p background-color-black-90 transition-all-400ms"></div>
		</main>
		<?=tpl_nav_primary()?>
		<?=tpl_nav_secondary()?>
		<div class="dplu5-lightbox dplu5-lightbox-js-close background-color-black-90 transition-all-400ms"></div>
		<?=tpl_scripts()?>
    </body>
</html>
<?php

$output = dplu5_util_minifiyHtml(ob_get_clean());

if ( !$_SESSION['isLoggedIn'] ) {
	file_put_contents($cacheFile, $output);
}

echo $output;
}