<?php
/**
 * Get a setting value
 *
 * @version 2020-09-14
 *
 * @uses dplu5_mysql_link
 *
 * @param $cat The category of the setting
 * @param $name The name of the setting
 *
 * @return string
 *
 */

function getSetting($cat, $name) {

	static $cache = [];

	if ( !isset($cache[$cat][$name]) ) {
		$cache[$cat][$name] = dplu5_mysql_simple_selectOne(dbLink(), 'setting', ['cat' => $cat, 'name' => $name])['value'];
	}

	return $cache[$cat][$name];
}