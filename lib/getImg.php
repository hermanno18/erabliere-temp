<?php
/**
 * Get the correct image for the screen with
 *
 * @version 2020-12-03
 *
 * @uses dplu5_mysql_link
 *
 * @param $url The url of the image
 *
 * @return string
 *
 */

function getImg($url) {

	static $cache = [];

	if ( !isset($cache[$url]) ) {

		$mobileDetect = new Mobile_Detect();

		if ( $mobileDetect->isMobile() && !$mobileDetect->isTablet() ) {
			$suffix = 'low';
		} else if ( $mobileDetect->isTablet() ) {
			$suffix = 'med';
		} else {
			$suffix = 'high';
		}

		$filePath = parse_url($url)['path'];
		$filePathParts = explode('/', $filePath);
		$fileName = end($filePathParts);
		$fileDir = str_replace($fileName, '', $filePath);
		$fileExt = dplu5_str_lastPart($fileName, '.');
		$fileNameNoExt = str_replace('.' . $fileExt, '', $fileName);

		if ( file_exists(rtrim(ROOTPATH, '/') . $fileDir . $fileNameNoExt . '-' . $suffix . '.' . $fileExt) ) {
			$cache[$url] = $fileDir . $fileNameNoExt . '-' . $suffix . '.' . $fileExt;
		} else {
			$cache[$url] = $fileDir . $fileName;
		}


	}

	return $cache[$url];
}