<?php
/**
 * Return the page name
 *
 * @version 2021-02-15
 *
 * @return string
 *
 */

function getPageName() {

	static $pageName = null;

	if ( $pageName === null ) {

		$pageUrlName = dplu5_str_lastPart(dplu5_url_current_path(), '/');

		if ( file_exists(ROOTPATH . 'lib/page/page_' . dplu5_str_hyphen2camel($pageUrlName) . '.php') ) {
			$pageName = $pageUrlName;
		} elseif ( file_exists(ROOTPATH . 'lib/page/page_' . getSetting('pageAlias', $pageUrlName) . '.php') ) {

			$pageName = getSetting('pageAlias', $pageUrlName);

		} else {
			$pageName = 'notfound';
		}
	}
	return $pageName;
}