<?php
/**
 * Execute shortcodes
 *
 * @version 2021-03-03
 *
 * @param string $str The string
 *
 * @return string The HTML source
 *
 */

function shortcode($str) {

	$allowedFunctions = ['mod_price_get','getSetting','seasonDate'];

	$output = $str;

	preg_match_all('/\[[a-zA-Z0-9\_]+\|?[^\]]*\]/', $str, $matches);

	foreach ($matches[0] as $match) {
		$shortcode = str_replace(['[', ']'], '', $match);

		$shortcodeParts = explode('|', $shortcode);
		$funcName = $shortcodeParts[0];

		if ( function_exists($funcName) && in_array($funcName, $allowedFunctions) ) {

			array_shift($shortcodeParts);

			$shortcodeOutput = call_user_func_array($funcName, $shortcodeParts);

			$output = str_replace('[' . $shortcode . ']', $shortcodeOutput, $output);
		}
	}

	return $output;
}