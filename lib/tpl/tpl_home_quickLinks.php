<?php
/**
 * @version 2021-03-11
 *
 * @return string The HTML source
 *
 */

use function mod_image_get as img;
use function mod_link_get as link;
use function mod_text_get as txt;

function tpl_home_quickLinks() {

ob_start();
?>
<section class="background-custom-1 padding-bottom-15 padding-left-right-15 text-align-center">
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<?php $links = [1 => 'menus', 2 => 'online-store', 3 => 'receptions'] ?>
			<?php for ( $i = 1; $i <= 3; $i++ ) { ?>
			<div class="display-inlineBlock width-33p b3-width-100p padding-top-15 padding-left-right-15">
				<a class="frame width-100p max-width-350 background-color-beige" href="<?=link($links[$i])['href']?>" target="<?=link($links[$i])['target']?>">
					<div class="frame-1x1 b3-padding-top-0">
						<div class="frame-inner b3-position-relative background-custom-3 padding-20r">
							<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
								<div class="display-inlineTable width-100p height-100p">
									<div class="display-tableRow b3-display-tableCell b3-width-33p b3-verticalAlign-middle b3-text-align-left">
										<div class="display-tableCell b3-display-inlineBlock b3-padding-top-bottom-15 padding-left-right-15 verticalAlign-middle">
											<img class="width-69p height-auto b3-width-100p" src="<?=img('quickLink_' . $i . '_bg', 'src')?>">
										</div>
									</div>
									<div class="display-tableRow b3-display-tableCell b3-width-66p b3-verticalAlign-middle">
										<div class="display-tableCell b3-display-inlineBlock padding-bottom-15 b3-padding-bottom-0 verticalAlign-bottom text-align-center b3-text-align-left">
											<div class="font-size-large font-family-mralex color-red"><?=txt('quickLink_' . $i . '_title')?></div>
											<p class="display-inlineBlock max-width-200 line-height-1dot25 color-black"><?=txt('quickLink_' . $i . '_txt')?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}