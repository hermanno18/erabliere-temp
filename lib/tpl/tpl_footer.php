<?php
/**
 * Footer template
 *
 * @version 2021-04-21
 *
 * @return string The HTML source
 *
 */

use function mod_contact_get as ctc;
use function mod_link_get as lnk;
use function mod_text_get as txt;
use function dplu5_mysql_simple_select as mysql_select;

function tpl_footer() {

ob_start();
?>
<footer class="print-display-none background-color-darkgrey">
	<div class="display-table width-100p">
		<div class="display-tableCell padding-top-40r padding-bottom-40r text-align-center">
			<div class="display-inlineBlock width-100p b2-width-auto max-width-1100 min-height-170">
				<div class="block position-relative width-50p b2-width-auto padding-top-40r b2-text-align-center">
					<div class="text-align-left b4-text-align-center">
						<div class="display-inlineBlock">
							<div class="display-inlineBlock b1-display-none position-relative verticalAlign-bottom" style="width: 105px;">
								<img class="position-absolute position-bottom-0" src="/img/logo-erabliere-2.png" alt="<?=txt('img_logo_erabliere_1')?>">
							</div>
							<div class="display-inlineBlock padding-left-right-15">
								<div class="line-height-1dot15 font-size-large font-family-mralex color-red">
									<?=str_replace([' ','inc.'], ['<br>',''], ctc('Érablière Charbonneau inc.')['name'])?>
								</div>
								<div class="padding-top-20r line-height-1dot25 font-size-small">
									<?=ctc('Érablière Charbonneau inc.')['phone_1']?><br>
									<?=ctc('Érablière Charbonneau inc.')['email_1']?><br>
									<?=ctc('Érablière Charbonneau inc.')['address_1']?>, <?=ctc('Érablière Charbonneau inc.')['city']?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="display-none b2-display-inlineBlock width-33p b2-width-auto b4-width-100p padding-top-40r padding-left-right-15 text-align-left b4-text-align-center">
					<?=tpl_social()?>
				</div>
				<div class="display-inlineBlock width-50p b2-width-100p text-align-right b2-text-align-center">
					<nav class="display-inlineBlock width-100p b2-width-auto">
						<div class="display-inlineBlock width-33p b2-width-auto padding-top-40r padding-left-right-15 text-align-left">
							<div class="display-inlineBlock min-width-92">
								<?php foreach ( mysql_select(dbLink(), 'module_link', [], ['tag' => 'nav_primary', 'lng' => dplu5_util_lng()], 'pos', 'asc', '4') as $elem ) { ?>
								<a class="display-block" href="<?=$elem['href']?>" target="<?=$elem['target']?>"><?=$elem['label']?></a>
								<?php } ?>
							</div>
						</div>
						<div class="display-inlineBlock width-33p b2-width-auto padding-top-40r padding-left-right-15 text-align-left">
							<div class="display-inlineBlock min-width-92">
								<?php foreach ( mysql_select(dbLink(), 'module_link', [], ['tag' => 'nav_primary', 'lng' => dplu5_util_lng()], 'pos', 'asc', '4,3') as $elem ) { ?>
								<a class="display-block" href="<?=$elem['href']?>" target="<?=$elem['target']?>"><?=$elem['label']?></a>
								<?php } ?>
								<a class="display-block" href="<?=lnk(getPageName(), otherLng())['href']?>"><?=(otherLng() === 'en' ? 'English' : 'Français')?></a>
							</div>
						</div>
						<div class="display-inlineBlock b2-display-none width-33p b4-width-100p padding-top-40r padding-left-right-15 text-align-left">
							<?=tpl_social()?>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="height-10 background-image-checkered background-repeat-repeat" style="background-position: -40px center"></div>
	<div class="padding-bottom-15 text-transform-uppercase font-size-tiny font-family-sans-serif color-grey">
		<div class="display-inlineBlock width-50p b3-width-100p padding-top-15 padding-left-right-15 b3-text-align-center">
			<div class="display-inlineBlock">
				&copy; <?=date('Y')?> <?=ctc('Érablière Charbonneau inc.')['name']?> <?=txt('footer_txt_1')?>
			</div>
		</div>
		<div class="display-inlineBlock width-50p b3-width-100p padding-top-15 padding-left-right-15 text-align-right b3-text-align-center">
			<div class="display-inlineBlock">
				<?=txt('footer_txt_2')?> <a class="position-relative" href="https://design.domaineplus.com" target="_blank" style="min-width: 112px"><img class="position-absolute" src="/img/dpLogo-white.png" style="opacity: .5; left: 0; bottom: -4px"></a>
			</div>
		</div>
		<div class="padding-top-15 padding-left-right-15 b3-text-align-center"><?=txt('footer_txt_3')?></div>
	</div>
</footer>
<?php
return ob_get_clean();
}