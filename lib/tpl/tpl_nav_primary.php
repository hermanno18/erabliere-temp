<?php
/**
 * Primary navigation template
 *
 * @version 2021-02-16
 *
 * @return string The HTML
 *
 */

use function mod_contact_get as ctc;
use function mod_link_get as lnk;
use function mod_image_get as img;

function tpl_nav_primary() {
?>
<div class="nav-primary print-display-none position-fixed position-top-0 position-left-0 b1-position-left-minus300 box-shadow-1 width-300 height-100p background-image-checkered background-repeat-repeat overflow-y-auto transition-all-400ms">
	<div class="position-relative box-shadow-1 height-20 background-image-checkered background-repeat-repeat background-position-minus50" style="z-index: 3"></div>
	<div class="background-color-black">
		<div class="position-relative" style="z-index: 2">
			<div class="display-inlineBlock width-100p padding-right-15 text-align-right">
				<a class="ribbon ribbon-bottom margin-left-10 min-width-30 min-height-40 background-color-beige color-red" href="<?=lnk(getPageName(), otherLng())['href']?>">
					<div class="ribbon-inner">
						<div class="display-inlineBlock height-20 text-transform-uppercase line-height-1 font-size-20 font-family-mralex"><?=otherLng()?></div>
					</div>
				</a>
				<div class="ribbon ribbon-bottom margin-left-10 min-width-30 min-height-40 background-color-red">
					<div class="ribbon-inner">
						<a class="display-inlineBlock width-20 height-20" href="<?=ctc('Érablière Charbonneau inc.')['facebook_url']?>" target="_blank">
							<?php readfile(ROOTPATH . 'img/icon/icon-facebook.svg')?>
						</a>
					</div>
				</div>
				<div class="ribbon ribbon-bottom margin-left-10 min-width-30 min-height-40 background-color-red">
					<div class="ribbon-inner">
						<a class="display-inlineBlock width-20 height-20" href="<?=ctc('Érablière Charbonneau inc.')['instagram_url']?>" target="_blank">
							<?php readfile(ROOTPATH . 'img/icon/icon-instagram.svg')?>
						</a>
					</div>
				</div>
			</div>
			<div class="display-none b1-display-inlineTable position-absolute position-top-0 position-left-0 height-100p">
				<div class="display-tableCell verticalAlign-middle">
					<button type="button" class="btn-times js-toggle-nav color-red">
						<?php readfile(ROOTPATH . 'img/icon/times.svg')?>
					</button>
				</div>
			</div>
		</div>
		<div class="nav-primary-logo padding-top-40 background-color-1 text-align-center">
			<a class="display-inlineBlock" href="<?=mod_link_get('home')['href']?>"><img class="b1-display-none width-250 height-auto" src="<?=img('logo-erabliere-1', 'src')?>" alt="<?=img('logo-erabliere-1', 'alt')?>"></a>
		</div>
		<nav class="nav-primary-nav padding-bottom-40 text-align-center">
			<ul>
				<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_link', [], ['tag' => 'nav_primary', 'lng' => dplu5_util_lng()], 'pos') as $elem ) { ?>
				<li class="padding-top-30">
					<a class="btn-1 <?=( $_SERVER['REQUEST_URI'] === $elem['href'] ) ? ' btn-1-active' : ''?>" href="<?=$elem['href']?>" target="<?=$elem['target']?>"><?=$elem['label']?></a></li>
				<?php } ?>
			</ul>
		</nav>
	</div>
</div>
<?php
}