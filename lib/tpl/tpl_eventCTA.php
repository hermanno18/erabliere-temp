<?php
/**
 * @version 2021-04-21
 *
 * @return string The HTML source
 *
 */

use function mod_image_get as img;
use function mod_link_get as lnk;
use function mod_text_get as txt;

function tpl_eventCTA() {

ob_start();
?>
<section class="print-display-none padding-top-60r padding-bottom-60r background-cover-center text-align-center" style="background-image: url(<?=img('receptions_bg','src')?>)">
	<div class="display-inlineBlock width-100p max-width-700 padding-top-40r padding-bottom-20r padding-left-right-20r background-color-black-75 text-align-center">
		<div class="line-height-1 font-size-extraLarge font-family-mralex">
			<?=txt('receptions_title')?>
		</div>
		<div>
			<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
				<?=txt('receptions_txt')?>
			</div>
		</div>
		<div class="padding-top-30r">
			<a class="btn-2 outline-color-beige border-color-red background-color-beige font-size-extraLarge color-red" href="<?=lnk('receptions')['href']?>">
				<?=txt('receptions_btn')?>
			</a>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}