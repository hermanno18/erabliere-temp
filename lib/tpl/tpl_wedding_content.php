<?php

/**
 * @version 2021-04-21
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_link_get as lnk;
use function mod_text_get as txt;
use function mod_image_get as img;

function tpl_wedding_content()
{

	ob_start();
?>
	<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center color-beige">
		<!-- <div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div> -->
		<div class="display-inlineBlock width-100p max-width-1250">
			<div class="margin-left-right-minus15 display-flex m-flex-col">
				<div class="display-inlineBlock width-50p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
					<div class="display-inlineBlock height-100p">
						<img src="<?= img('wedding_main_img', 'src') ?>" alt="<?= img('wedding_main_img', 'alt') ?>" srcset="" class=" height-100p object-cover" style="width:100%">
					</div>
				</div>
				<div class="display-inlineBlock width-50p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
					<div class="font-size-medium margin-bottom-15r font-family-mralex display-flex items-center m-justify-center" style="gap: 7.5px;">
						<div class="width-26 height-26">
							<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
						</div>
						<span class="text-underline">
							<?= txt('wedding_sub_title') ?>
						</span>
					</div>
					<div class="font-size-40r font-family-mralex color-red">
						<?= txt('wedding_title') ?>
					</div>
					<div class="padding-top-1em text-align-left  font-size-medium">
						<?= md(txt('wedding_text_1')) ?>
					</div>
					<div class="margin-top-30r">
						<a href="#form-contact" class="border border-width-1 color-beige border-color-beige btn-action-wedding font-family-mralex"><?= txt('wedding_btn_1') ?> </a>
					</div>
				</div>
			</div>
		</div>
		<div class="display-inlineBlock width-100p max-width-1250 text-align-left padding-top-60r">
			<div class="font-size-medium margin-bottom-15r font-family-mralex display-flex items-center  padding-top-40r m-justify-center" style="gap: 7.5px;">
				<div class="width-26 height-26">
					<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
				</div>
				<span class="text-underline">
					<?= txt('wedding_menus_sub_title') ?>
				</span>
			</div>
			<div class="font-size-40r margin-bottom-20r font-family-mralex color-red m-text-center">
				<?= txt('wedding_menus_title') ?>
			</div>
			<div class="Wedding-menus-tab">
				<div class="nav font-size-40r font-family-mralex text-align-center margin-bottom-50r">
					<button class="item padding-15r color-darkgrey" data-slide="1">
						<?= nl2br(txt('wedding_menus_nav_1')) ?>
					</button>
					<button class="item padding-15r background-color-darkgrey" data-slide="2">
						<?= nl2br(txt('wedding_menus_nav_2')) ?>

					</button>
					<button class="item padding-15r background-color-darkgrey" data-slide="3">
						<?= nl2br(txt('wedding_menus_nav_3')) ?>
					</button>
				</div>
				<div class="slides">
					<div class="wedding-menu-slide">
						<div>
							<h3 class="font-size-32r font-family-mralex  color-red"> <?= txt('wedding_menus_nav_1_price_title') ?></h3>
							<ul class="menu-description">
								<li class="display-flex items-center justify-content-between dots-between">
									<div class="text-uppercase"><span><?= txt('wedding_menus_nav_1_price_text_1') ?></span></div>
									<div><span><?= txt('wedding_menus_nav_1_price_1') ?></span></div>
								</li>
								<li class="display-flex items-center justify-content-between dots-between">
									<div class="text-uppercase"><span><?= txt('wedding_menus_nav_1_price_text_2') ?></span></div>
									<div><span><?= txt('wedding_menus_nav_1_price_2') ?></span></div>
								</li>
							</ul>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"><?= txt('wedding_menus_nav_1_hot_title') ?></span>
								<span class="margin-left-5"><?= txt('wedding_menus_nav_1_hot_subtitle') ?></span>
							</h3>
							<div class="highlight-strong menu-description">
								<?= md(txt('wedding_menus_nav_1_hot_text')) ?>
							</div>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"><?= txt('wedding_menus_nav_1_cold_title') ?></span>
								<span class="margin-left-5"><?= txt('wedding_menus_nav_1_cold_subtitle') ?></span>
							</h3>
							<div class="highlight-strong menu-description">
								<?= md(txt('wedding_menus_nav_1_cold_text')) ?>
							</div>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"> <?= txt('wedding_menus_nav_1_granite_title') ?> </span>
								<span class="margin-left-5"> <?= txt('wedding_menus_nav_1_granite_subtitle') ?></span>
							</h3>
							<div class="menu-description granite padding-top-10r">
								<?= md(txt('wedding_menus_nav_1_granite_text')) ?>
							</div>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"> <?= txt('wedding_menus_nav_1_mainmeal_title') ?></span>
								<span class="margin-left-5"><?= txt('wedding_menus_nav_1_mainmeal_subtitle') ?></span>
							</h3>
							<div class="highlight-strong menu-description">
								<?= md(txt('wedding_menus_nav_1_mainmeal_text')) ?>
							</div>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"><?= txt('wedding_menus_nav_1_accompagniment_title') ?></span>
								<span class="margin-left-5"><?= txt('wedding_menus_nav_1_accompagniment_subtitle') ?></span>
							</h3>
							<div class="highlight-strong menu-description">
								<?= md(txt('wedding_menus_nav_1_accompagniment_text')) ?>
							</div>
						</div>
						<div class="margin-top-40r">
							<h3 class="margin-bottom-15r">
								<span class="font-size-32r font-family-mralex  color-red"><?= txt('wedding_menus_nav_1_dessert_title') ?></span>
								<span class="margin-left-5"><?= txt('wedding_menus_nav_1_dessert_subtitle') ?> </span>
							</h3>
							<div class="highlight-strong menu-description">
								<?= md(txt('wedding_menus_nav_1_dessert_text')) ?>
							</div>
						</div>
					</div>
					<div class="wedding-menu-slide slide-with-flex">
						<?php for ($i = 1; $i < 6; $i++) {  ?>
							<div class="<?= $i == 1 ? '' : 'margin-top-40r' ?> ">
								<h3 class="display-flex items-center justify-content-between dots-between margin-bottom-20r">
									<div><span class=" font-size-32r font-family-mralex  color-red"><?= txt('wedding_menus_nav_2_opt_' . $i . '_title') ?> <small><?= txt('wedding_menus_nav_2_opt_' . $i . '_subtitle') ?></small></span></div>
									<div><span class="text-uppercase"><?= txt('wedding_menus_nav_2_opt_' . $i . '_price') ?></span></div>
								</h3>
								<div class="display-flex menu-description">
									<div class="flex-1 highlight-strong"><?= md(txt('wedding_menus_nav_2_opt_' . $i . '_text_1')) ?></div>
									<div class="flex-1 highlight-strong"><?= md(txt('wedding_menus_nav_2_opt_' . $i . '_text_2')) ?></div>
									<div class="flex-1 highlight-strong"><?= md(txt('wedding_menus_nav_2_opt_' . $i . '_text_3')) ?></div>
								</div>
							</div>
						<?php } ?>
						<!-- wedding_menus_nav_2_opt_4_price -->
					</div>

					<div class="wedding-menu-slide">
						<?php foreach (dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'wedding_menus_nav_3_opt_title', 'lng' => dplu5_util_lng()]) as $i => $opt) {
							$i++ ?>
							<div class="<?= $i == 1 ? '' : 'margin-top-40r' ?>">
								<h3 class="">
									<span class="font-size-32r font-family-mralex  color-red"> <?= txt('wedding_menus_nav_3_opt_' . $i . '_title') ?> </span>
									<span class="margin-left-5 highlight-strong inlie-md"><?= md(txt('wedding_menus_nav_3_opt_' . $i . '_subtitle')) ?></span>
								</h3>
								<ul class="menu-description">
									<?php
									// 'lng' => dplu5_util_lng()
									foreach (dplu5_mysql_simple_select(dbLink(), 'module_price', [], ['tag' => 'wedding_menus_nav_3_opt_' . $i]) as $key => $elem) {
									?>
										<li class="display-flex items-center justify-content-between dots-between">
											<div class="highlight-strong"><span><?= md($elem['title']) ?></span></div>
											<div><span><?= $elem['price'] ?> $</span></div>
										</li>
									<?php } ?>
								</ul>
								<div class="highlight-strong margin-top-20r menu-description " style='display: inline'><?= md(txt('wedding_menus_nav_3_opt_' . $i . '_text')) ?></div>
							</div>
						<?php } ?>




						<!-- <div class="margin-top-40r">
							<h3 class="">
								<span class="font-size-32r font-family-mralex  color-red">FONCTIONNEMENT D’UN BAR À COUPON ALCOOL</span>
								<span class="margin-left-5 highlight-strong">Offrir un nombre limité de coupon par invité</span>
							</h3>
							<p class="highlight-strong margin-top-20r menu-description">
								Vous fixé un nombre de coupon par personne que vous voulez offrir à vos invités. Votre invité vient au bar avec son coupon et il peut choisir une consommation de son choix dans la sélection (vin, bière, cidre, cocktail et boisson gazeuse) .
							</p>
						</div>
						<div class="margin-top-40r">
							<h3 class="">
								<span class="font-size-32r font-family-mralex  color-red">ou</span>
								<span class="margin-left-5 highlight-strong">Offrir un bar ouvert à ses invités</span>
							</h3>
							<p class="highlight-strong margin-top-20r menu-description">
								Votre invité vient au bar commander une consommation de son choix dans la sélection (vin, bière, cidre, cocktail et boisson gazeuse). À chaque consommation commandée, un coupon est compté par le serveur. Le décompte des boissons prises sera facturé et réglé en fin de soirée. De cette façon vous ne payez que pour l'alcool réellement consommé. Vous pouvez égalements mettre un montant fixe de coupon, comme par exemple 200 coupons pour la soirée.
							</p>
						</div> -->
					</div>
				</div>
				<div class="margin-top-50r">
					<a href="/media/Carte-des-vins-2024-Erabliere-14X21.5cm.pdf" target="_blank" class="border border-width-1 color-beige border-color-beige btn-action-wedding font-family-mralex display-block width-fit m-margin-auto"> carte des vins</a>
				</div>
			</div>
		</div>
		</div>
	</section>
	<section class=" background-cover-center text-align-center color-beige" style="background-image: url(<?= img('wedding_contact_bg_img', 'src') ?>)">
		<div class="display-inlineBlock width-100p max-width-1250 text-align-left" id="form-contact">
			<div class="display-inlineBlock width-100p max-width-700 padding-top-120r padding-bottom-120r padding-left-right-40r background-color-darkgrey-75 text-align-center">
				<div class="font-size-medium  margin-bottom-15r font-family-mralex display-flex items-center m-justify-center" style="gap: 7.5px;">
					<div class="width-26 height-26">
						<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
					</div>
					<span class="text-underline ">
						<?= txt('wedding_contact_sub_title') ?>
					</span>
				</div>
				<div class="font-size-40r margin-bottom-20r font-family-mralex color-red text-align-left">
					<?= txt('wedding_contact_title') ?>
				</div>
				<form action="" id="wedding-form" class="wedding-contact-form">
					<div class="">
						<input type="text" class="width-100p" name="first_name" id="name" placeholder="<?= txt('wedding_contact_label_name') ?>" required>
					</div>
					<div class="">
						<input type="text" class="width-100p" name="last_name" id="prenom" placeholder="<?= txt('wedding_contact_label_lastname') ?>" required>
					</div>
					<div class="">
						<input type="tel" class="width-100p" name="phone" id="phone" placeholder="<?= txt('wedding_contact_label_phone') ?>" required>
					</div>
					<div class="">
						<input type="email" class="width-100p" name="email" id="email" placeholder="<?= txt('wedding_contact_label_mail') ?>" required>
					</div>
					<div class="">
						<input type="date" class="width-100p" name="date" id="date" placeholder="<?= txt('wedding_contact_label_date') ?>" required>
					</div>
					<div class="">
						<input type="number" class="width-100p" name="nb_invites" id="nb_invites" placeholder="<?= txt('wedding_contact_label_qty') ?>" required>
					</div>
					<div class="field-7 width-100p">
						<textarea id="message" class="width-100p" name="message" rows="4" placeholder="<?= txt('wedding_contact_label_message') ?>"></textarea>
					</div>
					<div class="text-align-left width-100p">
						<button class="border border-width-1 color-beige border-color-beige btn-action-wedding font-family-mralex display-block width-fit m-margin-auto">
							<span> <?= txt('wedding_contact_submit') ?></span>
						</button>
					</div>
				</form>
				<div class="width-100p padding-top-10r">
					<p class="text-align-left padding-top-10r" id="wedding-form-response"></p>
				</div>
			</div>
		</div>
	</section>
	<section class=" padding-top-50r padding-bottom-40r padding-left-right-15 background-cover-center text-align-center color-beige">
		<!-- <section class="padding-top-60r padding-bottom-60r background-cover-center text-align-center" style="background-image: url(<?= img('receptions_bg', 'src') ?>)"> -->
		<div class="display-inlineBlock width-100p max-width-1250 text-align-left">
			<section class="">
				<div class="display-inlineBlock width-100p max-width-1250 text-align-left">
					<div class="display-inlineBlock width-100p padding-top-40r padding-bottom-20r  text-align-center">
						<div class="font-size-medium margin-bottom-15r font-family-mralex display-flex items-center m-justify-center" style="gap: 7.5px;">
							<div class="width-26 height-26">
								<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
							</div>
							<span class="text-underline ">
								<?= txt('wedding_highlight_sub_title') ?>
							</span>
						</div>
						<div class="font-size-40r margin-bottom-20r font-family-mralex color-red  text-align-left m-text-center ">
							<?= txt('wedding_highlight_title') ?>
						</div>
						<div class="points-forts width-100p display-grid grid-cols-5 m-grid-cols-2  gap-perso place-content-center">
							<div class="display-grid place-content-center">
								<img src="/img/icon/romantic-dinner.svg" alt="" class="m-auto margin-bottom-20r">
								<p class="font-weight-bold">
									<?= nl2br(txt('wedding_highlight_1')) ?>
								</p>
							</div>
							<div class="display-grid place-content-center">
								<img src="/img/icon/wedding.svg" alt="" class="m-auto margin-bottom-20r">
								<p class="font-weight-bold">
									<?= nl2br(txt('wedding_highlight_2')) ?>
								</p>
							</div>
							<div class="display-grid place-content-center">
								<img src="/img/icon/long-distance.svg" alt="" class="m-auto margin-bottom-20r">
								<p class="font-weight-bold">
									<?= nl2br(txt('wedding_highlight_3')) ?>
								</p>
							</div>
							<div class="display-grid place-content-center">
								<img src="/img/icon/heart_flower.svg" alt="" class="m-auto margin-bottom-20r">
								<p class="font-weight-bold">
									<?= nl2br(txt('wedding_highlight_4')) ?>
								</p>
							</div>
							<div class="display-grid place-content-center">
								<img src="/img/icon/Layer-5.svg" alt="" class="m-auto margin-bottom-20r">
								<p class="font-weight-bold">
									<?= nl2br(txt('wedding_highlight_5')) ?>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
	<section class="padding-top-0r padding-left-right-15  background-cover-center text-align-center color-beige">
		<div class="display-inlineBlock width-100p max-width-1250">
			<div class="margin-left-right-minus15 display-flex m-flex-col">
				<div class="display-inlineBlock width-50p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
					<div class="font-size-medium margin-bottom-15r font-family-mralex display-flex items-center m-justify-center" style="gap: 7.5px;">
						<div class="width-26 height-26">
							<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
						</div>
						<span class="text-underline">
							<?= txt('wedding_magic_sub_title') ?>
						</span>
					</div>
					<div class="font-size-40r font-family-mralex color-red">
						<?= txt('wedding_magic_title') ?>
					</div>
					<div class="padding-top-1em text-align-left  font-size-medium">
						<p>
							<?= nl2br(txt('wedding_magic_text')) ?>
						</p>
					</div>
					<div class="margin-top-30r">
						<a href="#form-contact" class="border border-width-1 color-beige border-color-beige btn-action-wedding font-family-mralex"><?= txt('wedding_magic_btn') ?></a>
					</div>
				</div>
				<div class="display-inlineBlock width-50p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
					<div class="display-inlineBlock height-100p">
						<img src="<?= img('wedding_magic_img', 'src') ?>" alt="<?= img('wedding_magic_img', 'alt') ?>" srcset="" class=" height-100p object-cover" style="width:100%">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="padding-top-60r padding-bottom-60r padding-left-right-15 background-cover-center text-align-center color-beige">
		<!-- <section class="padding-top-60r padding-bottom-60r background-cover-center text-align-center" style="background-image: url(<?= img('receptions_bg', 'src') ?>)"> -->
		<div class="display-inlineBlock width-100p max-width-1250 text-align-left">
			<section class="">
				<div class="display-inlineBlock width-100p max-width-1250">
					<div class="display-inlineBlock padding-top-40r padding-bottom-20r">
						<div class="font-size-medium   font-family-mralex display-flex items-center margin-bottom-15r m-justify-center" style="gap: 7.5px;">
							<div class="width-26 height-26">
								<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
							</div>
							<span class="text-underline">
								<?= txt('wedding_gallery_sub_title') ?>
							</span>
						</div>
						<div class="font-size-40r font-family-mralex color-red margin-bottom-20r m-text-center">
							<?= txt('wedding_gallery_title') ?>
						</div>
						<div class="wedding-gallery-images ">
							<?php foreach (dplu5_mysql_simple_select(dbLink(), 'module_image', [], ['tag' => 'wedding_gallery', 'lng' => dplu5_util_lng()]) as $key => $elem) {
							?>
								<div class="img-<?= $key + 1 ?>">
									<img src="<?= $elem['src'] ?>" alt="<?= $elem['alt'] ?>" srcset="">
								</div>
							<?php } ?>
						</div>

					</div>
				</div>
			</section>
		</div>
	</section>
	<section class="padding-top-35 padding-left-right-15 padding-bottom-25 text-align-center color-beige background-color-black">
		<div class="display-inlineBlock width-100p max-width-1250 text-align-left">
			<section class="">
				<div class="display-inlineBlock width-100p max-width-1250">
					<div class="display-inlineBlock padding-top-40r padding-bottom-20r ">
						<div class="font-size-medium  margin-bottom-15r font-family-mralex display-flex items-center m-justify-center" style="gap: 7.5px;">
							<div class="width-26 height-26">
								<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg') ?>
							</div>
							<span class="text-underline">
								<?= txt('wedding_testimonial_sub_title') ?>
							</span>
						</div>
						<div class="font-size-40r font-family-mralex color-red margin-bottom-35r m-text-center">
							<?= txt('wedding_testimonial_title') ?>
						</div>

						<div class="testimmonials-slider">
							<?php foreach (dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'wedding_testimonial', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem) { ?>
								<div class="">
									<div class="display-flex items-start">
										<img src="/img/icon/quotes.svg" alt="" class="padding-right-30r">
										<p>
											<?= nl2br(txt('wedding_testimonial_' . ($key + 1) . '_text')) ?>
										</p>
									</div>
									<p class="text-align-right color-red font-family-mralex margin-top-50r">
										<?= txt('wedding_testimonial_' . ($key + 1) . '_author') ?>
									</p>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
<?php
	return ob_get_clean();
}
