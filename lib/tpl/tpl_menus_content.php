<?php //
/**
 * @version 2021-03-12
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_contact_get as ctc;
use function mod_link_get as lnk;
use function mod_image_get as img;
use function mod_text_get as txt;

function tpl_menus_content() {

ob_start();
?>
<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<div class="display-inlineBlock width-66p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock max-width-540">
					<div class="line-height-1dot25 font-size-medium">
						<h1 class="line-height-1dot15 font-size-40r font-family-mralex color-red">
							<?=txt('menus_title')?>
						</h1>
						<div class="padding-top-1em">
							<div class="text-align-left"><?=md(shortcode(txt('menus_text_1')))?></div>
							<div class="margin-top-15 background-color-black padding-top-bottom-30 padding-left-right-30">
								<?=md(txt('menus_highlight_1'))?>
								<a class="display-inlineBlock font-size-40r font-weight-bold color-red" href="tel: <?=ctc('Érablière Charbonneau inc.')['phone_1']?>">
									<?=ctc('Érablière Charbonneau inc.')['phone_1']?>
								</a>
							</div>
						</div>
						<div class="padding-top-1em font-size-large font-family-mralex color-red">
							<?=txt('menus_content_3')?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_4'))?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_5'))?>
						</div>
						<div class="padding-top-1em font-size-large font-family-mralex color-red">
							<?=txt('menus_content_6')?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_7'))?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_8'))?>
						</div>
						<div class="padding-top-1em font-size-large font-family-mralex color-red">
							<?=txt('menus_content_14')?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_15'))?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">

						</div>
						<div class="padding-top-1em font-size-large font-family-mralex color-red">
							<?=txt('menus_content_9')?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_10'))?>
						</div>
						<div class="display-inlineBlock width-50p min-width-200">
							<?=md(txt('menus_content_11'))?>
						</div>
						<div class="padding-top-1em">
							<?=md(txt('menus_content_12'))?>
						</div>
					</div>
					<div class="display-inlineBlock width-100p padding-top-30">
						<div class="background-color-beige">
							<div class="background-custom-3 padding-20r">
								<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
									<div class="display-inlineTable width-100p">
										<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
											<div class="font-size-large font-family-mralex color-red"><?=shortcode(txt('menus_insert_1'))?></div>
											<div class="padding-top-15"><?=shortcode(md(txt('menus_insert_2')))?></div>
											<div class="padding-top-15">
												<div class="display-inlineTable width-100p font-size-medium">
													<?php $menuRows = dplu5_mysql_simple_select(
														dbLink(),
														'module_text',
														[],
														['tag' => 'menus_insert_table_1', 'lng' => dplu5_util_lng()],
														'pos',
														'asc'
													);?>
													<?php for ( $i = 1; $i <= (count($menuRows) / 2); $i++ ) { ?>
													<?php $borderClass = ($i > 1) ? 'border-top-width-1 border-style-solid' : '' ?>
													<div class="display-tableRow">
														<div class="display-tableCell <?=$borderClass?> padding-top-bottom-10 text-align-left" style="border-color: rgba(0,0,0,.1)"><?=md(txt('menus_insert_table_1_row_' . $i . '_cell_1'))?></div>
														<div class="display-tableCell <?=$borderClass?> padding-top-bottom-10 text-align-right font-weight-semibold" style="border-color: rgba(0,0,0,.1)"><?=shortcode(txt('menus_insert_table_1_row_' . $i . '_cell_2'))?></div>
													</div>
													<?php } ?>
												</div>
											</div>
											<div class="padding-top-15">
												<?=md(txt('menus_insert_3'))?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div>
						<div class="noStroller position-relative">
							<div class="noStroller-icon">
								<div class="noStroller-icon-stroller"><?php readfile(ROOTPATH . 'img/icon/stroller.svg')?></div>
								<div class="noStroller-icon-no"><?php readfile(ROOTPATH . 'img/icon/no.svg')?></div>
							</div>
							<div class="noStroller-text">
								<?=md(txt('menus_content_13'))?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="display-inlineBlock width-33p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p max-width-346">
					<button type="button" class="width-100p b2-padding-top-40r">
						<div class="display-inlineTable width-100p">
							<div class="display-tableCell background-cover-center padding-left-right-15 padding-top-bottom-60r verticalAlign-middle text-align-center dplu5-lightbox-js-open-1 js-lazyload" data-page="/fr/gallery?id=menus" style="background-image: url(<?=img('rightSide-menus','src')?>)">
								<div class="line-height-1 font-size-extraLarge font-family-mralex">
									<?=txt('medias_1_title')?>
								</div>
								<div>
									<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
										<?=txt('medias_1_txt')?>
									</div>
								</div>
								<div class="padding-top-20r">
									<div class="display-inlineBlock width-60 height-60">
										<?php readfile(ROOTPATH . 'img/icon/image-gallery.svg')?>
									</div>
								</div>
							</div>
						</div>
					</button>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('contact')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/clock.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_1')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('snackbar')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/burger.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_2')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('activities')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/tractor.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_3')?>
								</div>
							</div>
						</a>
					</div>
<!--					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-red" href="#">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-black">
									<?php readfile(ROOTPATH . 'img/icon/promotions.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_4')?>
								</div>
							</div>
						</a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}