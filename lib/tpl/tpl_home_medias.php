<?php
/**
 * @version 2021-03-11
 *
 * @return string The HTML source
 *
 */

use function mod_image_get as img;
use function mod_text_get as txt;

function tpl_home_medias() {

ob_start();
?>
<section class="padding-bottom-1" style="background-image: url(<?=img('receptions_wedding', 'src')?>)">
	<?php
	$svg = ['image-gallery','online-video', '360-degrees', '360-degrees'];
	for ( $i = 1; $i <= 4; $i++ ) { ?>
	<button type="button" class="width-50p b3-width-100p <?=($i < 3) ? 'dplu5-lightbox-js-open-' . $i : ($i === 3 ? 'js-open360' : 'js-open360-2')?>" data-page="/fr/gallery?id=home">
		<div class="display-inlineTable width-100p">
			<div class="display-tableCell background-cover-center padding-top-230r padding-bottom-130r padding-left-right-15 verticalAlign-middle text-align-center js-lazyload" style="background-image: url(<?=img('medias_' . $i . '_bg', 'src')?>); position: relative">
				<?php if ( $i === 4 ) { ?>
				<div class="" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: rgba(0,0,0,.8);"></div>
				<?php } ?>
				<div class="position-relative line-height-1 font-size-extraLarge font-family-mralex">
					<?=txt('medias_' . $i . '_title')?>
				</div>
				<div class="position-relative">
					<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
						<?=txt('medias_' . $i . '_txt')?>
					</div>
				</div>
				<div class="position-relative padding-top-20r">
					<div class="display-inlineBlock width-60 height-60">
						<?php readfile(ROOTPATH . 'img/icon/' . $svg[$i - 1] . '.svg')?>
					</div>
				</div>
			</div>
		</div>
	</button>
	<?php } ?>
</section>
<?php
return ob_get_clean();
}