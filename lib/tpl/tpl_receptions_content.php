<?php
/**
 * @version 2021-03-15
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_contact_get as ctc;
use function mod_image_get as img;
use function mod_link_get as lnk;
use function mod_text_get as txt;

function tpl_receptions_content() {

ob_start();
?>
<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<div class="display-inlineBlock width-66p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock max-width-540" style="page-break-after: always">
					<div class="line-height-1dot25 font-size-medium">
						<div class="font-size-40r font-family-mralex color-red">
							<?=txt('receptions_title')?>
						</div>
						<div class="padding-top-1em text-align-left font-size-medium">
							<?=shortcode(md(txt('receptions_text_1')))?>
						</div>
						<div class="padding-top-1em">
							<div class="display-inlineBlock width-50p min-width-200">
								<?=md(txt('receptions_text_2'))?>
							</div>
							<div class="display-inlineBlock width-50p min-width-200">
								<?=md(txt('receptions_text_3'))?>
							</div>
						</div>
					</div>
					<div class="padding-top-30" style="page-break-before: always">
						<div class="background-color-beige">
							<div class="background-custom-3 padding-20r">
								<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
									<div class="display-inlineTable width-100p">
										<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
											<div class="font-size-large font-family-mralex color-red"><?=txt('receptions_insert_title')?></div>
											<div class="padding-top-15"><?=md(txt('receptions_insert_text'))?></div>
											<a class="display-inlineBlock padding-top-15 line-height-1 font-size-40r font-weight-bold color-red" href="tel: <?=ctc('Érablière Charbonneau inc.')['phone_1']?>">
												<?=ctc('Érablière Charbonneau inc.')['phone_1']?>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="display-inlineBlock print-display-none width-33p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p max-width-346">
					<button type="button" class="width-100p b2-padding-top-40r">
						<div class="display-inlineTable width-100p">
							<div class="display-tableCell background-cover-center padding-left-right-15 padding-top-bottom-60r verticalAlign-middle text-align-center dplu5-lightbox-js-open-1 js-lazyload" data-page="/fr/gallery?id=receptions" style="background-image: url(<?=img('rightSide-receptions','src')?>)">
								<div class="line-height-1 font-size-extraLarge font-family-mralex">
									<?=txt('medias_1_title')?>
								</div>
								<div>
									<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
										<?=txt('medias_1_txt')?>
									</div>
								</div>
								<div class="padding-top-20r">
									<div class="display-inlineBlock width-60 height-60">
										<?php readfile(ROOTPATH . 'img/icon/image-gallery.svg')?>
									</div>
								</div>
							</div>
						</div>
					</button>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('contact')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/clock.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_1')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('snackbar')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/burger.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_2')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('activities')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/tractor.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_3')?>
								</div>
							</div>
						</a>
					</div>
<!--					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-red" href="<?=lnk('promo')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-black">
									<?php readfile(ROOTPATH . 'img/icon/promotions.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_4')?>
								</div>
							</div>
						</a>
					</div>-->
				</div>
			</div>
			<div class="font-size-40r font-family-mralex color-red padding-top-60r padding-bottom-20r line-height-1" style="page-break-before: always">
				<?=txt('reception_menu_title')?>
			</div>
			<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'reception_menu_title', 'lng' => dplu5_util_lng()], 'pos') as $elem ) { ?>
			<?php if ( $elem['pos'] === '10' || $elem['pos'] === '14' || $elem['pos'] === '16' ) {
				continue;
			}
			?>
			<div class="display-inlineBlock width-33p min-width-300 padding-left-right-15 padding-top-20r">
				<div class="background-color-black background-custom-4">
					<div class="">
						<div class="padding-20r">
							<div class="position-relative border-style-dashed border-width-2 border-color-white line-height-1dot25">
								<div class="padding-left-right-15 padding-top-bottom-15">
									<div class="receptionMenu_title" id="receptionMenu_<?=$elem['pos']?>_title">
										<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_title')))?>
									</div>
									<div class="padding-top-10">
										<div class="display-inlineTable width-100p">
											<div class="display-tableCell width-auto verticalAlign-middle">
												<div class="height-1 background-color-white"></div>
											</div>
											<div class="display-tableCell width-26 height-26">
												<div class="width-26 height-26" style="padding: 5px">
													<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg')?>
												</div>
											</div>
											<div class="display-tableCell width-auto verticalAlign-middle">
												<div class="height-1 background-color-white"></div>
											</div>
										</div>
									</div>
									<div id="receptionMenu_<?=$elem['pos']?>_content" class="display-inlineTable receptionMenu_content">
										<div class="display-tableCell verticalAlign-middle">
											<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_content')))?>
										</div>
									</div>
									<div class="padding-top-10">
										<div class="display-inlineTable width-100p">
											<div class="display-tableCell width-auto verticalAlign-middle">
												<div class="height-1 background-color-white"></div>
											</div>
											<div class="display-tableCell width-26 height-26">
												<div class="width-26 height-26" style="padding: 5px">
													<?php readfile(ROOTPATH . 'img/icon/maple-leaf-white.svg')?>
												</div>
											</div>
											<div class="display-tableCell width-auto verticalAlign-middle">
												<div class="height-1 background-color-white"></div>
											</div>
										</div>
									</div>
									<div id="receptionMenu_<?=$elem['pos']?>_price" class="receptionMenu_price editText-js-open">
										<div class="editText-content">
											<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_price')))?>
										</div>
										<?php if ( $_SESSION['isLoggedIn'] ) { ?>
										<form class="editText position-absolute" method="post">
										<?php $funcArr = getShortcodeFunc(trim(txt('reception_menu_' . $elem['pos'] . '_price'))) ?>
										<?php foreach ( $funcArr as $functions ) { ?>
										<?php foreach ( $functions as $function ) { ?>
											<input type="hidden" name="curr-<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
											<input type="number" name="<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
										<?php } ?>
										<?php } ?>
										</form>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="padding-top-40r">
				<div class="font-size-large font-weight-bold line-height-1">
					<?=txt('receptions_choices_title')?>
				</div>
				<div class="font-size-medium">
					<?=txt('receptions_choices_subtitle')?>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-cover-center" style="background-image: linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(0,0,0,1) 1100px), url(<?=img('receptions_choices_service1','src')?>)">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service1_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-red"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock width-50p b3-width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service1_txt1'))?>
								</div>
								<div class="display-inlineBlock width-50p b3-width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service1_txt2'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-cover-center" style="background-image: linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(0,0,0,1) 1100px), url(<?=img('receptions_choices_service2','src')?>)">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service2_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-red"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock position-relative width-100p padding-left-right-15 text-align-left editText-js-open">
									<div class="editText-content">
										<?=md(shortcode(txt('receptions_choices_service2_txt1')))?>
									</div>
									<?php if ( $_SESSION['isLoggedIn'] ) { ?>
									<form class="editText position-absolute" method="post">
									<?php $funcArr = getShortcodeFunc(trim(txt('receptions_choices_service2_txt1'))) ?>
									<?php foreach ( $funcArr as $functions ) { ?>
									<?php foreach ( $functions as $function ) { ?>
										<input type="hidden" name="curr-<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
										<input type="number" name="<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
									<?php } ?>
									<?php } ?>
									</form>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-cover-center" style="background-image: linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(0,0,0,1) 1100px), url(<?=img('receptions_choices_service3','src')?>)">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service3_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-red"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock width-100p padding-left-right-15 text-align-left">
									<?=md(txt('receptions_choices_service3_txt1'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-cover-center" style="background-image: linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(0,0,0,1) 1100px), url(<?=img('receptions_choices_service4','src')?>)">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service4_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-red"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15 text-align-left line-height-1dot25 font-size-medium">
								<div class="display-inlineBlock width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt1'))?>
								</div>
								<div class="display-inlineBlock width-50p b3-width-100p padding-top-1em padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt2'))?>
								</div>
								<div class="display-inlineBlock width-50p b3-width-100p padding-top-1em padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt3'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-cover-center" style="background-image: linear-gradient(180deg, rgba(255,255,255,0) 0%, rgba(0,0,0,1) 1100px), url(<?=img('receptions_choices_service5','src')?>)">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service5_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-red"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock width-100p b3-width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service5_txt1'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-position-center-top">
					<div class="background-color-black-75 padding-top-bottom-15 padding-left-right-15">
<!--						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_beverages_title')?>
							</div>
						</div>-->
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="padding-left-right-15">
<!--									<div class="text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat1', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>-->
									<div class="line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat2_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat2', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
									<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat3_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat3', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
									<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat4_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat4', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
									<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat8_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat8', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
									<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat5_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat5', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
									<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat7_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat7', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="background-color-beige">
					<div class="background-custom-3 padding-20r">
						<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
							<div class="display-inlineTable width-100p">
								<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
									<div class="font-size-large font-family-mralex color-red"><?=txt('receptions_insert2_title')?></div>
									<div class="padding-top-15"><?=md(shortcode(txt('receptions_insert2_text')))?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="margin-top-30">
		<a class="btn-2 outline-color-beige border-color-red background-color-beige font-size-extraLarge color-red" href="receptions-pdf" target="_blank">Version PDF</a>
	</div>
</section>
<?php
return ob_get_clean();
}