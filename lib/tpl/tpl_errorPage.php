<?php
/**
 * Show a nice error page
 *
 * @version 2020-09-09
 *
 * @return string The HTML
 *
 */

function tpl_errorPage() {
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<?=app_tpl_metas()?>
	<?=app_tpl_stylesheets()?>
	<title></title>
</head>
<body>
	<h2>Problème technique</h2>
	<p>Le système a rencontré un problème technique imprévu et nous en sommes désolé. Nous vous demandons un peu de patience pendant que nous remédions à la situation.</p>
</body>
</html>
<?php
}