<?php
/**
 * @version 2021-03-09
 *
 * @param string $pageName The page name
 *
 * @return string The HTML source
 *
 */

use function mod_image_get as img;
use function mod_text_get as txt;

function tpl_pageTitle($pageName) {

ob_start();
?>
<section class="padding-top-60r padding-bottom-60r background-cover-center text-align-center" style="background-image: url(<?=img('pageTitle_' . $pageName,'src')?>)">
	<div class="line-height-1 font-size-80r font-weight-bold text-shadow-1"><?=txt('pageTitle_' . $pageName)?></div>
</section>
<div class="height-10 background-image-checkered background-repeat-repeat" style="background-position: -40px center"></div>
<?php
return ob_get_clean();
}