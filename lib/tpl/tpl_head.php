<?php
/**
 * html head template
 *
 * @version 2021-03-04
 *
 * @return string The HTML source
 *
 */

function tpl_head() {
ob_start();
echo tpl_metas();
?>
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css2?family=Trirong:wght@300;600;700&display=swap">
<link rel="stylesheet" type="text/css" href="/plugins/slick/slick.css">
<link rel="stylesheet" type="text/css" href="/plugins/dplu5/comp/sliderType1/sliderType1.css">
<link rel="stylesheet" type="text/css" href="/plugins/dplu5/comp/lightbox/lightbox.css">
<style>
<?php readfile(ROOTPATH . 'plugins/dplu5/css/base.css') ?>
<?php readfile(ROOTPATH . 'plugins/dplu5/css/stdClasses.css') ?>
<?php readfile(ROOTPATH . 'css/style.css') ?>
</style>
<?php
return ob_get_clean();
}