<?php
/**
 * @version 2022-05-19
 *
 * @param array The db text element
 *
 * @return string The HTML source
 *
 */

function tpl_receptions_content_priceLine($elem) {

$elemParts = explode('---', $elem['value']);

ob_start();
?>
<div class="padding-top-1em">
	<div class="display-inlineBlock width-50p b4-width-100p font-weight-semibold"><?=trim($elemParts[0])?></div>
	<div class="position-relative display-inlineBlock width-50p b4-width-100p text-align-right b4-text-align-center font-weight-semibold editText-js-open">
		<div class="editText-content">
			<?=shortcode(trim($elemParts[1]))?>
		</div>
		<?php if ( $_SESSION['isLoggedIn'] ) { ?>
		<form class="editText position-absolute" method="post">
		<?php $funcArr = getShortcodeFunc(trim($elemParts[1])) ?>
		<?php foreach ( $funcArr as $functions ) { ?>
		<?php foreach ( $functions as $function ) { ?>
			<input type="hidden" name="curr-<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
			<input type="number" name="<?=$function[0]?>" value="<?=mod_price_get($function[0], true)?>">
		<?php } ?>
		<?php } ?>
		</form>
		<?php } ?>
	</div>
	<div style="padding: 4px 0">
		<div class="height-1 background-color-red"></div>
	</div>
	<div><?=trim($elemParts[2])?></div>
</div>
<?php
return ob_get_clean();
}