<?php
/**
 * @version 2021-02-18
 *
 * @return string The HTML source
 *
 */

use function mod_image_get as img;
use function mod_link_get as lnk;
use function mod_text_get as txt;

function tpl_home_featured() {

ob_start();
?>
<section class="js-slider">
	<div class="block position-relative width-100p min-height-600r background-cover-center padding-top-bottom-15 padding-left-right-15" style="background-image: url(<?=img('slide_1_bg','src')?>)">
		<?php if ( strtotime(getSetting('season', 'end')) > strtotime(date('Y-m-d')) ) { ?>
		<!-- Logos "Ma cabane à la maison" - start -->
		<div class="position-absolute b1-display-none" style="max-width: 260px; left: 0; top: 0;"><img class="width-100p height-auto" src="<?=img('logo-macabane', 'src')?>"></div>
		<div class="position-absolute b1-display-none" style="max-width: 260px; left: 50%; bottom: 0; margin-left: -130px"><img class="width-100p height-auto" src="<?=img('slogan-macabane', 'src')?>"></div>
		<!-- Logos "Ma cabane à la maison" - end -->
		<?php } ?>
		<div>
			<div class="block max-width-700">
				<div>
					<div class="display-none b1-display-inlineBlock width-250r padding-bottom-20r">
						<a class="display-inlineBlock" href="<?=lnk('home')['href']?>"><img class="width-100p height-auto" src="<?=img('logo-erabliere-1', 'src')?>" alt="<?=img('logo-erabliere-1', 'alt')?>"></a>
					</div>
					<div class="position-relative line-height-1 font-size-80r font-weight-bold text-shadow-1"><?=txt('slide_1_txt')?></div>
					<div class="padding-top-50r">
						<a class="btn-2 width-100p max-width-360 outline-color-black border-color-red background-color-black font-size-extraLarge anim-heartbeat" href="<?=lnk('online-store')['href']?>" target="<?=lnk('online-store')['target']?>">
							<?=lnk('online-store')['label']?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}