<?php
/**
 * Application scripts
 *
 * @version 2021-05-13
 *
 * @return string The HTML source
 *
 */

function tpl_scripts() {
ob_start();
?>
<script src="/plugins/jquery/jquery.min.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/jquery/jquery.easing.1.3.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/jquery/jquery.lazy.min.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/slick/slick.min.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/js.cookie/js.cookie.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/dplu5/js/dplu5_adjustImgToWin.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/dplu5/js/dplu5_fadeInImg.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/plugins/dplu5/comp/lightbox/lightbox.js?v=<?=getSetting('system', 'lastUpdate')?>"></script>
<script src="/js/script.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-DLWZC191FB"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-DLWZC191FB');
</script>
<?php
return ob_get_clean();
}