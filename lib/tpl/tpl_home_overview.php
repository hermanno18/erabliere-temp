<?php
/**
 * @version 2021-03-09
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_contact_get as ctc;
use function mod_link_get as lnk;
use function mod_text_get as txt;

function tpl_home_overview() {

ob_start();
?>
<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<div class="display-inlineBlock width-66p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock max-width-540">
					<h1 class="font-size-40r font-family-mralex color-red">
						<?=str_replace(['inc.'], [''], ctc('Érablière Charbonneau inc.')['name'])?>
					</h1>
					<div class="padding-top-1em text-align-left line-height-1dot25 font-size-medium">
						<?=md(txt('section_3_txt_1'))?>
					</div>
					<div class="padding-top-40r">
						<div class="font-size-medium">
							<?=txt('section_3_txt_2')?>
						</div>
						<div class="line-height-1 font-size-40r font-weight-bold color-red">
							<?=ctc('Érablière Charbonneau inc.')['phone_1']?>
						</div>
					</div>
				</div>
			</div>
			<div class="display-inlineBlock width-33p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p max-width-346">
					<div class="b2-padding-top-40r">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('contact')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/clock.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_1')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/burger.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_2')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('activities')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/tractor.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_3')?>
								</div>
							</div>
						</a>
					</div>
<!--					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-red">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-black">
									<?php readfile(ROOTPATH . 'img/icon/promotions.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_4')?>
								</div>
							</div>
						</a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}