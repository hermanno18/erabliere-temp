<?php
/**
 * Application meta
 *
 * @version 2020-12-21
 *
 * @return string The HTML source
 *
 */

function tpl_metas() {
ob_start();
?>
<meta charset="UTF-8">
<meta name=”robots” content=”<?=($_SERVER['HTTP_HOST'] === PRODHOST) ? 'index,follow' : 'noindex,nofollow'?>”>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=yes">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php
return ob_get_clean();
}