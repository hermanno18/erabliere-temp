<?php
/**
 * @version 2022-08-10
 *
 * @param string $schedule The schedule's name
 *
 * @return string The HTML source
 *
 */

use function mod_schedule_get as hr;

function tpl_contact_content_openingHoursTable($schedule) {

$elemParts = explode('---', $elem['value']);

ob_start();
?>
<div class="display-inlineTable width-100p font-size-medium">
	<?php for ( $i = 1; $i <= 7; $i++ ) { ?>
	<?php $borderClass = ($i > 1) ? 'border-top-width-1 border-style-solid' : '' ?>
	<div class="display-tableRow">
		<div class="display-tableCell <?=$borderClass?> padding-top-bottom-10 text-align-left" style="border-color: rgba(0,0,0,.1)"><?=hr($schedule)[$i . '_label']?></div>
		<div class="display-tableCell <?=$borderClass?> padding-top-bottom-10 text-align-right font-weight-semibold editOpeningHours-js-open" style="border-color: rgba(0,0,0,.1)">
			<div class="editOpeningHours-content display-inlineBlock"><?=hr($schedule)[$i . '_value']?></div>
			<?php if ( $_SESSION['isLoggedIn'] ) { ?>
			<form class="editOpeningHours position-absolute" method="post">
				<input type="hidden" name="schedule" value="<?=$schedule?>">
				<input type="text" name="<?=$i?>_value" value="<?=hr($schedule)[$i . '_value']?>">
			</form>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>
<?php
return ob_get_clean();
}