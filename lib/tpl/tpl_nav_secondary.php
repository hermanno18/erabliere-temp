<?php
/**
 * Primary secondary template
 *
 * @version 2021-02-18
 *
 * @return string The HTML
 *
 */

use function mod_contact_get as ctc;
use function mod_link_get as lnk;

function tpl_nav_secondary() {
?>
<nav class="print-display-none position-fixed b1-display-none position-top-20 position-right-0">
	<div>
		<a class="ribbon ribbon-left width-50r height-50r background-color-red" href="<?=lnk('online-store')['href']?>" target="_blank">
			<div class="ribbon-inner background-custom-2">
				<div class="display-inlineBlock width-30r height-30r color-beige">
					<?php readfile(ROOTPATH . 'img/icon/shopping-bag.svg')?>
				</div>
			</div>
		</a>
	</div>
	<div class="padding-top-10">
		<a class="ribbon ribbon-left width-50r height-50r background-color-beige" href="tel: <?=ctc('Érablière Charbonneau inc.')['phone_1']?>">
			<div class="ribbon-inner background-custom-2">
				<div class="display-inlineBlock width-30r height-30r color-red">
					<?php readfile(ROOTPATH . 'img/icon/call.svg')?>
				</div>
			</div>
		</a>
	</div>
</nav>
<?php
}