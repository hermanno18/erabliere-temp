<?php
/**
 * Social icons
 *
 * @version 2021-03-12
 *
 * @return string The HTML source
 *
 */

use function dplu5_str_subStrWords as subStrWords;
use function mod_contact_get as ctc;
use function mod_text_get as txt;

function tpl_social() {
ob_start();
?>
<div class="display-inlineBlock">
	<div class="line-height-1dot15 white-space-nowrap font-size-large font-family-mralex color-red">
		<?=subStrWords(['str' => txt('footer_social_title'), 'begin' => 1, 'end' => 1] )?><br>
		<?=subStrWords(['str' => txt('footer_social_title'), 'begin' => 2] )?>
	</div>
	<div class="padding-top-20">
		<a class="display-inlineBlock width-30 height-30" href="<?=ctc('Érablière Charbonneau inc.')['facebook_url']?>" target="_blank">
			<?php readfile(ROOTPATH . 'img/icon/icon-facebook.svg')?>
		</a>
		<a class="display-inlineBlock margin-left-10 width-30 height-30" href="<?=ctc('Érablière Charbonneau inc.')['instagram_url']?>" target="_blank">
			<?php readfile(ROOTPATH . 'img/icon/icon-instagram.svg')?>
		</a>
	</div>
</div>
<?php
return ob_get_clean();
}