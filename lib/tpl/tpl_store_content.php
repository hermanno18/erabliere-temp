<?php
/**
 * @version 2021-03-10
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_image_get as img;
use function mod_link_get as lnk;
use function mod_text_get as txt;

function tpl_store_content() {

ob_start();
?>
<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<div class="display-inlineBlock width-66p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock max-width-540">
					<div class="line-height-1dot25 font-size-medium">
						<div class="font-size-40r font-family-mralex color-red">
							<?=txt('store_title')?>
						</div>
						<div class="padding-top-1em">
							<?=md(shortcode(txt('store_text_1')))?>
						</div>
					</div>
					<div class="display-inlineBlock width-100p padding-top-30">
						<div class="background-color-beige">
							<div class="background-custom-3 padding-20r">
								<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
									<div class="display-inlineTable width-100p">
										<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
											<div class="font-size-large font-family-mralex color-red"><?=txt('store_insert_title')?></div>
											<div class="padding-top-15"><?=md(txt('store_insert_text'))?></div>
											<div class="padding-top-50r text-align-center">
												<a class="btn-2 width-100p max-width-360 outline-color-black border-color-red background-color-black font-size-extraLarge color-white anim-heartbeat" href="<?=lnk('online-store')['href']?>" target="<?=lnk('online-store')['target']?>">
													<?=lnk('online-store')['label']?>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="display-inlineBlock width-33p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p max-width-346">
					<button type="button" class="width-100p b2-padding-top-40r">
						<div class="display-inlineTable width-100p">
							<div class="display-tableCell background-cover-center padding-left-right-15 padding-top-bottom-60r verticalAlign-middle text-align-center dplu5-lightbox-js-open-1 js-lazyload" data-page="/fr/gallery?id=store" style="background-image: url(<?=img('rightSide-store','src')?>)">
								<div class="line-height-1 font-size-extraLarge font-family-mralex">
									<?=txt('medias_1_title')?>
								</div>
								<div>
									<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
										<?=txt('medias_1_txt')?>
									</div>
								</div>
								<div class="padding-top-20r">
									<div class="display-inlineBlock width-60 height-60">
										<?php readfile(ROOTPATH . 'img/icon/image-gallery.svg')?>
									</div>
								</div>
							</div>
						</div>
					</button>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('contact')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/clock.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_1')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('snackbar')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/burger.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_2')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('activities')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/tractor.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_3')?>
								</div>
							</div>
						</a>
					</div>
<!--					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-red" href="#">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-black">
									<?php readfile(ROOTPATH . 'img/icon/promotions.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_4')?>
								</div>
							</div>
						</a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}