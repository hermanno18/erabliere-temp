<?php
/**
 * @version 2021-04-21
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_contact_get as ctc;
use function mod_image_get as img;
use function mod_link_get as lnk;
use function mod_schedule_get as hr;
use function mod_text_get as txt;

function tpl_contact_content() {

ob_start();
?>
<section class="position-relative padding-top-40r padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain" style="max-width: 299px; max-height: 572px; background-image: url(/img/bg-tree.png); z-index: -1"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="display-inlineBlock margin-left-right-minus15">
			<div class="display-inlineBlock width-66p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p line-height-1dot25 font-size-medium">
					<h1 class="font-size-40r font-family-mralex color-red"><?=txt('contact_content_title')?></h1>
					<div class="margin-left-right-minus15">
					<div class="display-inlineBlock width-100p text-align-center" style="max-width: 600px">
						<div class="display-inlineBlock width-66p b4-width-100p padding-left-right-15 text-align-left">
							<div class="display-inlineBlock text-align-left">
								<div>
									<a class="display-inlineTable margin-top-15" href="<?=ctc('Érablière Charbonneau inc.')['googleMap_url']?>" target="_blank">
										<div class="display-tableCell width-30r">
											<div class="display-inlineBlock width-30r height-30r color-red">
												<?php readfile(ROOTPATH . 'img/icon/marker.svg')?>
											</div>
										</div>
										<div class="display-tableCell padding-left-15 text-align-left">
											<b><?=ctc('Érablière Charbonneau inc.')['name']?></b><br>
											<?=ctc('Érablière Charbonneau inc.')['address_1']?><br>
											<?=ctc('Érablière Charbonneau inc.')['city']?>, <?=ctc('Érablière Charbonneau inc.')['postalCode']?><br>
										</div>
									</a>
								</div>
								<div class="b4-text-align-center">
									<a class="display-inlineTable margin-top-15" href="mailto: <?=ctc('Érablière Charbonneau inc.')['email_1']?>">
										<div class="display-tableCell width-30r">
											<div class="display-inlineBlock width-30r height-30r color-red">
												<?php readfile(ROOTPATH . 'img/icon/email.svg')?>
											</div>
										</div>
										<div class="display-tableCell padding-left-15 text-align-left">
											<?=ctc('Érablière Charbonneau inc.')['email_1']?>
										</div>
									</a>
								</div>
								<div class="b4-text-align-center">
									<div class="display-inlineTable margin-top-15">
										<div class="display-tableCell width-30r">
											<a class="display-inlineBlock width-30r height-30r color-red" href="<?=ctc('Érablière Charbonneau inc.')['facebook_url']?>">
												<?php readfile(ROOTPATH . 'img/icon/icon-facebook.svg')?>
											</a>
										</div>
										<div class="display-tableCell padding-left-15 text-align-left">
											<a href="<?=ctc('Érablière Charbonneau inc.')['facebook_url']?>" target="_blank">Facebook</a>
										</div>
									</div>
								</div>
								<div class="b4-text-align-center">
									<div class="display-inlineTable margin-top-15">
										<div class="display-tableCell width-30r">
											<a class="display-inlineBlock width-30r height-30r color-red" href="<?=ctc('Érablière Charbonneau inc.')['instagram_url']?>">
												<?php readfile(ROOTPATH . 'img/icon/icon-instagram.svg')?>
											</a>
										</div>
										<div class="display-tableCell padding-left-15 text-align-left">
											<a href="<?=ctc('Érablière Charbonneau inc.')['instagram_url']?>" target="_blank">Instagram</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="display-inlineBlock width-33p b4-width-100p padding-left-right-15 text-align-left">
							<div class="display-inlineBlock text-align-left">
								<div>
									<a class="display-inlineTable margin-top-15" href="tel: <?=ctc('Érablière Charbonneau inc.')['phone_1']?>">
										<div class="display-tableCell width-30r">
											<div class="display-inlineBlock width-30r height-30r color-red">
												<?php readfile(ROOTPATH . 'img/icon/call.svg')?>
											</div>
										</div>
										<div class="display-tableCell padding-left-15 text-align-left">
											<b><?=txt('contact_content_phoneLabel')?></b><br>
											<?=ctc('Érablière Charbonneau inc.')['phone_1']?>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="display-inlineBlock width-100p max-width-530 padding-top-30">
					<h1 class="font-size-40r font-family-mralex color-red"><?=txt('contact_hours_title')?></h1>
					<div class="margin-top-15 background-color-beige">
						<div class="background-custom-3 padding-20r">
							<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
								<div class="display-inlineTable width-100p">
									<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
										<div class="font-size-large font-family-mralex color-red"><?=txt('contact_hours_1_title')?></div>
										<div class="padding-top-15 text-align-center">
											<?=tpl_contact_content_openingHoursTable('store')?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="margin-top-15 background-color-beige">
						<div class="background-custom-3 padding-20r">
							<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
								<div class="display-inlineTable width-100p">
									<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
										<div class="font-size-large font-family-mralex color-red"><?=txt('contact_hours_2_title')?></div>
										<div class="padding-top-15 font-size-medium"><?=shortcode(md(txt('contact_hours_2_text1')))?></div>
										<div class="padding-top-15 text-align-center">
											<?=tpl_contact_content_openingHoursTable('sugar')?>
										</div>
										<div class="padding-top-15 font-size-medium"><?=md(txt('contact_hours_2_text2'))?></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="margin-top-15 background-color-beige">
						<div class="background-custom-3 padding-20r">
							<div class="position-relative border-style-dashed border-width-2 border-color-red height-100p">
								<div class="display-inlineTable width-100p">
									<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
										<div class="font-size-large font-family-mralex color-red"><?=txt('contact_hours_3_title')?></div>
										<div class="padding-top-15 font-size-medium"><?=shortcode(md(txt('contact_hours_3_text1')))?></div>
										<div class="padding-top-15 text-align-center">
											<?=tpl_contact_content_openingHoursTable('snack')?>
										</div>
<!--										<div class="padding-top-15 font-size-medium"><?=md(txt('contact_hours_3_text2'))?></div>-->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="print-display-none display-inlineBlock width-33p b2-width-100p padding-top-40r padding-left-right-15 text-align-left b2-text-align-center">
				<div class="display-inlineBlock width-100p max-width-346">
					<a class="width-100p b2-padding-top-40r" href="<?=ctc('Érablière Charbonneau inc.')['googleMap_url']?>" target="_blank">
						<div class="display-inlineTable width-100p">
							<div class="display-tableCell background-cover-center padding-left-right-15 padding-top-bottom-60r verticalAlign-middle text-align-center js-lazyload" style="background-image: url(<?=img('rightSide-contact', 'src')?>)">
								<div class="line-height-1 font-size-extraLarge font-family-mralex">
									<?=txt('googleMap_title')?>
								</div>
								<div>
									<div class="display-inlineBlock max-width-530 padding-top-20r line-height-1dot25 font-size-medium">
										<?=txt('googleMap_text')?>
									</div>
								</div>
								<div class="padding-top-20r">
									<div class="display-inlineBlock width-60 height-60">
										<?php readfile(ROOTPATH . 'img/icon/marker.svg')?>
									</div>
								</div>
							</div>
						</div>
					</a>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="mailto:<?=ctc('Érablière Charbonneau inc.')['email_1']?>?subject=<?=txt('job_mailto_subject')?>&body=<?=txt('job_mailto_body')?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/email.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_5')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('snackbar')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/burger.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_2')?>
								</div>
							</div>
						</a>
					</div>
					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-black" href="<?=lnk('activities')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-red">
									<?php readfile(ROOTPATH . 'img/icon/tractor.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_3')?>
								</div>
							</div>
						</a>
					</div>
<!--					<div class="padding-top-15">
						<a class="display-inlineTable width-100p height-90r background-color-red" href="<?=lnk('promo')['href']?>">
							<div class="display-tableCell width-33p verticalAlign-middle text-align-center">
								<div class="display-inlineBlock width-50r height-50r verticalAlign-middle color-black">
									<?php readfile(ROOTPATH . 'img/icon/promotions.svg')?>
								</div>
							</div>
							<div class="display-tableCell width-66p verticalAlign-middle text-align-left">
								<div class="display-inlineBlock font-size-large font-family-mralex">
									<?=txt('section_3_btn_4')?>
								</div>
							</div>
						</a>
					</div>-->
				</div>
			</div>
		</div>
	</div>
</section>
<?php
return ob_get_clean();
}