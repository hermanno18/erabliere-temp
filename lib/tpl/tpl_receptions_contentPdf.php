<?php
/**
 * @version 2021-03-17
 *
 * @return string The HTML source
 *
 */

use function markdown as md;
use function mod_text_get as txt;

function tpl_receptions_contentPdf() {

$pageChange = [4,8];

ob_start();
?>
<div class="position-relative padding-bottom-80r padding-left-right-15 text-align-center">
	<div class="position-absolute position-left-bottom width-100p height-auto background-position-left-bottom background-size-contain"></div>
	<div class="display-inlineBlock width-100p max-width-1100">
		<div class="margin-left-right-minus15">
			<div class="font-size-40r font-family-mralex padding-top-60r padding-bottom-20r line-height-1">
				<?=txt('reception_menu_title')?>
			</div>
			<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'reception_menu_title', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
			<?php if ( $elem['pos'] === '10' || $elem['pos'] === '14' || $elem['pos'] === '16' ) {
				continue;
			}
			?>
			<?php if ( in_array($key, $pageChange) ) { ?><div style="page-break-before: always"></div><?php } ?>

			<div class="display-inlineBlock width-50p padding-left-right-15 padding-top-20r" style="max-width: 370px">
				<div class="border-style-solid border-width-1">
					<div class="">
						<div>
							<div class="position-relative line-height-1dot25">
								<div class="padding-left-right-15 padding-top-bottom-15">
									<div class="receptionMenu_title" id="receptionMenu_<?=$elem['pos']?>_title">
										<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_title')))?>
									</div>
									<div class="padding-top-10">
										<div class="display-inlineTable width-100p">
											<div class="display-tableCell width-auto padding-bottom-10 verticalAlign-middle">
												<div class="height-1 background-color-black"></div>
											</div>
										</div>
									</div>
									<div id="receptionMenu_<?=$elem['pos']?>_content" class="display-inlineTable receptionMenu_content">
										<div class="display-tableCell verticalAlign-middle">
											<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_content')))?>
										</div>
									</div>
									<div class="padding-top-10">
										<div class="display-inlineTable width-100p">
											<div class="display-tableCell width-auto padding-bottom-10 verticalAlign-middle">
												<div class="height-1 background-color-black"></div>
											</div>
										</div>
									</div>
									<div id="receptionMenu_<?=$elem['pos']?>_price" class="receptionMenu_price editText-js-open">
										<div class="editText-content">
											<?=md(shortcode(txt('reception_menu_' . $elem['pos'] . '_price')))?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
			<div class="padding-top-40r" style="page-break-before: always">
				<div class="font-size-large font-weight-bold line-height-1">
					<?=txt('receptions_choices_title')?>
				</div>
				<div class="font-size-medium">
					<?=txt('receptions_choices_subtitle')?>
				</div>
			</div>
			<div class="padding-top-15 padding-left-right-15">
				<div class="border-style-solid border-width-1 padding-top-bottom-15 padding-left-right-15">
					<div class="padding-top-15">
						<div class="line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_choices_service1_title')?>
						</div>
						<div class="padding-top-15">
							<div class="height-1 background-color-black"></div>
						</div>
					</div>
					<div class="margin-left-right-minus15">
						<div class="padding-top-bottom-15 padding-left-right-15">
							<div class="display-inlineBlock width-50p b3-width-100p padding-left-right-15">
								<?=md(txt('receptions_choices_service1_txt1'))?>
							</div>
							<div class="display-inlineBlock width-50p b3-width-100p padding-left-right-15">
								<?=md(txt('receptions_choices_service1_txt2'))?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service2_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-black"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock position-relative width-100p padding-left-right-15 text-align-left editText-js-open">
									<div class="editText-content">
										<?=md(shortcode(txt('receptions_choices_service2_txt1')))?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15" style="page-break-before: always">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service3_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-black"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock width-100p padding-left-right-15 text-align-left">
									<?=md(txt('receptions_choices_service3_txt1'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service4_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-black"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15 text-align-left line-height-1dot25 font-size-medium">
								<div class="display-inlineBlock width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt1'))?>
								</div>
								<div class="display-inlineBlock width-50p b3-width-100p padding-top-1em padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt2'))?>
								</div>
								<div class="display-inlineBlock width-50p b3-width-100p padding-top-1em padding-left-right-15">
									<?=md(txt('receptions_choices_service4_txt3'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_service5_title')?>
							</div>
							<div class="padding-top-15">
								<div class="height-1 background-color-black"></div>
							</div>
						</div>
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="display-inlineBlock width-100p b3-width-100p padding-left-right-15">
									<?=md(txt('receptions_choices_service5_txt1'))?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-40r" style="page-break-before: always">
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
<!--						<div class="padding-top-15 padding-left-right-15">
							<div class="line-height-1 font-size-40r font-family-mralex">
								<?=txt('receptions_choices_beverages_title')?>
							</div>
						</div>-->
						<div class="margin-left-right-minus15">
							<div class="padding-top-bottom-15 padding-left-right-15">
								<div class="padding-left-right-15">
									<div class="line-height-1 font-size-40r font-family-mralex">
										<?=txt('receptions_beverages_cat2_title')?>
									</div>
									<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
										<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat2', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
										<?=tpl_receptions_content_priceLine($elem)?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-40r" style="page-break-before: always">
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_beverages_cat3_title')?>
						</div>
						<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
							<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat3', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
							<?=tpl_receptions_content_priceLine($elem)?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-40r" style="page-break-before: always">
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_beverages_cat4_title')?>
						</div>
						<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
							<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat4', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
							<?=tpl_receptions_content_priceLine($elem)?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-40r" style="page-break-before: always">
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_beverages_cat8_title')?>
						</div>
						<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
							<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat8', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
							<?=tpl_receptions_content_priceLine($elem)?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_beverages_cat5_title')?>
						</div>
						<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
							<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat5', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
							<?=tpl_receptions_content_priceLine($elem)?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div style="page-break-before: always"></div>
			<div class="padding-top-30 padding-left-right-15">
				<div class="border-style-solid border-width-1">
					<div class="padding-top-bottom-15 padding-left-right-15">
						<div class="padding-top-60r line-height-1 font-size-40r font-family-mralex">
							<?=txt('receptions_beverages_cat7_title')?>
						</div>
						<div class="padding-top-30 text-align-left b4-text-align-center line-height-1dot25 font-size-medium">
							<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_text', [], ['tag' => 'receptions_beverages_cat7', 'lng' => dplu5_util_lng()], 'pos') as $key => $elem ) { ?>
							<?=tpl_receptions_content_priceLine($elem)?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-top-30 ">
				<div class="padding-20r">
					<div class="position-relative border-style-solid border-width-1 border-color-black height-100p">
						<div class="display-inlineTable width-100p">
							<div class="display-tableCell padding-top-bottom-15 padding-left-right-15 verticalAlign-middle line-height-1dot25 color-black">
								<div class="font-size-large font-family-mralex"><?=txt('receptions_insert2_title')?></div>
								<div class="padding-top-15"><?=md(shortcode(txt('receptions_insert2_text')))?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
return ob_get_clean();
}