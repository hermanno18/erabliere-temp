<?php
/**
 * Mobile navigation
 *
 * @version 2021-02-26
 *
 * @return string The HTML
 *
 */

use function mod_contact_get as ctc;
use function mod_link_get as lnk;

function tpl_nav_mobile() {
?>
<nav class="nav-mobile position-fixed position-top-0 position-left-0 display-none b1-display-block box-shadow-1 width-100p background-color-black transition-all-400ms js-hideOnScroll">
	<div class="box-shadow-1 height-20 background-image-checkered background-repeat-repeat background-position-minus50"></div>
	<div class="height-50">
		<div class="display-inlineTable width-100p height-100p">
			<div class="display-tableCell width-60 verticalAlign-middle padding-left-right-15">
				<button type="button" class="btn-hamburger js-toggle-nav">
					<div class="background-color-red"></div>
					<div class="background-color-beige"></div>
					<div class="background-color-red"></div>
				</button>
			</div>
			<div class="display-tableCell verticalAlign-middle padding-left-right-15">
				<span class="font-family-mralex font-size-30r color-white" style="line-height: 50px">
					<?=str_replace(['inc.'], [''], mod_contact_get('Érablière Charbonneau inc.')['name'])?>
				</span>
			</div>
			<div class="display-tableCell verticalAlign-middle text-align-right color-red">
				<div class="display-inlineBlock">
					<a class="display-inlineBlock width-20 height-20" href="<?=lnk('online-store')['href']?>" target="_blank">
						<?php readfile(ROOTPATH . 'img/icon/shopping-bag.svg')?>
					</a>
				</div>
				<div class="display-inlineBlock padding-left-right-15">
					<a class="display-inlineBlock width-20 height-20" href="tel: <?=ctc('Érablière Charbonneau inc.')['phone_1']?>">
						<?php readfile(ROOTPATH . 'img/icon/call.svg')?>
					</a>
				</div>
			</div>
		</div>
	</div>
</nav>
<?php
}