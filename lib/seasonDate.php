<?php
/**
 * Get one of the season date (start or end)
 *
 * @version 2021-03-15
 *
 * @use dplu5_util_lng
 *
 * @param string $dateChoice start or end
 * @param string $format A PHP date format
 *
 * @return string
 *
 */

function seasonDate($dateChoice, $format) {
	return dplu5_format_date(getSetting('season', $dateChoice), $format, dplu5_util_lng() . '_CA');
}