<?php
/**
 * Return the other supported language
 *
 * @version 2021-02-15
 *
 * @return string
 *
 */

function otherLng() {

	static $otherLng = null;

	if ( $otherLng === null ) {
		$currentLng = dplu5_util_lng();
		$otherLng = ($currentLng === 'fr') ? 'en' : 'fr';
	}

	return $otherLng;
}