<?php
/**
 * Link to database
 *
 * Return database link ressource re-usable in all databases functions
 *
 * @version 2020-09-08
 *
 * @uses dplu5_mysql_link
 *
 * @return ressource database link
 *
 */

function dbLink() {
	return dplu5_mysql_link(DB_USR, DB_PWD, DB_NAME);
}