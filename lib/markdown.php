<?php
/**
 * Markdown formating
 *
 * @version 2020-09-10
 *
 * @param string $str The string
 *
 * @return string The HTML source
 *
 */

function markdown($str) {
	return '<div class="markdown">' . Parsedown::instance()->setBreaksEnabled(true)->text($str). '</div>';
}