<?php
/**
 * Get sortcode functions and parametrers
 *
 * @version 2022-05-18
 *
 * @param string $str The string
 *
 * @return array The HTML source
 *
 */

function getShortcodeFunc($str) {

	$allowedFunctions = ['mod_price_get','getSetting','seasonDate'];

	$funcArr = [];

	preg_match_all('/\[[a-zA-Z0-9\_]+\|?[^\]]*\]/', $str, $matches);

	foreach ($matches[0] as $match) {

		$shortcode = str_replace(['[', ']'], '', $match);

		$shortcodeParts = explode('|', $shortcode);
		$funcName = $shortcodeParts[0];

		if ( function_exists($funcName) && in_array($funcName, $allowedFunctions) ) {

			array_shift($shortcodeParts);

			$funcArr[][$funcName] = $shortcodeParts;


		}
	}

	return $funcArr;
}