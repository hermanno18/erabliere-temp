<?php
require '../init.php';
?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="/plugins/dplu5/css/base.css">
	<link rel="stylesheet" type="text/css" href="/plugins/dplu5/css/stdClasses.css">
	<link rel="stylesheet" type="text/css" href="/css/adm.css">
</head>
<body class="font-size-0 font-weight-light text-align-center">
<form>
	<div class="display-inlineBlock width-100p max-width-1140">
		<div class="display-inlineTable width-100p">
			<?php foreach ( dplu5_mysql_simple_select(dbLink(), 'module_price', []) as $elem ) { ?>
			<div class="display-tableRow">
				<div class="display-tableCell border-style-solid border-top-width-1 width-auto text-align-left font-size-small"><?=$elem['title']?></div>
				<div class="display-tableCell border-style-solid border-top-width-1 text-align-right"><input class="max-width-100 text-align-right font-size-medium" type="" name="price[<?=$elem['id']?>]" value="<?=$elem['price']?>"></div>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class=""><input type="tel"></div>
	<div class=""></div>
</form>
</body>
</html>