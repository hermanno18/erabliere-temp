<?php
/**
 * Check the token
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-09-24
 *
 * @package dplu5
 *
 * @category secToken
 *
 * @param string $id Id of the token
 *
 * @return boolean
 */

function dplu5_secToken_isValid($id) {

	if (
		isset( $_SESSION['dplu5_secToken'][$id]['token']['key'] ) &&
		isset( $_SESSION['dplu5_secToken'][$id]['token']['val'] ) &&
		isset($_POST[$_SESSION['dplu5_secToken'][$id]['token']['key']]) &&
		$_SESSION['dplu5_secToken'][$id]['token']['val'] === $_POST[$_SESSION['dplu5_secToken'][$id]['token']['key']] &&
		$_SESSION['dplu5_secToken'][$id]['ip'] === $_SERVER['REMOTE_ADDR'] &&
		$_SESSION['dplu5_secToken'][$id]['user-agent'] === $_SERVER['HTTP_USER_AGENT']
	) {
		return true;
	}
	return false;
}