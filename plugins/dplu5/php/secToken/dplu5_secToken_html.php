<?php
/**
 * Generate security values and display the hidden input for the token
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-09-24
 *
 * @package dplu5
 *
 * @category secToken
 *
 * @param string $id Id of the token
 *
 * @return string HTML source
 */

function dplu5_secToken_html($id) {
	$_SESSION['dplu5_secToken'][$id]['token']['key'] = md5(uniqid(microtime(), true));
	$_SESSION['dplu5_secToken'][$id]['token']['val'] = md5(uniqid(microtime(), true));
	$_SESSION['dplu5_secToken'][$id]['ip'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION['dplu5_secToken'][$id]['user-agent'] = $_SERVER['HTTP_USER_AGENT'];
	return '<input type="hidden" name="' . $_SESSION['dplu5_secToken'][$id]['token']['key'] . '" value="' . $_SESSION['dplu5_secToken'][$id]['token']['val'] . '">';
}
