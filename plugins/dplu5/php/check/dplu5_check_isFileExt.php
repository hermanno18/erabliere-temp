<?php
/**
 * Test if the value end with a particular string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-24)
 * 
 * @todo Better description of the parameters
 * 
 * @package dplu5
 *
 * @category dplu5_val_test
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isFileExt($param) {
	$value = strtolower($param['value']) ?? '';
	$ext = $param['ext'] ?? [];

	if ( $value === '' ) {
		return true;
	}

	foreach ( $ext as $elem ) {

		if ( dplu5_str_endWith($value, $elem) ) {
			return true;
		}
	}

	return false;
}