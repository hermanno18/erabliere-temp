<?php
/**
 * Test if the value is a valid email address
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-26)
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isEmail($param) {
	return (filter_var($param['value'], FILTER_VALIDATE_EMAIL) !== false);
}