<?php
/**
 * Test if a string length is between X and Y characters
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2018-04-26 )
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isRightLength($param) {
	$min = $param['min'] ?? 0;
	$max = $param['max'] ?? 0;
	$str = $param['value'] ?? '';

	if ( $min > 0 && $max > 0 ) {
		return strlen($str) >= $min && strlen($str) <= $max;
	} else if ( $min > 0 && $max == 0  ) {
		return strlen($str) >= $min;
	} else if ( $min == 0 && $max > 0 ) {
		return strlen($str) <= $max;
	} else {
		return false;
	}
}