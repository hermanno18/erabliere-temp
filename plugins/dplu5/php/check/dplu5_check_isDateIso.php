<?php
/**
 * Test if the value is date formated as: YYYY-MM-DD
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-07-03
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isDateIso($param) {
	$parts = explode('-', $param['value']);

	return checkdate($parts[1], $parts[2], $parts[0]);
}