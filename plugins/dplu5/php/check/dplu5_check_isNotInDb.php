<?php
/**
 * Test if the value is not present in a db table
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2019-01-14 )
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isNotInDb($param) {
	return !dplu5_mysql_simple_selectOne($param['link'], $param['table'], $param['conditions'], $param['fields']);
}