<?php
/**
 * Test if the value is a valid IPV4
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-08-20)
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isIPV4($param) {
	return (1 === preg_match_all(dplu5_util_regexPattern('ipv4'), $param['value']));
}