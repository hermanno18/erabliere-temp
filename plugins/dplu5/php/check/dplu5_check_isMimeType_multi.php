<?php
/**
 * Test if a file is required mime type
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2018-04-26 )
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 * @param array $param[acceptedTypes] The accepted mime types
 * @param array $param[files] The filenames
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isMimeType_multi($param) {
	$acceptedMimeTypes = $param['acceptedTypes'] ?? [];
	$files = $param['value'] ?? null;

	if (is_array($files) && !empty($files)) {
		foreach ($files as $elem) {
			if ( file_exists($elem)) {
				if ( !dplu5_val_test_isMimeType(['value' => $elem, 'acceptedTypes' => $acceptedMimeTypes]) ) {
					return false;
				}
			}
		}
	}
	return true;
}