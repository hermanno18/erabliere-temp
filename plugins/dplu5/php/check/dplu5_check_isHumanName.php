<?php
/**
 * Test if the value is a valid human name
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-24)
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isHumanName($param) {
	$value = dplu5_str_removeAccents($param['value']);
	$value = str_replace("'", '', $value);
	return (1 === preg_match(dplu5_util_regexPattern('humanName'), $value));
}