<?php
/**
 * Check if the csv file has no duplicate line
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-26)
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_csvFileHasNoDuplicate($param) {
	$value = $param['value'] ?? '';

	$lines = explode(PHP_EOL, $value);

	foreach ( $lines as $elem ) {
		$csvArray[] = str_getcsv($elem);
	}

	return !dplu5_arr_hasDuplicates($csvArray, 0);
}