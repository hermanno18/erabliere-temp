<?php
/**
 * Test if the value is a valid company name
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-26)
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isCompanyName($param) {
	return (1 === preg_match(dplu5_util_regexPattern('companyName'), $param['value']));
}