<?php
/**
 * Test if a file is required mime type
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2018-04-26 )
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 * @param string $param[file] The filename
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isMimeType($param) {
	$acceptedMimeTypes = $param['acceptedTypes'] ?? [];
	$file = $param['value'] ?? null;

	if (!empty($file) && file_exists($file)) {
		return in_array(dplu5_file_mime($file), $acceptedMimeTypes);
	}
	return true;
}