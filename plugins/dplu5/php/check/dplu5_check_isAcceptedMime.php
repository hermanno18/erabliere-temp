<?php
/**
 * Test if a file is required mime type
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.1 ( 2019-04-23 )
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 * @param string $param[value] The filename
 * @param array $param[acceptedTypes] Range of accepted mime types
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isAcceptedMime($param) {

	$acceptedMimeTypes = isset($param['acceptedTypes']) ? $param['acceptedTypes'] : [];
	$file = isset($param['value']) ? $param['value'] : null;

	if (!empty($file) && file_exists($file)) {

		foreach ( $acceptedMimeTypes as $type ) {
			if ( dplu5_str_containStr(dplu5_file_mime($file), $type) ) {
				return true;
			}
		}
	}
	return false;
}