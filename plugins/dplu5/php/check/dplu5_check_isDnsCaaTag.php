<?php
/**
 * Test if the value is a valid flag for a CAA record
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-12-14)
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isDnsCaaTag($param) {
	return in_array($param['value'], ['issue','issuewild', 'iodef']);
}