<?php
/**
 * Test if the value is a valid TTL
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-08-20)
 *
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isTTL($param) {
	return is_numeric($param['value']) && $param['value'] >= 300 && $param['value'] <= 86400;
}