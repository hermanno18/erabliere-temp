<?php
/**
 * Test if the value is alphanumeric
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isAlphaNum($param) {
	return (1 === preg_match(dplu5_util_regexPattern('alphanumeric'), $param['value']));
}