<?php
/**
 * Test if the value is not present in an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2018-04-26 )
 * 
 * @package dplu5
 *
 * @category check
 *
 * @param array $param Parameters
 *
 * @return boolean The result of the test
 *
 */

function dplu5_check_isNotInList($param) {
	$value = $param['value'] ?? '';
	$list = $param['list'];
	return !in_array($value, $list);
}