<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_file_delete($filepath) {
	if ( ! is_array( $filepath ) ) {
		$filepath = array( $filepath );
	}

	foreach ( $filepath as $value ) {
		if ( file_exists( $value ) ) {
			unlink( $value );
		}
	}
	return true;
}
