<?php
/**
 * Return the files of a directory
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @package dplu5
 *
 * @category file
 * 
 * @version 1.0 (2018-08-01)
 *
 * @param string $dir The full path to the directory
 * @param array $opt The optional parameters
 * @param array $opt[excludes] array List of excluded files
 * @param bool $opt[recursive] Specify if the function must be recursive
 * @param string $opt[fileMask] A regular expression to match wanted files
 * @param string $opt[dirMask] A regular expression to match wanted directories
 * @param string $opt[matchCase] Match the case of the mask
 *
 * @return array file list
 */

function dplu5_file_dir2array($dir, $opt = array()) {
	$out= array();

	// We don't want the current directory and parent directory operators
	$opt['excludes'][] = '.';
	$opt['excludes'][] = '..';

	// The recursive option is set to true by default
	$opt['recursive'] = isset($opt['recursive']) && is_bool($opt['recursive']) ? $opt['recursive'] : true;

	// If the filemask is not set, include all files
	$opt['fileMask'] = isset($opt['fileMask']) && is_string($opt['fileMask']) ?  $opt['fileMask'] : '.*';

	// If the dirmask is not set, include all directories
	$opt['dirMask'] = isset($opt['dirMask']) && is_string($opt['dirMask']) ?  $opt['dirMask'] : '.*';

	$opt['matchCase'] = isset($opt['matchCase']) && is_bool($opt['matchCase']) ?  $opt['matchCase'] : false;

	// Read a directory and return an array
	$files= scandir($dir);

	foreach ( $files as $file ) {
		if ( !in_array($file, $opt['excludes']) ) {
			
			$pregFlag = $opt['matchCase'] ? '' : 'i';
			
			if ( is_dir($dir . DIRECTORY_SEPARATOR . $file) && preg_match( '/' . $opt['dirMask'] . '/' . $pregFlag, $file) === 1 ) {
				if ( $opt['recursive'] ) {
					$out[$file] = dplu5_file_dir2array($dir . DIRECTORY_SEPARATOR . $file, $opt);
				} else {
					$out[] = $file;
				}
			} else {
				if ( preg_match( '/' . $opt['fileMask'] . '/' . $pregFlag, $file) === 1 ) {
					$out[] = $file;
				}
			}
		}
	}
	return $out;
}