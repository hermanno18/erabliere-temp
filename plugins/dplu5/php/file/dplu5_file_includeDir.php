<?php
/**
 * Include all the files in the provided directory
 *
 * This function is recursive
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @package dplu5
 *
 * @category file
 * 
 * @version 1.0.0 (2018-02-21)
 * 
 * @uses dplu5_file_dir2array To get the file list
 *
 * @param string $dir The full path to the directory
 * @param array $opt The optional parameters
 * @param array $opt[excludes] array List of excluded files
 * @param bool $opt[recursive] Specify if the function must be recursive
 * @param string $opt[fileMask]A regular expression to match wanted files
 * @param string $opt[dirMask]A regular expression to match wanted directories
 *
 * @return void
 */

function dplu5_file_includeDir($dir, $opt = array()) {

	// We don't want the current directory and parent directory operators
	$opt['excludes'][] = '.';
	$opt['excludes'][] = '..';

	// The recursive option is set to true by default
	$opt['recursive'] = isset($opt['recursive']) && is_bool($opt['recursive']) ? $opt['recursive'] : true;

	// If the filemask is not set, include all files
	$opt['fileMask'] = isset($opt['fileMask']) && is_string($opt['fileMask']) ? $opt['fileMask'] : '.*';

	// If the dirmask is not set, include all directories
	$opt['dirMask'] = isset($opt['dirMask']) && is_string($opt['dirMask']) ?  $opt['dirMask'] : '.*';

	// Make sure we dosen't re-include this file
	$pathParts = explode('/', __FILE__);
	$thisFile = end($pathParts);

	if ( !in_array($thisFile, $opt['excludes']) ) {
		$opt['excludes'][] = $thisFile;
	}

	// Read a directory and return an array
	$files = dplu5_file_dir2array($dir, array('recursive' => false));

	foreach ( $files as $file ) {
		if ( !in_array($file, $opt['excludes']) ) {
			if ( is_dir($dir . DIRECTORY_SEPARATOR . $file) && preg_match( '/' . $opt['dirMask'] . '/', $file) === 1 ) {
				if ( $opt['recursive'] ) {
					dplu5_file_includeDir($dir . DIRECTORY_SEPARATOR . $file, $opt);
				}
			} else {
				if ( preg_match( '/' . $opt['fileMask'] . '/', $file) === 1 ) {
					require_once $dir . DIRECTORY_SEPARATOR . $file;
				}
			}
		}
	}
}