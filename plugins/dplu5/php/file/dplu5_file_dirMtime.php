<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_file_dirMtime($dir) {

	if ( substr($dir, -1) === '/' ) {
		$dir = substr($dir, 0, -1);
	}

	clearstatcache(true, $dir);
	$mtime = filemtime($dir);

	$files= scandir($dir);

	foreach ( $files as $elem ) {
		if ( $elem !== '.' && $elem !== '..' ) {

			if ( is_dir($dir . '/' . $elem) ) {

				$newMtime = dplu5_file_dirMtime($dir . '/' . $elem);
				if ( $newMtime > $mtime ) {
					$mtime = $newMtime;
				}
			}
		}
	}
	return $mtime;
}
