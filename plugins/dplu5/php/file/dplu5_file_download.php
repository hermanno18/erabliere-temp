<?php
/**
 * Force the download of a file
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @category dplu5
 *
 * @package file
 *
 * @return boolean true on success, false on error
 */

function dplu5_file_download($file) {

	if ( file_exists($file) ) {
        $filename= dplu5_str_lastPart( $file, '/' );
        header('Content-type: ' . dplu5_file_mime( $file ));
		header('Content-Disposition: attachment; filename=' . $filename);
        readfile($file);
		return true;
	}
	trigger_error( 'File does not exist', E_USER_WARNING );
	return false;
}