<?php
define('DPLU5_FILE_MIME_JPEG', 'image/jpeg');
define('DPLU5_FILE_MIME_PDF', 'application/pdf');

function dplu5_file_mime($filename) {
//	$finfo = finfo_open( FILEINFO_MIME_TYPE );
//	return finfo_file($finfo, $filename);

	return mime_content_type($filename);
}
