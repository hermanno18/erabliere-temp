<?php
/**
 * Redirect to a new URL
 * 
 * This is a 302 redirect
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 ( 2018-07-27 )
 *
 * @package dplu5
 * 
 * @category url
 * 
 * @param string $url
 *
 * @return null
 *
 */

function dplu5_url_redirect($url) {
	header('location: ' . $url);
	die();
}