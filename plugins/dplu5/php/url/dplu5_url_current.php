<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.1 (2018-09-19)
 * 
 * @package dplu5
 *
 * @category url
 * 
 * @param array $opt Optional parameters
 * @param boolean $opt['appendQuery'] Append the query string
 * @param boolean $opt['part'] Part of the url to return (base, scheme, host, path)
 * 
 * @return string The url
 */

function dplu5_url_current($opt = array()) {
	
	$opt['appendQuery'] = (isset($opt['appendQuery']) && is_bool($opt['appendQuery'])) ? $opt['appendQuery'] : false;

	$out= '';
	if ( $_SERVER['SERVER_PORT'] === '80' ) {
		$out.= 'http://';
	} elseif ( $_SERVER['SERVER_PORT'] === '443' ) {
		$out.= 'https://';
	} elseif ( $_SERVER['SERVER_PORT'] === '21' ) {
		$out.= 'ftp://';
	}
	$questionmarkpos= strpos( $_SERVER['REQUEST_URI'], '?' );
	if ( $questionmarkpos !== false ) {
		$out .= $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, $questionmarkpos);
	} else {
		$out .= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	if ( isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING']) && $opt['appendQuery']) {
		$out.= '?' . $_SERVER['QUERY_STRING'];
	}
	
	if ( isset($opt['part']) ) {
		if ( $opt['part'] == 'base' ) {
			$urlParts= explode('/',$out);
			$out = $urlParts[0] . '//' . $urlParts[1] . $urlParts[2];
		} elseif ( $opt['part'] == 'scheme' ) {
			$out = parse_url($out, PHP_URL_SCHEME);
		} elseif ( $opt['part'] == 'host' ) {
			$out = parse_url($out, PHP_URL_HOST);
		} elseif ( $opt['part'] == 'path' ) {
			$out = parse_url($out, PHP_URL_PATH);
		}
	}
	return $out;
}