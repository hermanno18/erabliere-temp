<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_url_current_base() {
	static $output = null;

	if ( is_null($output) ) {
		$parts= explode('/', dplu5_url_current());
		$output = $parts[0] . '//' . $parts[1] . $parts[2];
	}

	return $output;
}