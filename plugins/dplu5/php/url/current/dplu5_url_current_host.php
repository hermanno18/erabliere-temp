<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_url_current_host() {
	static $output = null;

	if ( null === $output ) {
		$output = parse_url(dplu5_url_current(), PHP_URL_HOST);
	}
	return $output;
}