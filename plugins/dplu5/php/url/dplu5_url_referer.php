<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

define('DPLU5_URL_FLAG_ALL', 1);
define('DPLU5_URL_FLAG_HOST', 2);
define('DPLU5_URL_FLAG_URI', 3);

function dplu5_url_referer($flag = DPLU5_URL_FLAG_ALL) {
	$url = $_SERVER['HTTP_REFERER'];
	$urlParts = parse_url($url);

	if ( $flag === DPLU5_URL_FLAG_ALL ) {
		return $url;
	}
	if ( $flag === DPLU5_URL_FLAG_HOST ) {
		return $urlParts['scheme'] . '://' . $urlParts['host'];
	}
	if ( $flag === DPLU5_URL_FLAG_URI ) {
		return $urlParts['path'];
	}
}