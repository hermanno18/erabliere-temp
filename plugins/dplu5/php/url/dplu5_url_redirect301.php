<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_url_redirect301($url) {
	header('HTTP/1.1 301 Moved Permanently', true, 301);
	header('location:' . $url);
	die();
}