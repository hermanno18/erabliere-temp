<?php
/**
 * Return a formated price according to a local iso code
 *
 * @version 2021-03-12
 *
 * @param string $number A decimal number
 * @param string $locale A valid locale iso code (eg: fr_CA)
 * @param string $currency The currency
 *
 * @return boolean
 *
 */

function dplu5_format_price($number, $locale, $currency) {
	return numfmt_format_currency(numfmt_create( $locale, NumberFormatter::CURRENCY ), $number, $currency);
}