<?php
/**
 * Return a formated date according to a local setting
 *
 * @version 2021-03-15
 *
 * @param string $date A valid iso date
 * @param string $format The format
 * @param string $locale A valid locale iso code (eg: fr_CA)

 *
 * @return boolean
 *
 */

function dplu5_format_date($date, $format, $locale) {

	setlocale(LC_TIME, $locale . '.UTF-8');

	return strftime($format, strtotime($date));
}