<?php
/**
 * Add a test to the validator
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-26)
 * 
 * @package dplu5
 *
 * @category val
 *
 * @param string $id Value identifier
 * @param string $function The name of the test function
 * @param array $param Parameters of the test function
 *
 * @return null
 *
 */

function dplu5_val_addCheck($id, $function, $param = array()) {

	$data = dplu5_val_data();
	$msg = isset($param['msg']) ? $param['msg'] : null;

	if ( !isset($data[$id]) ) {
		$data[$id]['isValid'] = true;
		$data[$id]['failedTest'] = null;
	}

	$data[$id]['tests'][] = array(
		'function' => $function,
		'param' => $param,
		'msg' => $msg,
	);

	dplu5_val_data($data);
}