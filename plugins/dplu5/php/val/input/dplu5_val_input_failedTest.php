<?php
/**
 * Return the name of the failed test ( function returning false )
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-02-16)
 *
 * @package dplu5
 *
 * @category val
 *
 * @param string $id Input identifier
 *
 * @return string The function name
 *
 */
function dplu5_val_input_failedTest($id) {
	$data = dplu5_val_data();
	return $data[$id]['failedTest'];
}