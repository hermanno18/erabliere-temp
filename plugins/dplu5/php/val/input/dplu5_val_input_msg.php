<?php
/**
 * Get the error message
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-02-16)
 *
 * @category dplu5_val
 *
 * @param string $id Value identifier
 *
 * @return string The error message
 *
 */

function dplu5_val_input_msg($id) {

	$data = dplu5_val_data();
	if ( !is_null( $data[$id]['msg'] ) ) {
		return $data[$id]['msg'];
	} else  {
		return dplu5_val_stdMsg(dplu5_val_input_failedTest($id), 'fr');
	}
}