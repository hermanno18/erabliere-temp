<?php
/**
 * Return the validity state on an input
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-02-16)
 *
 * @category dplu5_val
 *
 * @param string $id Input identifier
 *
 * @return boolean
 *
 */

function dplu5_val_input_isValid($id) {
	$data = dplu5_val_data();
	return isset($data[$id]['isValid']) ? $data[$id]['isValid'] : true;
}