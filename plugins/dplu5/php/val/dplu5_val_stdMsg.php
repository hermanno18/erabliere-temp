<?php
/**
 * Return standard error message
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-05-08)
 *
 * @package dplu5
 *
 * @category val
 *
 * @param string $function Identifier of the test
 * @param string $lng Language of the message ( default: fr )
 *
 * @return string The message
 *
 */

function dplu5_val_stdMsg($function, $lng = 'fr') {
	$msg = array(
		'fr' => array(
			'dplu5_check_isNotEmpty' =>			"Ce champ est requis",
			'dplu5_check_isDateIso' =>			"Ce champ doit contenir une date au format suivant: 2000-12-31",
			'dplu5_check_isDomainName' =>		"Ce champ doit contenir un nom de domaine valide ex: exemple.com",
			'dplu5_check_isEmail' =>			"Ce champ doit contenir un adresse courriel valide ex: exemple@exemple.com",
			'dplu5_check_isUsername' =>			"Ce champ doit contenir un nom d'usager valide:"
													. "<br>- Entre 8 et 16 caractères"
													. "<br>- Peut contenir des lettres (a-zA-Z)"
													. "<br>- Peut contenir des chiffres (0-9)"
													. "<br>- Peut contenir des tirets (-)",
			'dplu5_check_isPassword' =>			"Ce champ doit contenir mot de passe valide:"
													. "<br>- Entre 8 et 16 caractères"
													. "<br>- Doit contenir au moins une lettre minuscule (a-z)"
													. "<br>- Doit contenir au moins une lettre majuscule (A-Z)"
													. "<br>- Doit contenir au moins un chiffre (0-9)"
													. "<br>- Doit contenir au moins un caractère spécial (.,!$%?&*()-_+=)",
			'dplu5_check_isHumanName' =>		"Ce champ doit contenir le nom d'une personne ex: John Doe",
			'dplu5_check_isPrice' =>			"Ce champ doit contenir un prix ex: 10.00",
			'dplu5_check_isInt' =>				"Ce champ doit contenir nombre entier ex: 10",
			'dplu5_check_isMimeType' =>			"Le type de fichier est invalide",
			'dplu5_check_isMimeType_multi' =>	"Au moins un des fichiers n'est pas du bon type",
			'dplu5_check_isFileExt' =>			"L'extension du fichier est invalide",
			'dplu5_check_fileHasNoDuplicate' =>	"Le fichier contient des doublons",
			'dplu5_check_isAlphaNum' =>			"Ce champ doit contenir des caratères alphanuméric uniquement",
			'dplu5_check_isPhone' =>			"Ce champ doit contenir un numéro de téléphone valide ex: 999-999-9999",
		),
		'en' => array(
			'dplu5_check_isNotEmpty' =>			"This field is required",
			'dplu5_check_isDomainName' =>		"This field must contain a valid domain name eg: exemple.com",
			'dplu5_check_isEmail' =>			"This field must contain a valid e-mail address eg: example@example.com",
			'dplu5_check_isUsername' =>			"This field must contain a valid user name:"
													. "<br>- Between 8 and 16 characters"
													. "<br>- May contain letters (a-zA-Z)"
													. "<br>- May contain digits (0-9)"
													. "<br>- May contain dashes (-)",
			'dplu5_check_isPassword' =>			"This field must contain a valid password:"
													. "<br>- Between 8 and 16 characters"
													. "<br>- Must contain at least one lowercase letter (a-z)"
													. "<br>- Must contain at least one uppercase letter (A-Z)"
													. "<br>- Must contain at least one digit (0-9)"
													. "<br>- Must contain at least one special character (.,! $%?&*()-_ + =)",
			'dplu5_check_isHumanName' =>		"This field must contain the name of a person eg: John Doe",
			'dplu5_check_isPrice' =>			"This field must contain a price eg: 10.00",
			'dplu5_check_isInt' =>				"This field must contain an integer eg: 10",
			'dplu5_check_isMimeType' =>			"Invalid file type",
			'dplu5_check_isMimeType_multi' =>	"At least one file type is invalid",
			'dplu5_check_isFileExt' =>			"Invalid file extension",
			'dplu5_check_fileHasNoDuplicate' =>	"The file contains duplicate",
			'dplu5_check_isAlphaNum' =>			"This field must contain alphanumeric characters only",
			'dplu5_check_isPhone' =>			"This field must contain a valid phone number eg: 999-999-9999",
		)
	);

	if ( isset($msg[$lng][$function]) ) {
		return $msg[$lng][$function];
	} else {
		if ( $lng === 'fr' ) {
			return 'Erreur inconnue';
		} else {
			return 'Unknown error';
		}
	}
}