<?php
/**
 * Execute the form validation, update the form data and return the result
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-02-16)
 *
 * @package dplu5
 *
 * @category val
 *
 * @return boolean
 *
 */

function dplu5_val_isValid() {

	$data = dplu5_val_data();

	$isValid = true;

	foreach ( $data as $valueId => $valueData ) {
		foreach ( $valueData['tests'] as $test ) {
			if ( !call_user_func($test['function'], $test['param']) ) {
				$data[$valueId]['isValid'] = false;
				$data[$valueId]['failedTest'] = $test['function'];
				$data[$valueId]['msg'] = $test['msg'];
				$isValid = false;
				break;
			}
		}
	}
	
	dplu5_val_data($data);

	return $isValid;
}