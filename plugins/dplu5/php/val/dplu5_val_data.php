<?php
/**
 * Keep the validator data statically during the script execution
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-02-16)
 *
 * @category dplu5_val
 *
 * @param string $newData Value identifier
 *
 * @return array The data
 *
 */

function dplu5_val_data($newData = null) {
	static $data = null;

	if (null !== $newData) {
		$data = $newData;
	}
	return $data;
}