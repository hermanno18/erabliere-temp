<?php
/**
 * Add a test to the validator
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_val
 *
 * @param string $id Value identifier
 * @param mixed	 $value The value to be tested
 * @param string $function The name of the test function
 * @param array $param Parameters of the test function
 *
 * @return null
 *
 */

function dplu5_val_addTest($id, $value, $function, $param = array()) {

	$data = dplu5_val_data();
	$param['value'] = $value;
	$msg = isset($param['msg']) ? $param['msg'] : null;

	if ( !isset($data[$id]) ) {
		$data[$id]['isValid'] = true;
		$data[$id]['failedTest'] = null;
	}

	$data[$id]['tests'][] = array(
		'function' => $function,
		'param' => $param,
		'msg' => $msg,
	);

	dplu5_val_data($data);
}