<?php
/**
 * Return the present date and time
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-21)
 *
 * @category dplu5_date
 * 
 * @param boolean $withTime Include the time of the day 
 *
 * @return string The date ( yyyy-mm-dd hh:mm:ss )
 */

function dplu5_date_now($withTime = true) {
	if ($withTime) {
		return date('Y-m-d H:i:s');
	}
	return date('Y-m-d');
}