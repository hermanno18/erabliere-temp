<?php
/**
 * Filter an array of strings
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-08-21)
 * 
 * @package dplu5
 *
 * @category util
 *
 * @param array $strings The string array
 *
 * @return array The filtered values
 *
 */

function dplu5_util_filterStrings(array $strings) {
	$out = [];
	
	foreach ( $strings as $key => $val ) {
		if ( is_array($val) ) {
			$out[$key] = dplu5_util_filterStrings($val);
		} else {
			$out[$key] = filter_var(trim($val), FILTER_SANITIZE_STRING);
		}
	}
	return $out;
}