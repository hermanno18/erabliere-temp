<?php
/**
 * Return the language code of a script
 *
 * PHP >= 7
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2020-01-16
 *
 * @package dplu5
 *
 * @category util
 *
 * @param array $param
 * @param string $param['default'] The default language (2 letters iso code)
 * @param array $param['supported'] The supported languages (list of 2 letters iso codes)
 *
 * @return boolean
 *
 */

function dplu5_util_lng($param = []) {

	$param['default'] = $param['default'] ?? 'fr';
	$param['supported'] = $param['supported'] ?? ['fr', 'en'];

	$lng = null;
	$supportedLanguages = $param['supported'];
	$preferredLanguages = dplu5_http_prefLng();

	if ( isset($_GET['lng']) && in_array($_GET['lng'], $supportedLanguages)) {
		$lng = $_GET['lng'];
	} else {
		foreach ( $supportedLanguages as $elem1 ) {
			foreach ( $preferredLanguages as $key2 => $elem2 ) {
				if ( strpos($key2, $elem1) === 0 ) {
					$matches[$elem1] = $elem2;
					break;
				}
			}
		}

		foreach ( $matches as $key => $elem ) {
			if ( $elem == 1 ) {
				$lng = $key;
				break;
			}
		}

		if ( is_null($lng) ) {
			$lng = $param['default'];
		}
	}

	return $lng;
}

