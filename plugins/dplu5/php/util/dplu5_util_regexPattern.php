<?php
/**
 * A collection of useful regex patterns
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-04-26)
 * 
 * @package dplu5
 *
 * @category util
 *
 * @param string $name Pattern	name
 *
 * @return string The pattern
 *
 */
function dplu5_util_regexPattern($name) {
	$patterns = array(
		'alphanumeric' => '/[0-9]*[a-z]*/i',
		'companyName' => '/^[\p{L}0-9]+[\p{L}0-9\-\'\s\.]*[\p{L}0-9]+\.?$/',
		'domainName' => '/(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z]/',
		'hostName' => '/(?=^.{4,253}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)/',
		'email' => '/^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$/',
		'humanName' => '/^[a-z]+[a-z\-\'\s]+$/i',
		'int' => '/^([0-9]+)$/',
		'ipv4' => '/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/',
		'notEmpty' => '/.+/',
		'password' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*\W)[a-zA-Z\d\S]{8,16}$/',
		'phone' => '/\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}(x\d{1,4})?$/',
		'postalZip' => '/^[a-z0-9]+[a-z0-9-\s\.]+?$/i',
		'price' => '/^([0-9]+)(\.|\,)([0-9]{2})$/',
		'username' => '/[a-z0-9-]{8,16}/i',
	);
	return $patterns[$name];
}