<?php
/**
 * Minify HTML code
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 * 
 * @version 1.0 (2018-08-30)
 * 
 * @package dplu5
 *
 * @category util
 *
 * @param string $src The HTML source.
 *
 * @return string The minified HTML
 * 
 */

function dplu5_util_minifiyHtml($src) {
	return preg_replace(
		["#\\n#", "#\\r#", "#\\t#", '#\s{2,}#', '#\> \<#'],
		[' ', ' ', '', ' ', '><'],
		$src
	);
}