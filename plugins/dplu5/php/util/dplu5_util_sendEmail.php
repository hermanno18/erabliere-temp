<?php
/**
 * Send a simple email message
 *
 * PHP >= 7
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-09-28
 *
 * @package dplu5
 *
 * @category dplu5_util
 *
 * @uses phpmailer
 *
 * @param array $param
 * @param string $param['from'] The sender's address
 * @param array $param['to'] The destination addresses
 * @param string $param['replyTo'] The reply address
 * @param string $param['subject'] The email subject
 * @param string $param['body'] The email body
 *
 * @return boolean
 *
 */

function dplu5_util_sendEmail($param) {

	// Validate required parameters
	foreach ( ['from', 'to', 'subject', 'body'] as $elem ) {
		if ( !isset($param[$elem]) ) {
			trigger_error("[" . $elem . "] is required", E_USER_ERROR);
			return false;
		}
	}

	// Validate parameters that can not be empty
	foreach ( ['from', 'subject', 'body'] as $elem ) {
		if ( isset($param[$elem]) && $param[$elem] === '' ) {
			trigger_error("[" . $elem . "] cannot be empty", E_USER_ERROR);
			return false;
		}
	}

	// Validate string parameters
	foreach ( ['from', 'replyTo', 'subject', 'body'] as $elem ) {
		if ( isset($param[$elem]) && !is_string($param[$elem]) ) {
			trigger_error("[" . $elem . "] must be a string", E_USER_ERROR);
			return false;
		}
	}

	// Validate [from]
	if ( !dplu5_check_isEmail(['value' => $param['from']]) ) {
		trigger_error("[from] must be a valid email address", E_USER_ERROR);
		return false;
	}

	// Validate [to]
	if ( !is_array($param['to']) ) {
		trigger_error("[to] must be an array", E_USER_ERROR);
		return false;
	}

	if ( count($param['to']) === 0 ) {
		trigger_error("[to] cannot be empty", E_USER_ERROR);
		return false;
	}

	foreach ( $param['to'] as $elem ) {
		if ( !dplu5_check_isEmail(['value' => $elem ?? '']) ) {
			trigger_error("[to] must contain only valid email addresses", E_USER_ERROR);
			return false;
		}
	}

	// Validate [replyTo]
	if ( isset($param['replyTo']) &&  !dplu5_check_isEmail(['value' => $param['replyTo']]) ) {
		trigger_error("[replyTo] must be a valid email address", E_USER_ERROR);
		return false;
	}

	// Validate [subject]
	if ( !is_string($param['subject']) ) {
		trigger_error("[subject] must be a string", E_USER_ERROR);
		return false;
	}

	// Send the new password
	$mail = new PHPMailer;
	$mail->CharSet = 'UTF-8';

	// Set who the message is to be sent from
	$mail->setFrom($param['from']);

	// Set a reply-to address
	$mail->addReplyTo($param['replyTo'] ?? $param['from']);

	// Set who the message is to be sent to
	foreach ( $param['to'] as $address ) {
		$mail->addAddress($address);
	}

	$mail->Subject = html_entity_decode($param['subject'], ENT_QUOTES);

	$mail->msgHTML($param['body']);

	return $mail->send();
}