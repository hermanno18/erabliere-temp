<?php
/**
 * Return an array of http preferred languages
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @category dplu5_http
 *
 * @return array Preferred languages
 */

function dplu5_http_prefLng() {
	$prefLng = array();
	if ( isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ) {
		$prefLng = array_reduce(
			explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']),
			function ($res, $el) {
			  list($l, $q) = array_merge(explode(';q=', $el), array(1));
			  $res[$l] = (float) $q;
			  return $res;
			}, array());
		arsort($prefLng);
	}
	return $prefLng;
}