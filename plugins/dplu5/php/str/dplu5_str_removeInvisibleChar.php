<?php
/**
 * Remove invisible characters
 * 
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-26)
 *
 * @package dplu5
 *
 * @category str
 * 
 * @param string $str The string
 *
 * @return string
 *
 */

function dplu5_str_removeInvisibleChar($str) {
	return str_replace(array("\n", "\r", "\t"),  '', $str);
}