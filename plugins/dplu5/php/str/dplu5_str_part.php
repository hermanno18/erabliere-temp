<?php
/**
 * Return a part of a string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2019-01-09)
 *
 * @package dplu5
 *
 * @category str
 *
 * @return string
 *
 */

function dplu5_str_part($string, $delimiter, $part = 1) {
	$output = $string;
	$delimiterPos = strpos($string, $delimiter);
	if ( is_numeric($delimiterPos) ) {
		$parts = explode($delimiter, $string);
		return $parts[$part - 1];
	}
	return $output;
}