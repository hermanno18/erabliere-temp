<?php
/**
 * Check if the string encoding is utf8
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-27)
 *
 * @package dplu5
 *
 * @category str
 *
 * @param $str The string
 *
 * @return boolean
 *
 */

function dplu5_str_isUtf8($str) {
	$res = mb_detect_encoding($str, 'UTF-8', true);
	if ($res !== false) {
		return true;
	}
	return false;
}