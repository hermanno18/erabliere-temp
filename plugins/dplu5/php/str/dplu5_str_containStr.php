<?php
/**
 * Check if the string contain a string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-27)
 *
 * @package dplu5
 *
 * @category str
 *
 * @param $str The string
 * @param $needle The substring
 *
 * @return boolean
 *
 */

function dplu5_str_containStr($str, $needle) {
	return is_int(strpos($str, $needle));
}