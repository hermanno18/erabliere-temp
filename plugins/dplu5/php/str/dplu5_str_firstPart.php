<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo Add comments
 */

function dplu5_str_firstPart($string, $delimiter) {
	$output= $string;
	$delimiterPos= strpos($string, $delimiter);
	if ($delimiterPos !== false) {
		$output= substr($string, 0, $delimiterPos);
	}
	return $output;
}