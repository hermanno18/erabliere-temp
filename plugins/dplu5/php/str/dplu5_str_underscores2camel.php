<?php
/**
 * Get domain info
 * 
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-05-01)
 *
 * @package dplu5
 *
 * @category opensrs
 * 
 * @param string $str The string to convert
 *
 * @return array The result operation
 *
 */
function dplu5_str_underscores2camel($str) {
	return str_replace(
		array(
			'_a', '_b', '_c', '_d', '_e', '_f', '_g', '_h', '_i','_j', '_k', '_l', '_m', 
			'_n', '_o', '_p', '_q', '_r', '_s', '_t', '_u', '_v', '_w', '_x', '_y', '_z'
		), 
		array(
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
			'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
		),
		$str
	);
}