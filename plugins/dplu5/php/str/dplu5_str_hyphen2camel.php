<?php
/**
 * Replace hyphen with camel case
 *
 * @version 2021-04-22
 *
 * @param string $str The string to convert
 *
 * @return string
 *
 */
function dplu5_str_hyphen2camel($str) {
	return str_replace(
		array(
			'-a', '-b', '-c', '-d', '-e', '-f', '-g', '-h', '-i','-j', '-k', '-l', '-m',
			'-n', '-o', '-p', '-q', '-r', '-s', '-t', '-u', '-v', '-w', '-x', '-y', '-z'
		),
		array(
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
			'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
		),
		$str
	);
}