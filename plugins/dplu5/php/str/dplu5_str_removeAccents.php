<?php
/**
 * Remove accents from a string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @package dplu5
 * 
 * @category str
 *
 * @param string $str The string to encode
 * @param string $charset The charset of the string (Defautl: UTF-8)
 * 
 * @return string The new string without accent
 *
 */

function dplu5_str_removeAccents($str, $charset = 'UTF-8') {
	$step1 = preg_replace(
		'/&([a-zA-Z])(grave|acute|circ|tilde|uml|ring|cedil|slash);/',
		'$1',
		htmlentities($str, ENT_NOQUOTES, $charset )
	);
	return html_entity_decode(preg_replace( '/&([a-zA-Z]{2})(lig);/', '$1', $step1));
}