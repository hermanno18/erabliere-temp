<?php
/**
 * Tell if a string begin with a substring
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-26)
 * 
 * @package dplu5
 *
 * @category str
 *
 * @param $str The string
 * @param $needle The substring
 *
 * @return boolean
 */

function dplu5_str_beginWith($str, $needle) {
	return strpos($str, $needle) === 0;
}