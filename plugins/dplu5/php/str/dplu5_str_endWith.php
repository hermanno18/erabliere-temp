<?php
/**
 * Tell if a string end with a substring
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-29)
 *
 * @package dplu5
 * @category str
 *
 * @param $str The string
 * @param $needle The substring
 *
 * @return boolean
 */

function dplu5_str_endWith($str, $needle) {
	return strrpos($str, $needle) === (strlen($str) - strlen($needle));
}