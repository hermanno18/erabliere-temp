<?php
/**
 * Return a random string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-06-12
 *
 * @package dplu5
 *
 * @category str
 *
 * @param array $opt Optional parameters
 * @param array $opt['permitedChar'] Permited characters
 * @param int $opt['length'] Length of the output string
 *
 * @return string
 *
 */

function dplu5_str_random(array $opt = []) {

	$default['permitedChar'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$default['length'] = 10;

	$opt = array_merge($default, $opt);

	$permitedCharLength = strlen($opt['permitedChar']);

	$output = '';

	for( $i = 0; $i < $opt['length']; $i++ ) {
        $randomChar = $opt['permitedChar'][random_int(0, $permitedCharLength - 1)];
        $output .= $randomChar;
    }

	return $output;
}