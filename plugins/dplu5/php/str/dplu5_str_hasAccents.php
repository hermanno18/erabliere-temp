<?php
/**
 * Check if the string contain accents
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-27)
 *
 * @package dplu5
 *
 * @category str
 *
 * @param $str The string
 *
 * @return boolean
 *
 */

function dplu5_str_hasAccents($str) {
	$chars= array('é', 'ë', 'ê', 'è', 'â', 'à', 'æ', 'ô', 'oe', 'ù', 'û', 'ü', 'ç', 'î', 'ï', 'ÿ');
	foreach ($chars as $char) {
		if (dplu5_str_containStr($str, $char)) {
			return true;
		}
	}
	return false;
}