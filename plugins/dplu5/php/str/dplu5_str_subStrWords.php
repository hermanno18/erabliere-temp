<?php
/**
 * Return a substring based on words
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2020-08-26
 *
 * @param array $param The parameters array
 * @param string $param['str'] The string
 * @param integer $param['begin'] The position of the word at the beginning of the substring
 * @param integer $param['end'] The position of the word at the end of the substring
 *
 * @return string A substring composed of words
 *
 */

function dplu5_str_subStrWords($param) {

	$param['begin'] = isset($param['begin']) ? $param['begin'] : null;
	$param['end'] = isset($param['end']) ? $param['end'] : null;

	$wordArr = explode(' ', $param['str']);

	$beginIndex = ($param['begin'] !== null) ? ($param['begin'] - 1) : 0;
	$endIndex = ($param['end'] !== null) ? ($param['end'] - 1) : count($wordArr) - 1;

	$output = '';
	for ( $i = $beginIndex; $i <= $endIndex; $i++ ) {
		$output .= $wordArr[$i];
		if ( $i < $endIndex ) {
			$output .= ' ';
		}
	}

	return $output;
}