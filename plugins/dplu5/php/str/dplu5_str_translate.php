<?php
/**
 * Translate strings inside a bigger string
 *
 * The translation array need to be like the following:
 *
 * [
 *     'fr' => [
 *       'source string'
 *       => 'translated string',
 *     ],
 *     'en' => [
 *       'source string'
 *       => 'translated string',
 *     ]
 * ]
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-08
 *
 * @package app
 *
 * @param string $str The string to be translated
 * @param array $translation Translation's array
 * @param string $lng The language iso code
 *
 * @return string The translated string
 *
 */

function dplu5_str_translate($str, $translation, $lng) {
	return str_replace(dplu5_arr_sortByLength(array_keys($translation[$lng])), dplu5_arr_sortByLength($translation[$lng], true), $str);
}