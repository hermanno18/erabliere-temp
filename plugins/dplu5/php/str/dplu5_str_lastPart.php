<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo Add comments
 */

function dplu5_str_lastPart($string, $delimiter) {

	$output= $string;
	if ( false !== strpos($string, $delimiter) ) {
		$stringParts = explode($delimiter, $string);
		return end($stringParts);
	}
	return $output;
}