<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_referer_check($autorized) {
	return in_array($_SESSION['dplu5']['referer'], $autorized);
}