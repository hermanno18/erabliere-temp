<?php
/**
 * Append a string to all the values of an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param string $suffix String to be added at the end of all the values
 * @param array $array The array
 *
 * @return array The resulting array
 */

function dplu5_arr_append($suffix, $array) {

	$output = [];

	foreach ( $array as $key => $elem ) {
		if ( is_array($elem) ) {
			$output[$key] = dplu5_arr_append($suffix, $elem);
		} else {
			$output[$key] = $elem . $suffix;
		}
	}

	return $output;
}