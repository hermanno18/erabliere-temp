<?php
/**
 * Tell if values are in an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @package dplu5
 *
 * @category arr
 *
 * @version 2019-08-28
 *
 * @param array $param The parameters
 * @param array $param['values'] The values to check
 * @param array $param['array'] The array
 * @param string $param['type'] The type of verification (any, all)
 *
 * @return bolean
 */

function dplu5_arr_inArray($param) {

	// Validate required parameters
	foreach ( ['values', 'array', 'type'] as $elem ) {
		if ( !isset($param[$elem]) ) {
			trigger_error("[" . $elem . "] is required", E_USER_ERROR);
		}
	}

	// Validate the parameters that must be arrays
	foreach ( ['values', 'array'] as $elem ) {
		if ( !is_array($param[$elem]) ) {
			trigger_error("[" . $elem . "] must be an array", E_USER_ERROR);
		}
	}

	// Validate [type]
	if ( !in_array($param['type'], ['any','all']) ) {
		trigger_error("[type] must be 'any' or 'all'", E_USER_ERROR);
	}

	if ( $param['type'] === 'all' ) {
		return empty(array_diff($param['values'], $param['array']));
	} else {
		return !empty(array_intersect($param['values'], $param['array']));
	}
}
