<?php
/**
 * Filter an array of strings
 *
 * @version 2019-09-30
 *
 * @param array $strings The string array
 *
 * @return array The filtered values
 *
 */

function dplu5_arr_filterStrings(array $strings) {
	$out = [];

	foreach ( $strings as $key => $val ) {
		if ( is_array($val) ) {
			$out[$key] = dplu5_arr_filterStrings($val);
		} else {
			$out[$key] = filter_var(trim($val), FILTER_SANITIZE_STRING);
		}
	}
	return $out;
}