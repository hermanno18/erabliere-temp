<?php
/**
 * Sort an array by it's keys has defined by $order 
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-28)
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param array $arr The array
 * @param array $order The custom order
 *
 * @return array The resulting array
 */

function dplu5_arr_customKeySort($arr, $order) {

	uksort($arr, function($key1, $key2) use ($order) {
		return ((array_search($key1, $order) > array_search($key2, $order)) ? 1 : -1);
	});

	return $arr;
}