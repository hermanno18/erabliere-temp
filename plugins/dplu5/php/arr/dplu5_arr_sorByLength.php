<?php
/**
 * Sort an array by the length of its values
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param array $array The array
 *
 * @return boolean
 */

function dplu5_arr_sortByLength($array, $sorByKeys = false) {
	$function = $sorByKeys ? 'uksort' : 'usort';
	$function($array, function($a, $b) {
		return strlen($b) - strlen($a);
	});
	return $array;
}