<?php
/**
 * Sanitize an array of strings
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param string $array
 *
 * @return array The resulting array
 */

function dplu5_arr_sanitize($array) {

	$output = [];

	foreach ( $array as $key => $elem ) {
		if ( is_array($elem) ) {
			$output[$key] = dplu5_arr_sanitize($elem);
		} else if ( is_string($elem) ) {
			$output[$key] = dplu5_str_sanitize($elem);
		} else {
			$output[$key] = $elem;
		}
	}

	return $output;
}