<?php
/**
 * Flatten an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-06-29)
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param array $arr The source array
 * @param string $prefix The prefix for concatening the keys
 *
 * @return array The result
 *
 */

function dplu5_arr_flatten($arr, $prefix = '', $keyGlue = '_') {
	$out = [];
	
	foreach ( $arr as $key1 => $elem1 ) {
		
		if ( !empty($prefix) ) {
			$newKey = $prefix . $keyGlue . $key1;
		} else {
			$newKey = $key1;
		}
		
		if ( is_array($elem1) ) {
			$out = array_merge($out, dplu5_arr_flatten($elem1, $newKey));
		} else {
			$out[$newKey] = $elem1;
		}
	}
	return $out;
}