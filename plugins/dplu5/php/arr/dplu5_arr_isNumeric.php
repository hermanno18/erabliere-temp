<?php
/**
 * Check if all the values in an array are numeric
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-09-19)
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param array $arr The array
 *
 * @return boolean
 */

function dplu5_arr_isNumeric($arr) {
	foreach ( $arr as $val ) {
		if ( !is_numeric($val) ) {
			return false;
		}
	}
	return true;
}