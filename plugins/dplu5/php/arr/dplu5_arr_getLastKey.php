<?php
/**
 * Return the last key of an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2020-09-09
 *
 * @param array $array
 *
 * @return mixed The last ley of the array
 */

function dplu5_arr_getLastKey($array) {
	$keys= array_keys($array);
	return end($keys);
}