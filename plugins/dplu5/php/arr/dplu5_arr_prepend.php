<?php
/**
 * Prepend a string to all the values of an array
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category arr
 *
 * @param string $prefix String to be added at the beginning of all the values
 * @param array $array The array
 *
 * @return array The resulting array
 */

function dplu5_arr_prepend($prefix, $array) {

	$output = [];

	foreach ( $array as $key => $elem ) {
		if ( is_array($elem) ) {
			$output[$key] = dplu5_arr_prepend($prefix, $elem);
		} else {
			$output[$key] = $prefix . $elem;
		}
	}

	return $output;
}