<?php
/**
 * Custom error handler
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-10-17
 *
 * @package dplu5
 *
 * @category err
 *
 * @param int $errno The error number
 * @param stirng $errstr The error string
 *
 * @return null
 */
function dplu5_err_handler($errno, $errstr) {
	$errorType= array(
		E_ERROR				=> 'PHP ERROR',
		E_WARNING			=> 'PHP WARNING',
		E_NOTICE			=> 'PHP NOTICE',
		E_STRICT			=> 'PHP NOTICE',
		E_DEPRECATED		=> 'PHP WARNING',
		E_USER_ERROR         => 'APP ERROR',
		E_USER_WARNING       => 'APP WARNING',
		E_USER_NOTICE        => 'APP NOTICE',
	);

	$backtrace= debug_backtrace();

	$str = '[' . dplu5_date_now() . '] ';
	$str .= '[' . $_SERVER['REMOTE_ADDR'] . '] ';

	$str .= $errorType[$errno] . ': ' . $errstr . ' ';

	foreach ($backtrace as $element) {
		if (isset($element['file'])) {
			if ($backtrace[0] === $element) {
				$str.= 'IN FILE ';
			} else {
				$str.= ' CALLED AT ';
			}
			$str.= $element['file'];
			$str.= ' ON LINE ' . $element['line'];
		}
	}

	$str .= "\n";

	dplu5_err_str($str);

	if ( defined('DPLU5_ERR_LOGERRORS') && DPLU5_ERR_LOGERRORS ) {
		error_log(dplu5_err_str(), 3, DPLU5_ERR_LOGFILE);
	}

	if ( defined('DPLU5_ERR_DISPLAYERRORS') && DPLU5_ERR_DISPLAYERRORS ) {
		echo '<pre>';
		echo dplu5_err_str();
		echo '</pre>';
	}

	if ( $errno === E_ERROR || $errno === E_USER_ERROR ) {
		die;
	}

}