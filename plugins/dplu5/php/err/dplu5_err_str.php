<?php
/**
 * Return a readable error message
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-05)
 *
 * @package dplu5
 * 
 * @category err
 *
 * @param string $new The new error string
 * @param boolean $append Tell to append the new error at the end of the current error
 *
 * @return null
 */
function dplu5_err_str($new = null, $append = false) {
	static $str = '';
	if ( $new !== null ) {
		if ( $append ) {
			$str .= $new;
		} else {
			$str = $new;
		}
	}
	return $str;
}