<?php
function dplu5_err_setHandler($displayErrors= false) {

	error_reporting(E_ALL | E_STRICT);

	set_error_handler('dplu5_err_handler');
	dplu5_err_setting('displayError', $displayErrors);
}