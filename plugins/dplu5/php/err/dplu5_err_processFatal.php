<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo Add comments
 */

function dplu5_err_processFatal() {
	$output = '';
	$lastError = error_get_last();
	if ( E_ERROR === $lastError['type'] ) {
		$output = 'PHP ERROR: ' . $lastError['message'] . ' IN FILE ' . $lastError['file'] . ' ON LINE  ' . $lastError['line'];
	}
	if ( E_STRICT === $lastError['type'] ) {
		$output = 'PHP NOTICE: ' . $lastError['message'] . ' IN FILE ' . $lastError['file'] . ' ON LINE  ' . $lastError['line'];
	}
	dplu5_err_str($output, true);
}