<?php
function dplu5_err_html($str) {
	$output= str_replace(
		array(
			'APP ERROR',
			'APP WARNING',
			'APP NOTICE',
			'PHP ERROR',
			'PHP WARNING',
			'PHP NOTICE',
			'ON LINE',
			'IN FILE',
			'CALLED BY'
		),
		array(
			'<br><br><b>APP ERROR</b>',
			'<br><br><b>APP WARNING</b>',
			'<br><br><b>APP NOTICE</b>',
			'<br><br><b>PHP ERROR</b>',
			'<br><br><b>PHP WARNING</b>',
			'<br><br><b>PHP NOTICE</b>',
			'<b>ON LINE</b>',
			'<br><b>IN FILE</b>',
			'<br><b>CALLED BY</b>'
		),
		$str
	);
	return $output;
}