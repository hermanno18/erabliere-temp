<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo Add comments
 */
function dplu5_err_setting($name, $value = null) {
	static $setting = array();
	$output= null;
	if (is_null($value)) {
		if (isset($setting[$name])) {
			$output= $setting[$name];
		}
	} else {
		$setting[$name]= $value;
		$output= $setting[$name];
	}
	return $output;
}