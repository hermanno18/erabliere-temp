<?php
/**
 * Link to database
 *
 * Return database link ressource re-usable in all databases functions
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-19)
 *
 * @package dplu5
 * 
 * @category mysql
 * 
 * @param string $usr The database username
 * @param string $pwd The database password
 * @param string $db The database name
 * @param string $host The database host
 *
 * @return ressource database link
 *
 */

function dplu5_mysql_link($usr = null, $pwd = null, $db = null, $host = 'localhost') {
	return dplu5_mysql_connect($host, $usr, $pwd, $db);
}