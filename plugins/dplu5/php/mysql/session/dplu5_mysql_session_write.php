<?php
/**
 * Save session data in database
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param string $id The session id
 * @param array $data The session data
 *
 * @return boolean The result of the mysql operation
 *
 */

function dplu5_mysql_session_write($id, $data) {
	$sqlString = "REPLACE INTO session VALUES ('%s', '%s', '%s')";
	$sqlValues = array($id, time(), $data);
	return boolval(dplu5_mysql_query(dplu5_mysql_session_dbLink(), $sqlString, $sqlValues));
}