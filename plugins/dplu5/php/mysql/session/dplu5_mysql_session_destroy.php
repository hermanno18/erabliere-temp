<?php
/**
 * Delete the session
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param string $id The ID of the session in the database
 *
 * @return boolean The result of the operation
 *
 */

function dplu5_mysql_session_destroy($id) {
	$sqlString = "DELETE FROM session WHERE id = '%s'";
	$sqlValues = array($id);
	return boolval(dplu5_mysql_query(dplu5_mysql_session_dbLink(), $sqlString, $sqlValues));
}