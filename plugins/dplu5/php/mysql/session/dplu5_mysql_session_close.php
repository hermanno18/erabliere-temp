<?php
/**
 * Close the database connection
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @return boolean The result of the operation
 *
 */

function dplu5_mysql_session_close() {
	return mysqli_close(dplu5_mysql_session_dbLink());
}