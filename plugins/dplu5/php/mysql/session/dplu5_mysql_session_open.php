<?php
/**
 * Verify the database connection
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @return boolean The result of the operation
 *
 */

function dplu5_mysql_session_open() {
	return is_object(dplu5_mysql_session_dbLink());
}