<?php
/**
 * Delete old sessions from the database
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param string $maxAge The max age of the session in seconds
 *
 * @return boolean The result of the operation
 *
 */

function dplu5_mysql_session_gc($maxAge) {
	$sqlString = "DELETE FROM session WHERE lastAccess < '%s'";
	$sqlValues = array(time() - $maxAge);
	return boolval(dplu5_mysql_query(dplu5_mysql_session_dbLink(), $sqlString, $sqlValues));
}