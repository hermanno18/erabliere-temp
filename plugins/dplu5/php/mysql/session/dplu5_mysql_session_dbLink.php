<?php
/**
 * Keep database connection object for further use
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param string $newDbLink The new database connection object
 *
 * @return object The database connection
 *
 */

function dplu5_mysql_session_dbLink($newDbLink = null) {

	static $dbLink = null;

	if ( null !== $newDbLink && is_object($newDbLink) ) {
		$dbLink = $newDbLink;
	}
	return $dbLink;
}