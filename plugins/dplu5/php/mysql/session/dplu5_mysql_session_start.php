<?php
/**
 * Start the MySQL session
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param object The database connection
 *
 * @return null
 *
 */

function dplu5_mysql_session_start($dbLink) {

	dplu5_mysql_session_dbLink($dbLink);

	register_shutdown_function('session_write_close');
	$test = session_set_save_handler(
		'dplu5_mysql_session_open',
		'dplu5_mysql_session_close',
		'dplu5_mysql_session_read',
		'dplu5_mysql_session_write',
		'dplu5_mysql_session_destroy',
		'dplu5_mysql_session_gc'
	);

	ini_set('session.use_only_cookies', 1);
	$cookie_params = session_get_cookie_params();
	session_set_cookie_params(
		$cookie_params['lifetime'],
        $cookie_params['path'],
        $cookie_params['domain'],
        false,
        true
	);
	session_start();
}