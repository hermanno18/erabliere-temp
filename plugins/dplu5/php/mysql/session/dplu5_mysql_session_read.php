<?php
/**
 * Retrieve the session data from the database
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_session
 *
 * @param string $id The session id
 *
 * @return string The session data or an empty string
 *
 */

function dplu5_mysql_session_read($id) {

	$sqlString = "SELECT data FROM session WHERE id = '%s'";
	$sqlValues = array($id);
	$dbResult = dplu5_mysql_query(dplu5_mysql_session_dbLink(), $sqlString, $sqlValues);
	if (isset($dbResult[0]['data'])) {
		return $dbResult[0]['data'];
	}
	return '';
}