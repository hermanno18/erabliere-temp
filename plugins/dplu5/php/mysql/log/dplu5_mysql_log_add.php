<?php
/**
 * Add a generic log
 *
 * PHP >= 7
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-09
 *
 * @package dplu5
 *
 * @category mysql_log
 *
 * @param object $dbLink Database connection
 * @param string $type The log type (error, info, etc.)
 * @param string $msg Log message
 * @param array $opt Optional parameters
 *
 * @return null
 */

function dplu5_mysql_log_add($dbLink, $type, $msg, $opt= array()) {

	$opt['table'] = $opt['table'] ?? 'log';

	return dplu5_mysql_simple_insert(
		$dbLink,
		$opt['table'],
		array(
			'created' => dplu5_date_now(),
			'type' => $type,
			'ip' => $_SERVER['REMOTE_ADDR'],
			'message' => $msg,
		)
	);
}