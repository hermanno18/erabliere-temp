<?php
/**
 * Search text in a search index
 *
 * Note: This funtion requires a searchIndex table structured with the following fields:
 *
 * CREATE TABLE `searchIndex` (
 * `id` int(11) NOT NULL,
 * `created` datetime NOT NULL,
 * `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
 * `object` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
 * `object_id` int(11) NOT NULL,
 * `text` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
 * ) ENGINE=InnoDB;
 *
 * ALTER TABLE `searchIndex` ADD PRIMARY KEY (`id`);
 * ALTER TABLE `searchIndex` ADD FULLTEXT KEY `text` (`text`);
 * ALTER TABLE `searchIndex` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-10-11
 *
 * @package dplu5
 *
 * @category mysql
 *
 * @param resource $dbLnk The database link
 * @param string $object The object type to search
 * @param string $searchTerm The search term
 *
 * @return array $foundIds
 *
 */

function dplu5_mysql_searchText($dbLnk, $object, $searchTerm) {

	// Escape regex reserved characters
	$searchTerm = preg_quote(trim($searchTerm));

	// Replace multiples spaces with one space
    $searchTerm = preg_replace('!\s+!', ' ', trim($searchTerm));

	// Replace all remaining space with .+ for search with regex
    $regex = str_replace(' ', '.+', $searchTerm);

	if ( $regex === '' ) {
		$regex ='.+';
	}

	$sqlQuery = "
		SELECT object_id
		FROM searchIndex
		WHERE object = '%s'
		AND text REGEXP '%s'
	";

	$dbSearchRslt = dplu5_mysql_query($dbLnk, $sqlQuery, [$object, $regex]);

	$foundIds = [];

	if ( !empty($dbSearchRslt) ) {

		// Get found ids
		foreach ( $dbSearchRslt as $row ) {
			$foundIds[] = $row['object_id'];
		}
	}

	return $foundIds;
}