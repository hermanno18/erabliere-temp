<?php
/**
 * Retrieve all the values of a column in a database table
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category mysql
 *
 * @param ressource $link link identifier
 * @param string $table name of the table containing the column
 * @param string $column name of the column
 *
 * @return array column values
 *
 */
function dplu5_mysql_columnValues($link, $table, $column) {

	$sql_string = "SELECT %s FROM %s ORDER BY %s";
	$sql_values = array($column, $table, $column);
	$result = dplu5_mysql_query($link, $sql_string, $sql_values);

	$data = array();
	if ( is_array($result) ) {
		foreach ( $result as $element ) {
			$data[]= $element[$column];
		}
	}
	return $data;
}