<?php
/**
 * Create a listing array to be used in an HTML table
 *
 * Note: this function requires a "unique" column
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-10-11
 *
 * @package dplu5
 *
 * @category mysql
 *
 * @param resource $dbLnk The database link
 * @param array $p Parameters
 * @param ressource $p['dbLink'] The database connection ressource
 * @param array $p['tables'] The list of tables to be used in the query
 * @param array $p['columns'] The list of columns to be displayed in the list
 * @param array $p['uniqueKey'] The name of the unique column
 * @param array $p['inValues'] The collection of values found in another query
 *
 * @return array The listing array containing the rows and the parameters
 *
 */

function dplu5_mysql_listing($p) {

	// Validate required parameters
	foreach ( ['dbLink', 'tables'] as $elem ) {
		if ( !isset($p[$elem]) ) {
			trigger_error("[" . $elem . "] is required", E_USER_ERROR);
			return false;
		}
	}

	// Validate string parameters
	foreach ( ['uniqueKey'] as $elem ) {
		if ( isset($p[$elem]) && !is_string($p[$elem]) ) {
			trigger_error("[" . $elem . "] must be a string", E_USER_ERROR);
			return false;
		}
	}

	// Validate array parameters
	foreach ( ['tables'] as $elem ) {
		if ( isset($p[$elem]) && !is_array($p[$elem]) ) {
			trigger_error("[" . $elem . "] must be an array", E_USER_ERROR);
			return false;
		}
	}

	// Default parameters
	$default['columns'] = '*';
	$default['inValues'] = [];
	$p = $p + $default;

	if ( isset($_GET['ns']) ) {
		unset($_SESSION['dplu5']['mysql']['listing'][dplu5_url_current()]);
	}

	$searchParam = function($name, $default) {

		$urlToSessionParam = [
			'cp' => 'currPage',
			'ob' => 'orderBy',
			'so' => 'sortOrder',
		];

		if ( isset($_GET[$name]) ) {
			$_SESSION['dplu5']['mysql']['listing'][dplu5_url_current()][$name] = $_GET[$name];
			return $_GET[$name];
		} else if ( isset($_SESSION['dplu5']['mysql']['listing'][dplu5_url_current()][$urlToSessionParam[$name]]) ) {
			return $_SESSION['dplu5']['mysql']['listing'][dplu5_url_current()][$urlToSessionParam[$name]];
		} else {
			return $default;
		}
	};

	// Default values
	$listing['rows'] = [];
	$listing['rowsPerPage'] = 20;
	$listing['totalPages'] = 1;
	$listing['currPage'] = 1;

	// Get parameters from URL or SESSION
	$listing['orderBy'] = $searchParam('ob', $p['uniqueKey']);
	$listing['sortOrder'] = strtoupper($searchParam('so', 'asc'));
	$listing['currPage'] = $searchParam('cp', 1);

	// If we have the new search param, the current page is reset to 1
	if ( isset($_GET['ns']) ) {
		$listing['currPage'] = 1;
	}

	if ( !empty($p['inValues']) ) {

		$query = "SELECT SQL_CALC_FOUND_ROWS ";
		$query .=  implode(', ', $p['columns']);
		$query .= " FROM " . implode(', ', $p['tables']);
		$query .= " WHERE " . $p['uniqueKey'] . " IN ('". implode("','", $p['inValues']) . "')";
		$query .= " ORDER BY " . $listing['orderBy'];
		$query .= " " . $listing['sortOrder'];
		$query .= " LIMIT ";
		$query .= ($listing['currPage'] * $listing['rowsPerPage']) - $listing['rowsPerPage'] . ", ";
		$query .= $listing['rowsPerPage'];

		$listing['rows'] = dplu5_mysql_query($p['dbLink'], $query);

		$listing['totalRows'] = dplu5_mysql_query($p['dbLink'], "SELECT FOUND_ROWS()")[0]['FOUND_ROWS()'];

		$quotient = intval(ceil($listing['totalRows'] / $listing['rowsPerPage']));
		$listing['totalPages'] = $quotient < 1 ? 1 : $quotient;

	}

	$_SESSION['dplu5']['mysql']['listing'][dplu5_url_current()] = $listing;

	return $listing;
}