<?php
/**
 * Make a query to a database to show for a table named $name
 *
 * @category mysql
 * @param ressource $link link identifier
 * $param string $name name of the table
 * @return boolean
 */
function dplu5_mysql_tableExists($link, $name) {
	return mysqli_num_rows( mysqli_query($link, "SHOW TABLES LIKE '" . $name . "'") ) > 0;
}