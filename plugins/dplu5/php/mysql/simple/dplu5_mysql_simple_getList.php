<?php
/**
 * Get a list of all values form one column
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category mysql_simple
 *
 * @param ressource $link The database link
 * @param string $table The table
 * @param string $column Tne column
 * @param array $conditions The conditions fo the query
 *
 * @return array
 *
 */

function dplu5_mysql_simple_getList($link, $table, $column, $conditions= array()) {
	$result= dplu5_mysql_simple_select($link, $table, array($column), $conditions);
	$output= array();
	foreach ($result as $element) {
		$output[] = $element[$column];
	}
	return $output;
}