<?php
/**
 * Delete a record with simple conditions
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.1
 *
 * @category mysql_simple
 *
 * @param object $link link identifier
 * @param string $table name of the table containing the record to delete
 * @param array $conditions array where keys are the colunm names and values are values to search
 *
 * @return int Number of affected rows
 */

function dplu5_mysql_simple_delete($link, $table, $conditions = null) {

	$sqlValues = array();
	$sqlQuery = "DELETE FROM `" . $table . "`\n";
	if ( is_array($conditions) ) {
		$sqlQuery .= "WHERE ";
		foreach ( $conditions as $key => $element) {
			$sqlQuery .= "`" . $key . "` = '%s'" . "\n";
			if ( dplu5_arr_getLastKey($conditions) !== $key) {
				$sqlQuery .= "AND ";
			}
			$sqlValues[] = $element;
		}
	}
	return dplu5_mysql_query($link, $sqlQuery, $sqlValues);
}