<?php

/**
 * Insert a record with an array as data
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.1.0 ( 2018-01-22 )
 *
 * @package dplu5
 *
 * @category mysql_simple
 *
 * @param ressource $link link identifier
 * @param string $table Name of the table where to insert the new record
 * @param array $data Associative array where keys are the column names and values are column values
 *
 * @return mixed The query result
 *
 */
function dplu5_mysql_simple_insert($link, $table, $data = array()) {

	$sqlQuery = "INSERT INTO `" . $table . "` SET" . "\n";
	$sqlValues = array();

	foreach ( $data as $key => $element ) {

		$sqlQuery .= "`" . $key . "` = '%s'";

		if ( dplu5_arr_getLastKey($data) !== $key ) {
			$sqlQuery .= ", ";
		}
		$sqlQuery .= "\n";
		$sqlValues[] = $element;
	}
	return dplu5_mysql_query($link, $sqlQuery, $sqlValues);
}
