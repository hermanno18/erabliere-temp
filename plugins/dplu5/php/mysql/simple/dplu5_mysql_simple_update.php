<?php

/**
 * function dplu5_mysql_simple_update
 *
 * Update a record with an array as data and an array for conditions
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.1.0 ( 2018-01-22 )
 *
 * @package dplu5
 *
 * @category mysql_simple
 *
 * @param ressource $link link identifier
 * @param string $table name of the table where the record need to be updated
 * @param array $changes associative array of new data where keys are the colunm names and values are values
 * @param array $conditions associative array for the conditions where keys are the colunm names and values are values
 * @return integer the number of updated records
 */
function dplu5_mysql_simple_update($link, $table, $changes = array(), $conditions = array()) {

	$sqlQuery = "UPDATE `" . $table . "` SET" . "\n";
	$sqlValues = array();

	foreach ( $changes as $key => $element ) {
		$sqlQuery .= "`" . $key . "` = '%s'";
		if ( dplu5_arr_getLastKey($changes) !== $key ) {
			$sqlQuery .= ", ";
		}
		$sqlQuery .= "\n";
		$sqlValues[] = $element;
	}
	if ( !empty($conditions) ) {
		$sqlQuery .= "WHERE ";
		foreach ( $conditions as $key => $element ) {
			$sqlQuery .= "`" . $key . "` = '%s'" . "\n";
			if ( dplu5_arr_getLastKey($conditions) !== $key ) {
				$sqlQuery .= "AND ";
			}
			$sqlValues[] = $element;
		}
	}
	$result = dplu5_mysql_query($link, $sqlQuery, $sqlValues);
	if ( $result === false ) {
		return false;
	}
	return mysqli_affected_rows($link);
}
