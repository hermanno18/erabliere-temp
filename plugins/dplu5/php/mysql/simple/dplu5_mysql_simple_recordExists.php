<?php
/**
 * function dplu5_mysql_simple_recordExists
 *
 * Tell if a record exist in a given table
 *
 * @category mysql_simple
 * @param ressource $link link identifier
 * @param string $table name of the table containing the column
 * @param string $column name of the column
 * @param string $value value to search
 * @return boolean true if found, false if not found
 */
function dplu5_mysql_simple_recordExist($link, $table, $column, $value) {
	$result= dplu5_mysql_simple_select($link, $table, array($column), array($column => $value));
	if (is_array($result) === true && !empty($result)) {
		return true;
	}
	return false;
}