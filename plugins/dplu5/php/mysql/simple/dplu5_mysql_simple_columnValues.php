<?php
/**
 * Retrieve all the values of a column in a database table
 *
 * @author Alexandre Perron <alex@domainplus.com>
 *
 * @package dplu5
 *
 * @category mysql_simple
 *
 * @param ressource $link link identifier
 * @param string $table name of the table where to find the record
 * @param array $column The column returned
 * @param array $conditions Conditions to met (optional)
 *
 * @return mixed The founded row on success and false on failure
 *
 */
function dplu5_mysql_simple_columnValues(
	$link,
	$table,
	$column,
	$conditions = []
) {
	$dbResult= dplu5_mysql_simple_select(
		$link,
		$table,
		[$column],
		$conditions,
		$column,
		'asc'
	);

	$output = [];

	foreach ( $dbResult as $elem ) {
		$output[]= $elem[$column];
	}
	return $output;
}