<?php
/**
 * Select records based on a list of conditions
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_simple
 *
 * @param ressource $link link identifier
 * @param string $table Table searched
 * @param array $fields simple array of columns to be returned
 * @param array $conditions Associative array for the conditions where keys are the colunm names and values are values
 * @param string $orderBy The column in which the results will be sorted
 * @param string $sortOrder Sort ascending on descending (asc, desc)
 * @param string $limit Quantity of records to return
 * @param string $conditionOperator type of search, match any (OR) of the conditions or all (AND) conditions default is all
 *
 * @return mixed array of records on success and false on failure
 */
function dplu5_mysql_simple_select(
		$link,
		$table,
		$fields= array(),
		$conditions= array(),
		$orderBy= '',
		$sortOrder= 'asc',
		$limit= '',
		$conditionOperator= 'AND'
) {
	$sqlValues= array();
	$sqlQuery= "SELECT ";
	if ( !empty($fields) ) {
		foreach ( $fields as $key => $element ) {
			$sqlQuery .= "`" . $element . "`";
			if ( dplu5_arr_getLastKey($fields) !== $key ) {
				$sqlQuery .= ", ";
			} else {
				$sqlQuery .= "\n";
			}
		}
	} else {
		$sqlQuery .= "*\n";
	}
	$sqlQuery.= "FROM `" . $table . "`\n";
	if ( !empty($conditions) ) {
		$sqlQuery .= "WHERE ";
		foreach ( $conditions as $key => $element ) {
			if ( is_string($element) && (dplu5_str_beginWith($element, '%') || dplu5_str_endWith($element, '%')) ) {
				$sqlQuery .= "`" . $key . "` LIKE '%s'" . "\n";
			} else {
				$sqlQuery .= "`" . $key . "` = '%s'" . "\n";
			}
			$sqlValues[] = $element;
			if ( dplu5_arr_getLastKey($conditions) !== $key ) {
				$sqlQuery .= $conditionOperator . " ";
			}
		}
	}

	if ($orderBy !== '') {
		if ( !dplu5_str_endWith($orderBy, ')') ) {
			$sqlQuery .= "ORDER BY `" . $orderBy . "` " . strtoupper($sortOrder);
		} else {
			$sqlQuery .= "ORDER BY " . $orderBy . " " . strtoupper($sortOrder);
		}
	}

	if ($limit !== '') {
		$sqlQuery .= " LIMIT " . $limit;
	}

	return dplu5_mysql_query($link, $sqlQuery, $sqlValues);
}