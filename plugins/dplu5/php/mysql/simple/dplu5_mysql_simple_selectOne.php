<?php
/**
 * Find a single row
 *
 * @author Alexandre Perron <alex@domainplus.com>
 *
 * @package dplu5
 *
 * @category mysql_simple
 *
 * @param ressource $link link identifier
 * @param string $table name of the table where to find the record
 * @param array $conditions Conditions to met (optional)
 * @param array $fields Fields returned (optional)
 *
 * @return mixed The founded row on success and false on failure
 *
 */
function dplu5_mysql_simple_selectOne(
	$link,
	$table,
	$conditions = array(),
	$fields = array()
) {
	$dbResult= dplu5_mysql_simple_select(
		$link,
		$table,
		$fields,
		$conditions,
		'',
		'asc',
		'1'
	);
	if (!isset($dbResult[0])) {
		return false;
	}
	return $dbResult[0];
}