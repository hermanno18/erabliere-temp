<?php
/**
 * Forget a remembered user
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param string $dbLink Database connection
 *
 * @return boolean
 */

function dplu5_mysql_login_forget($dbLink) {
	if ( isset($_COOKIE['dplu5_mysql_login_hash']) ) {
		$hashedUsername= substr($_COOKIE['dplu5_mysql_login_hash'], 0, 8) . substr($_COOKIE['dplu5_mysql_login_hash'], 24);
		setcookie('dplu5_mysql_login_hash', false, time(), '/', $_SERVER['HTTP_HOST'], false, true);
		return $result= dplu5_mysql_simple_delete($dbLink, 'login_remember', array('hashedUsername' => $hashedUsername));
	}
	return true;
}