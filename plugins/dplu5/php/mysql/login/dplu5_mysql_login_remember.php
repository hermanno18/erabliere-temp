<?php
/**
 * Remember a user
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param object $dbLink Database connection
 * @param object $username
 *
 * @return boolean
 */

function dplu5_mysql_login_remember($dbLink, $username) {
	$salt= dplu5_mysql_login_getSalt();
	$hashedUsername= dplu5_mysql_login_createHashedUsername($username);
	$hashedCookie= substr($hashedUsername, 0, 8) . $salt . substr($hashedUsername, 8);
	$hashedFingerprint = dplu5_mysql_login_hash($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'], $salt);

	$expiry= time() + 60 * 60* 24 * 30;
	dplu5_mysql_simple_insert(
		$dbLink,
		'login_remember',
		array(
			'username' => $username,
			'hashedUsername' => $hashedUsername,
			'hashedFingerprint' => $hashedFingerprint,
			'salt'=> $salt,
			'expiry' => $expiry
		)
	);
	setcookie('dplu5_mysql_login_hash', $hashedCookie, $expiry, '/', $_SERVER['HTTP_HOST'], false, true);
	return true;
}