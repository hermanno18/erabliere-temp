<?php
/**
 * Return the logged in username or the word 'guest' if nobody is logged in.
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @return string Logged in username
 */

function dplu5_mysql_login_loggedInUser() {
	if (isset($_SESSION['dplu5']['mysql']['login']['username'])) {
		return $_SESSION['dplu5']['mysql']['login']['username'];
	}
	return 'guest';
}