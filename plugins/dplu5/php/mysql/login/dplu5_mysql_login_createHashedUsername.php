<?php
/**
 * Create hashed username
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param string $username
 *
 * @return string hashed version of the username
 */

function dplu5_mysql_login_createHashedUsername($username) {
	return hash('sha512', $username);
}