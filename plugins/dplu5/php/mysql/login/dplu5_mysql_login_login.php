<?php
/**
 * Populate the session containing the login informations
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-27)
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param string $username The username to save in session
 *
 * @return null
 *
 */

function dplu5_mysql_login_login($username) {
	//session_regenerate_id();
	$_SESSION['dplu5']['mysql']['login']['username']= $username;
	$_SESSION['dplu5']['mysql']['login']['loggedIn']= true;
	$_SESSION['dplu5']['mysql']['login']['lastLogin']= dplu5_date_now();
}