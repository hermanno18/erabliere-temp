<?php
/**
 * Add the combination of a username and ip in the blacklist
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-26
 *
 * @category dplu5_mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $ip
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_addToBlackList($dbLink, $username, $ip, $tablePrefix = null) {
	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_blacklist' : 'login_blacklist';
	return dplu5_mysql_simple_insert(
		$dbLink,
		$table,
		array('username' => $username, 'ip' => $ip)
	);
}