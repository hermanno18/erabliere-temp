<?php
/**
 * Check login credentials
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-06-21
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $password
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_execute($dbLink, $username, $password, $tablePrefix = null) {
	if ( !dplu5_mysql_login_isInBlackList($dbLink, $username, $_SERVER['REMOTE_ADDR'], $tablePrefix) ) {

		if ( dplu5_mysql_login_check($dbLink, $username, $password, $tablePrefix) ) {
			
			dplu5_mysql_login_login($username);
			// Remove faillures for this username and this ip
			dplu5_mysql_login_cleanFaillures($dbLink, $username, $tablePrefix);
			return true;
			}
		dplu5_mysql_login_addFaillure($dbLink, $username, $tablePrefix);
		if ( dplu5_mysql_login_exceedFailedAttempts($dbLink, $username, $_SERVER['REMOTE_ADDR'], $tablePrefix) ) {
			dplu5_mysql_login_addToBlackList($dbLink, $username, $_SERVER['REMOTE_ADDR'], $tablePrefix);
		}
	}
	return false;
}