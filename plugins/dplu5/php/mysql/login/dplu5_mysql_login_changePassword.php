<?php
/**
 * Update the password of a user
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-26
 *
 * @category dplu5_mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $newPassword
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_changePassword($dbLink, $username, $newPassword, $tablePrefix = null) {
	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_user' : 'login_user';
	$salt= dplu5_mysql_login_getSalt();
	$hashedPassword = dplu5_mysql_login_hash($newPassword, $salt);
	return dplu5_mysql_simple_update($dbLink, $table, array('password' => $hashedPassword, 'salt'=> $salt), array('username' => $username));
}