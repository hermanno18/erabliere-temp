<?php
/**
 * Retrieve a remembered user
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param string $dbLink Database connection
 *
 * @return string The username
 */

function dplu5_mysql_login_getRemembered($dbLink) {
	if ( dplu5_str_notEmpty(dplu5_input('dplu5_mysql_login_hash', 'cookie')) ) {
		$hashedUsername= substr( dplu5_input('dplu5_mysql_login_hash', 'cookie'), 0, 8) . substr(dplu5_input('dplu5_mysql_login_hash', 'cookie'), 24);
		$result= dplu5_mysql_simple_select(
			$dbLink,
			'login_remember',
			array('username'),
			array('hashedUsername' => $hashedUsername)
		);

		if ( isset($result[0]['username']) ) {
			return $result[0]['username'];
		}
		return 'guest';
	}
	return 'guest';
}