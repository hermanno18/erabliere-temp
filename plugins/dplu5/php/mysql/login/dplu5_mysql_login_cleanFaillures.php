<?php
/**
 * Clean login failures for this user with this ip
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-26
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_cleanFaillures($dbLink, $username, $tablePrefix = null) {

	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_faillure' : 'login_faillure';

	return dplu5_mysql_simple_delete($dbLink, $table, array('username' => $username, 'ip' => $_SERVER['REMOTE_ADDR']));
}