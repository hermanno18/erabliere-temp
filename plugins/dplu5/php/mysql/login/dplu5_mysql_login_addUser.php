<?php
/**
 * Add a user
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-26
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $password
 * @param string $tablePrefix
 *
 * @return boolean
 *
 */

function dplu5_mysql_login_addUser($dbLink, $username, $password, $tablePrefix = null) {
	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_user' : 'login_user';
	$salt = dplu5_mysql_login_getSalt();
	$hashedPassword = dplu5_mysql_login_hash($password, $salt);
	return dplu5_mysql_simple_insert($dbLink, $table, array('username' => $username, 'password' => $hashedPassword, 'salt'=> $salt));
}