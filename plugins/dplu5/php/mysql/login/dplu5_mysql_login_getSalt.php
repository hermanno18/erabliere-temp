<?php
/**
 * Create the salt
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @return string The salt
 * 
 */

function dplu5_mysql_login_getSalt() {
	return substr(hash('sha512', uniqid(rand(), true) . '!@#$%^&*()_+=-{}][;";/?<>.,' . microtime()), 0, 16);
}