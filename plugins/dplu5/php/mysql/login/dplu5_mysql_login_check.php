<?php
/**
 * Check login credentials
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-26
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $password
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_check($dbLink, $username, $password, $tablePrefix = null) {

	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_user' : 'login_user';

	$result= dplu5_mysql_simple_select(
		$dbLink,
		$table,
		array('username', 'password', 'salt'),
		array('username' => $username),
		'username',
		'asc',
		'1'
	);

	if (!empty($result)) {
		$hashedPassword= dplu5_mysql_login_hash($password, $result[0]['salt']);
		if ($hashedPassword === $result[0]['password']) {
			return true;
		}
	}
	return false;
}