<?php
/**
 * Create a hashed string
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param string $phrase The string to be hashed
 * @param string $salt The salt used to hash the string
 *
 * @return string Hashed string
 */

function dplu5_mysql_login_hash($phrase, $salt) {
	return hash('sha512', $phrase . $salt);
}