<?php
/**
 * Return the login status
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-27)
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @return boolean
 */

function dplu5_mysql_login_isLoggedIn() {
	return (isset($_SESSION['dplu5']['mysql']['login']['loggedIn']) && $_SESSION['dplu5']['mysql']['login']['loggedIn'] === true);
}