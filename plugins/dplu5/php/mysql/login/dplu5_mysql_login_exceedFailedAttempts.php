<?php
/**
 * Check if the login faillures has exceeded the treashold
 *
 * The treashold is 50 faillures in the last 10 min
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-27)
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @param string $dbLink Database connection
 * @param string $username
 * @param string $ip
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_exceedFailedAttempts($dbLink, $username, $ip, $tablePrefix = null) {

	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_faillure' : 'login_faillure';

	$result= dplu5_mysql_query(
		$dbLink,
		"SELECT id FROM " . $table . " WHERE username = '%s' AND ip = '%s' AND created > '%s' LIMIT 50",
		array($username, $ip, date('Y-m-d H:i:s', time() - (60 * 10)))
	);

	if (is_array($result) && !empty($result)) {
		if (count($result) === 50) {
			return true;
		}
	}
	return false;
}