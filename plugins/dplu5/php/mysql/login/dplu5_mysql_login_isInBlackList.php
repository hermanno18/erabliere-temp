<?php
/**
 * Check if the combination of the username and ip is in the blacklist
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param object $dbLink Database connection
 * @param string $username
 * @param string $ip
 * @param string $tablePrefix
 *
 * @return boolean
 */

function dplu5_mysql_login_isInBlackList($dbLink, $username, $ip, $tablePrefix = null) {

	$table = !is_null($tablePrefix) ? $tablePrefix . '_' . 'login_blacklist' : 'login_blacklist';

	$result= dplu5_mysql_simple_select(
		$dbLink,
		'login_blacklist',
		array('username', 'ip'),
		array('username' => $username,'ip' => $ip),
		'username'
	);
	if ( !empty($result) ) {
		return true;
	}
	return false;
}