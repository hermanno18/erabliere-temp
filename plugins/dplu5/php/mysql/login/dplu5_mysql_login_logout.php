<?php
/**
 * Logout
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0 (2018-03-27)
 *
 * @package dplu5
 *
 * @category mysql_login
 *
 * @return null
 *
 */

function dplu5_mysql_login_logout() {
	//session_regenerate_id();
	if (isset($_SESSION['dplu5']['mysql']['login'])) {
		$_SESSION['dplu5']['mysql']['login']['username']= 'guest';
		$_SESSION['dplu5']['mysql']['login']['loggedIn']= false;
		$_SESSION['dplu5']['mysql']['login']['lastLogin']= null;
	}
}