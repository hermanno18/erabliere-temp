<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo comments
 */

function dplu5_mysql_login_setCookie($name, $value, $expiry = null) {
	if ( $expiry === null) {
		$expiry = time() + 60 * 60* 24 * 30;
	}
	setcookie('dplu5_mysql_login_' . $name, $value, $expiry, '/', $_SERVER['HTTP_HOST'], false, true);
}