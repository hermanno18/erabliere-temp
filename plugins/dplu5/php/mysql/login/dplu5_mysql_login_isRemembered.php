<?php
/**
 * Check if the user is remembered
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 1.0
 *
 * @category dplu5_mysql_login
 *
 * @param object $dbLink Database connection
 *
 * @return boolean
 */

function dplu5_mysql_login_isRemembered($dbLink) {
	if ( isset($_COOKIE['dplu5_mysql_login_hash']) ) {
		$hashedUsername_part1= substr($_COOKIE['dplu5_mysql_login_hash'], 0, 8);
		$hashedUsername_part2= substr($_COOKIE['dplu5_mysql_login_hash'], 24);
		$hashedUsername= $hashedUsername_part1 . $hashedUsername_part2;
		$result= dplu5_mysql_simple_select(
			$dbLink,
			'login_remember',
			array('hashedUsername', 'hashedFingerprint', 'salt'),
			array(
				  'hashedUsername' => $hashedUsername
			),
			'hashedUsername',
			'asc',
			'1'
		);
		if (!empty($result)) {
			$hashedFingerprint= dplu5_mysql_login_hash($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'], $result[0]['salt']);
			if ($hashedFingerprint === $result[0]['hashedFingerprint']) {
				return true;
			}
		}
		return false;
	}
	return false;
}