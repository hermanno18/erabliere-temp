<?php
/**
 * Connect to a MySQL server and select a database
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-08-08
 *
 * @package dplu5
 *
 * @category mysql
 *
 * @param string $host hostname of the server
 * @param string $username
 * @param string $password
 * @param string $database name of the database
 * @param string $charset character set of the queries and results
 *
 * @return mixed return the link identifier on success, false on failure
 *
 */
function dplu5_mysql_connect($host, $username, $password, $database, $charset= 'utf8') {
	$link = mysqli_connect($host, $username, $password, $database);

	if ( !$link ) {
		trigger_error("Unable to connect to database: " . mysqli_connect_error() . "(" . mysqli_connect_errno() . ")", E_USER_ERROR);
		return false;
	}

		mysqli_set_charset($link, $charset);
	return $link;
}