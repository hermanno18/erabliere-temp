<?php
/**
 * Make a query to a database
 *
 * @package dplu5
 *
 * @category mysql
 *
 * @param ressource $dbLink Database ressource handle
 * @param string $sqlString A valid SQL query string with the value replaced by %s
 * @param array $sqlValues Values to be inserted in the query string
 *
 * @return mixed return true on succes for write operations and an array for read operations, return false on error
 *
 */
function dplu5_mysql_query($dbLink, $sqlString, $sqlValues = array()) {

	$sqlEscapedValues= array();

	foreach ($sqlValues as $key => $value ) {
		$sqlEscapedValues[$key] = trim(mysqli_real_escape_string($dbLink, $value));
	}

	$sqlQuery = vsprintf($sqlString, $sqlEscapedValues);

	dplu5_mysql_lastQuery($sqlQuery);

	$result = mysqli_query($dbLink, $sqlQuery);

	if ( $result ) {
		if ( is_object($result) ) {
			$array = array();
			while ( $row = mysqli_fetch_assoc($result) ) {
				array_push($array, $row);
			}
			mysqli_free_result($result);
			return $array;
		} else {
			return mysqli_affected_rows($dbLink);
		}
	}
	trigger_error( mysqli_error($dbLink) . ' ' . $sqlQuery , E_USER_ERROR);
	return false;
}