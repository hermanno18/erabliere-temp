<?php
/**
 * Change the sort order
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_changeSortOrder() {
	if ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder'] === 'asc') {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder']= 'desc';
	} elseif ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder'] === 'desc') {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder']= 'asc';
	}
}