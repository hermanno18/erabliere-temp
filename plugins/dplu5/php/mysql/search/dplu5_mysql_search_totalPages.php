<?php
/**
 * Return the number of pages for the search result
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return int Number of pages
 *
 */
function dplu5_mysql_search_totalPages() {
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['totalPages'];
}