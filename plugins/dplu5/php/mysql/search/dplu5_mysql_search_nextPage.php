<?php
/**
 * Return the next page number
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return int The next page number
 *
 */
function dplu5_mysql_search_nextPage() {
	if ( $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] + 1 > $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['totalPages'] ) {
		return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'];
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] + 1;
}