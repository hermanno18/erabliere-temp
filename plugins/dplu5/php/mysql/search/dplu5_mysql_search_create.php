<?php
/**
 * Set or reset the search session
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_create($tables) {
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['tables']= $tables;
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedFields']= array();
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['criterias']= array();
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['joinCondition']= array();
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['keyword']= '';
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['orderBy']= '';
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder']= 'asc';
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentRow']= 0;
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage']= 1;
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage']= null;
}