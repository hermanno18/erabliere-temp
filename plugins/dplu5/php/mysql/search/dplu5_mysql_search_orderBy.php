<?php
/**
 * Set and return the name of the column used for the sorting
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param string $column This new column will be used for the next search
 *
 * @return string Name of the column used for the sorting
 *
 */
function dplu5_mysql_search_orderBy($column = null) {
	if ( !is_null($column) ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['orderBy'] = strval($column);
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['orderBy'];
}