<?php
/**
 * Return the current row
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return int Number of the current row
 *
 */
function dplu5_mysql_search_currentRow() {
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentRow'] = $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] * $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage'] - $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage'];
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentRow'];
}