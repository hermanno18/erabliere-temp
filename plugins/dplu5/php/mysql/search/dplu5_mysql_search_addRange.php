<?php
/**
 * Add a range to the search
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_addRange($name, $from, $to) {
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['range'][$name]['from']= $from;
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['range'][$name]['to']= $to;
}