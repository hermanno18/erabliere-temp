<?php
/**
 * Add a table to the search
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_addTable($name) {
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['tables'][]= $name;
}