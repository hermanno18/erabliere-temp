<?php
/**
 * Build the SQL query
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param resource $dbLink A valid mysqli ressource
 *
 * @return array The search result with total pages, total rows and the list
 *
 */
function dplu5_mysql_search_buildQuery($dbLink) {

	$allColumnNames = array();

	$query = "SELECT SQL_CALC_FOUND_ROWS ";

	if (!empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedColumns'])) {
		foreach ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedColumns'] as $elem) {
			$query .= "`" . str_replace('.', '`.`', $elem) . "`";
			if ($elem !== end($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedColumns'])) {
				$query .= ", ";
			}
		}
	} else {
		$query.= "*";
	}

	$query.= " \nFROM ";

	foreach ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['tables'] as $elem1) {

		$query .= "`" . $elem1 . "`";

		if ($elem1 !== end($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['tables'])) {
			$query .= ", ";
		}

		$columnNames = dplu5_mysql_columnNames($dbLink, $elem1);

		foreach ($columnNames as $elem2) {
			$allColumnNames[]= $elem2;
		}
	}

	$query.= " \nWHERE 1 ";

	if ( !empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['criterias']) ) {

		$query .= " \nAND (\n";

		$counter= 1;

		foreach ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['criterias'] as $key => $elem) {
			$query .= "\t`" . str_replace('.', '`.`', $key) . "`";
			$query .= " LIKE '" . $elem . "'";
			if ($counter < count($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['criterias'])) {
				$query.= " AND\n ";
			}
			$counter++;
		}

		$query .= "\n) ";
	}

	if ( !empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['joinConditions']) ) {

		$query .= " \nAND (\n";

		$counter= 1;

		foreach ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['joinConditions'] as $key => $elem) {
			$query .= "\t`" . str_replace('.', '`.`', $key) . "`";
			$query .= " = `" . str_replace('.', '`.`', $elem) . "`";
			if ($counter < count($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['joinConditions'])) {
				$query.= " AND\n ";
			}
			$counter++;
		}

		$query .= "\n) ";
	}

	if ( !empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['keyword']) ) {

		$query .= " \nAND (\n";

		foreach ($allColumnNames as $elem) {

			$query .= "\t`" . str_replace('.', '`.`', $elem) . "`";

			$query .= " LIKE '%%" . $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['keyword'] . "%%'";

			if ($elem !== end($allColumnNames)) {
				$query .= " OR\n ";
			}
		}
		$query .= "\n) \n";
	}

	if ( isset($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['range']) && !empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['range']) ) {
		foreach ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['range'] as $key => $elem) {
			if ( !empty($elem['from']) || !empty($elem['to']) ) {
				$query .= " \nAND (\n";
				if ( !empty($elem['from']) ) {
					$query .= "`" . $key . "` >= " .  $elem['from'];
				}
				if ( !empty($elem['from']) && !empty($elem['to']) ) {
					$query.= " AND\n ";
				}
				if ( !empty($elem['to']) ) {
					$query .= "`" . $key . "` <= " .  $elem['to'];
				}
				$query .= "\n) \n";
			}
		}
	}

	if (!empty($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['orderBy'])) {
		$query .= "ORDER BY " . "`" . str_replace('.', '`.`', $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['orderBy']) . "`";
		$query .= " " . strtoupper($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder']);
	}
	if ( is_numeric($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage']) ) {
		$query .= "\nLIMIT ";
		$query .= $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentRow'] . ", ";
		$query .= $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage'];
	}

//echo '<pre>';
//var_dump($query);
//echo '</pre>';

	return $query;
}