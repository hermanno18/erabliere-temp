<?php
/**
 * Return the previous page number
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return int The previous page number
 *
 */
function dplu5_mysql_search_previousPage() {
	if ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] - 1 < 1) {
		return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'];
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] - 1;
}