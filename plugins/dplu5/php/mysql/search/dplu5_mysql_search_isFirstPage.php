<?php
/**
 * Tell if we are on the first page
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return boolean
 *
 */
function dplu5_mysql_search_isFirstPage() {
	if ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] === 1) {
		return true;
	}
	return false;
}