<?php
/**
 * Tell if the search session is already set
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return boolean
 *
 */
function dplu5_mysql_search_isSet() {
	return isset($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]);
}