<?php
/**
 * Delete the search session
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_delete() {
	unset($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]);
}