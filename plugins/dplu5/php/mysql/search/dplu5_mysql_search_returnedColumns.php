<?php
/**
 * Return the columns used in the search result
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param array $columns If this parameter is provided, it will change the columns returned for the next search
 *
 * @return array The list of columns
 *
 */
function dplu5_mysql_search_returnedColumns($columns = null) {
	if ( !is_null($columns) && is_array($columns) ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedColumns'] = $columns;
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['returnedColumns'];
}