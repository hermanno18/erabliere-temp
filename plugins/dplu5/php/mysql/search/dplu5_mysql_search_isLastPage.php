<?php
/**
 * Tell if we are on the last page
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return boolean
 *
 */
function dplu5_mysql_search_isLastPage() {
	if ($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] == $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['totalPages']) {
		return true;
	}
	return false;
}