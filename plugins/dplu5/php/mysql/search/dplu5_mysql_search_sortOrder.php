<?php
/**
 * Set and return the sort order
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param string $order Can be either 'asc' or 'desc', if this parmeter is provided, the sort order is changed to the now value
 *
 * @return string The sort order
 *
 */
function dplu5_mysql_search_sortOrder($order = null) {
	if ( !is_null($order) && ($order === 'asc' || $order === 'desc') ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder'] = $order;
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['sortOrder'];
}