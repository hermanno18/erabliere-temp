<?php
/**
 * Get the value of a search criteria by its name
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param string $name The name of the criteria
 *
 * @return string The value of the criteria
 *
 */
function dplu5_mysql_search_getCriteria($name) {
	return $_SESSION['mysql_search'][$_SERVER['PHP_SELF']]['criterias'][$name];
}