<?php
/**
 * Return the current page
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return int Number of the current page
 *
 */
function dplu5_mysql_search_currentPage($pageNumber = null) {
	if ( !is_null($pageNumber) ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'] = intval($pageNumber);
		dplu5_mysql_search_currentRow();
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['currentPage'];
}