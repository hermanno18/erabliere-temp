<?php
/**
 * Add a criteria to the search
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_addJoinCondition($name, $value) {
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['joinConditions'][$name]= $value;
}