<?php
/**
 * Update the search data with the request
 *
 * @version 1.1
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @return null
 *
 */
function dplu5_mysql_search_update() {

	// Get criteria
	foreach ($_REQUEST as $key => $element) {
		if (strpos($key, 'dplu5_mysql_search_criteria_') === 0) {
			$parts= explode('criteria_', $key);
			$name= end($parts);
			dplu5_mysql_search_addCriteria($name, $element);
		}
	}
	// Get between range for numbers and dates
	foreach ($_REQUEST as $key => $element) {
		if (strpos($key, 'dplu5_mysql_search_range_') === 0) {
			$keyParts= explode('range_', $key);

			$name= end($keyParts);
			$elementParts= explode('<=>', $element);
			$from = current($elementParts);
			$to = end($elementParts);

			dplu5_mysql_search_addRange($name, $from, $to);
		}
	}
	// Get keyword
	if (isset($_REQUEST['dplu5_mysql_search_keyword'])) {
		dplu5_mysql_search_keyword($_REQUEST['dplu5_mysql_search_keyword']);
	}

	// Change sort order
	if ( isset($_REQUEST['dplu5_mysql_search_orderBy']) && $_REQUEST['dplu5_mysql_search_orderBy'] === dplu5_mysql_search_orderBy() ) {
		dplu5_mysql_search_changeSortOrder();
	}

	// Get order by
	if (isset($_REQUEST['dplu5_mysql_search_orderBy'])) {
		dplu5_mysql_search_orderBy($_REQUEST['dplu5_mysql_search_orderBy']);
	}

	// Get current page
	if (isset($_REQUEST['dplu5_mysql_search_currentPage'])) {
		dplu5_mysql_search_currentPage($_REQUEST['dplu5_mysql_search_currentPage']);
	}
}