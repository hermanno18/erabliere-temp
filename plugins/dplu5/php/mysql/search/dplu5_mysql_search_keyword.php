<?php
/**
 * Set and return the keyword of the search
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param string $newKeyword This new keyword will be used for the next search
 *
 * @return string The keyword
 *
 */
function dplu5_mysql_search_keyword($newKeyword = null) {
	if ( !is_null($newKeyword) ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['keyword'] = strval($newKeyword);
		dplu5_mysql_search_currentPage(1);
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['keyword'];
}