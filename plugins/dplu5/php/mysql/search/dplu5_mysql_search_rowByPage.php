<?php
/**
 * Set and return the number of rows by page
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param int $number Number of rows by page, if this parameter is provided, it will be used for the next search
 *
 * @return string The sort order
 *
 */
function dplu5_mysql_search_rowByPage($number = null) {
	if ( !is_null($number) ) {
		$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage']= intval($number);
	}
	return $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage'];
}