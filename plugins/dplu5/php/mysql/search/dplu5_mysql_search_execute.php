<?php
/**
 * Execute the search in the database
 *
 * @package dplu5
 *
 * @category mysql_search
 *
 * @param $dbLink The database ressource handle
 *
 * @return array The search result
 *
 */
function dplu5_mysql_search_execute($dbLink) {

	// Update the search with the request
	dplu5_mysql_search_update();

	// Build the query according to the search data
	$sqlQuery = dplu5_mysql_search_buildQuery($dbLink);

	// Execute the query
	$query1Result = dplu5_mysql_query(
		$dbLink,
		$sqlQuery
	);

	// Get the number of rows in the result
	$query2Result = dplu5_mysql_query(
		$dbLink,
		"SELECT FOUND_ROWS()"
	);

	$output['foundRows']= $query2Result[0]['FOUND_ROWS()'];

	if ( is_int($_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage']) ) {
		$output['totalPages']= intval(ceil($output['foundRows'] / $_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['rowByPage']));
		if ($output['totalPages'] < 1) {
			$output['totalPages'] = 1;
		}
	} else {
		$output['totalPages'] = 1;
	}
	
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['foundRows']= $output['foundRows'];
	$_SESSION['dplu5_mysql_search'][$_SERVER['PHP_SELF']]['totalPages']= $output['totalPages'];
	$output['result']= $query1Result;

	return $output;
}