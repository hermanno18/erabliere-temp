<?php
/**
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @todo Add comments
 */

function dplu5_mysql_lastQuery($newQuery = null) {
	static $query = '';
	if ( null !== $newQuery ) {
		$query = $newQuery;
	}
	return $query;
}