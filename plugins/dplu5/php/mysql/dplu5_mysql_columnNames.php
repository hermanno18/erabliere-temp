<?php
/**
 * Retrieve all the column names of a table
 *
 * @package dplu5
 *
 * @category mysql
 *
 * @param ressource $dbLink link identifier
 * @param string $table name of the table
 *
 * @return array column names
 */

function dplu5_mysql_columnNames($dbLink, $table) {
	$sqlStr = "DESCRIBE %s";
	$sqlVal = array($table);
	$result = dplu5_mysql_query($dbLink, $sqlStr, $sqlVal);

	$columnNames = array();

	foreach ( $result as $elem) {
		$columnNames[] = $elem['Field'];
	}

	return $columnNames;
}