<?php
/**
 * Footer type 1
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2019-10-30
 *
 * @package dplu5
 *
 * @category tpl
 *
 * @param string $company The company name
 * @param array $opt Optional parameters
 * @param string $opt['devUrl'] The developer URL
 *
 * @return The HTML source
 *
 */

function dplu5_footerType1($company, $opt = array()) {

$opt['devUrl'] = isset($opt['devUrl']) ? $opt['devUrl'] : 'https://www.domaineplus.com';
$opt['lng'] = isset($opt['lng']) ? $opt['lng'] : 'fr';

ob_start();
?>
<div class="dplu5-footerType1">
	<div class="dplu5-footerType1-container">
		<div class="dplu5-footerType1-section">
			<div class="dplu5-footerType1-div">
				<div class="dplu5-footerType1-section">
					<div class="dplu5-footerType1-div">
						<span>&copy; <?=date('Y')?> <?=$company?></span>
					</div>
					<div class="dplu5-footerType1-div">
						<span class="dplu5-footerType1-txt-fr">Tous droits réservés.</span>
						<span class="dplu5-footerType1-txt-en">All right reserved.</span>
					</div>
				</div>
			</div>
			<div class="dplu5-footerType1-div">
				<div class="dplu5-footerType1-section">
					<div class="dplu5-footerType1-div">
						<div class="dplu5-footerType1-section">
							<div class="dplu5-footerType1-div">
								<span class="dplu5-footerType1-txt-fr">Conception & hébergement</span>
								<span class="dplu5-footerType1-txt-en">Web hosting & Design</span>
							</div>
							<div class="dplu5-footerType1-div">
								<a class="dplu5-footerType1-logo" href="<?=$opt['devUrl']?>" target="_blank"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

$out = preg_replace(
	'#<span class="dplu5-footerType1-txt-' . ($opt['lng'] === 'fr' ? 'en' : 'fr') . '">.*</span>#',
	'',
	ob_get_clean()
);

return $out;

}