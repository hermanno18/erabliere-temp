<?php
/**
 * Team card type 1
 *
 * @version 2020-10-19
 *
 * @param string $photo URL of the photo
 * @param string $name name of th team member
 * @param array $opt Optional parameters
 * @param string $opt['title'] Title of the team member
 * @param string $opt['email'] Email of the team member
 * @param string $opt['email-iconPath'] Path to the svg file
 * @param string $opt['phone'] Phone number of the team member
 * @param string $opt['phone-iconPath'] Path to the svg file
 *
 * @return The HTML source
 *
 */

function dplu5_teamCardType1($photo, $name, $opt = []) {

$opt['title'] = isset($opt['title']) ? $opt['title'] : null;
$opt['phone'] = isset($opt['phone']) ? $opt['phone'] : null;
$opt['phone-iconPath'] = isset($opt['phone-iconPath']) ? $opt['phone-iconPath'] : __DIR__ . '/svg/phone.svg';
$opt['email'] = isset($opt['email']) ? $opt['email'] : null;
$opt['email-iconPath'] = isset($opt['email-iconPath']) ? $opt['email-iconPath'] : __DIR__ . '/svg/email.svg';

ob_start();
?>
<div class="dplu5-teamCardType1">
	<div class="dplu5-teamCardType1-inner" style="background-image: url(<?=$photo?>)">
		<div class="dplu5-teamCardType1-text">
			<div class="dplu5-teamCardType1-text-line1"><?=$name?></div>
			<div class="dplu5-teamCardType1-text-line2"><?=$opt['title']?></div>
		</div>
		<div class="dplu5-teamCardType1-buttons">
			<a class="dplu5-teamCardType1-button dplu5-teamCardType1-button-phone" href="tel: <?=$opt['phone']?>">
				<div class="dplu5-teamCardType1-button-icon">
					<?php readfile($opt['phone-iconPath'])?>
				</div>
			</a>
			<a class="dplu5-teamCardType1-button dplu5-teamCardType1-button-email display-block width-40 height-40" href="mailto: <?=$opt['email']?>">
				<div class="dplu5-teamCardType1-button-icon">
					<?php readfile($opt['email-iconPath'])?>
				</div>
			</a>
		</div>
	</div>
</div>
<?php
return ob_get_clean();
}