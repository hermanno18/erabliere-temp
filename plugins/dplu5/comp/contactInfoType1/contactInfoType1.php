<?php
/**
 * Contact Info template
 *
 * @version 2020-10-22
 *
 * @param string $param
 *
 * @param string $param['address-title']
 * @param string $param['address-line1']
 * @param string $param['address-line2']
 * @param string $param['address-googleMapUrl']
 *
 * @param string $param['phone-title']
 * @param string $param['phone-1-label']
 * @param string $param['phone-1-number']
 * @param string $param['phone-2-label']
 * @param string $param['phone-2-number']
 *
 * @param string $param['email-title']
 * @param string $param['email-address1']
 * @param string $param['email-address2']
 *
 * @return The HTML source
 *
 */

function dplu5_contactInfoType1($param) {

$param['address-title'] = isset($param['address-title']) ? $param['address-title'] : 'Notre adresse';
$param['address-line1'] = isset($param['address-line1']) ? $param['address-line1'] : null;
$param['address-line2'] = isset($param['address-line2']) ? $param['address-line2'] : null;
$param['address-googleMapUrl'] = isset($param['address-googleMapUrl']) ? $param['address-googleMapUrl'] : null;
$param['address-iconPath'] = isset($param['address-iconPath']) ? $param['address-iconPath'] : __DIR__ . '/svg/mapmarker.svg';

$param['phone-title'] = isset($param['phone-title']) ? $param['phone-title'] : 'Par téléphone';
$param['phone-1-label'] = isset($param['phone-1-label']) ? $param['phone-1-label'] : null;
$param['phone-1-number'] = isset($param['phone-1-number']) ? $param['phone-1-number'] : null;
$param['phone-2-label'] = isset($param['phone-2-label']) ? $param['phone-2-label'] : null;
$param['phone-2-number'] = isset($param['phone-2-number']) ? $param['phone-2-number'] : null;
$param['phone-iconPath'] = isset($param['phone-iconPath']) ? $param['phone-iconPath'] : __DIR__ . '/svg/phone.svg';

$param['email-title'] = isset($param['email-title']) ? $param['email-title'] : 'Par courriel';
$param['email-address1'] = isset($param['email-address1']) ? $param['email-address1'] : null;
$param['email-address2'] = isset($param['email-address2']) ? $param['email-address2'] : null;
$param['email-iconPath'] = isset($param['email-iconPath']) ? $param['email-iconPath'] : __DIR__ . '/svg/email.svg';

ob_start();
?>
<div class="dplu5-contactInfoType1">
	<div class="dplu5-contactInfoType1-block dplu5-contactInfoType1-block1">
		<div class="dplu5-contactInfoType1-block-inner">
			<div class="dplu5-contactInfoType1-info dplu5-contactInfoType1-address">
				<div class="dplu5-contactInfoType1-info-icon dplu5-contactInfoType1-address-icon">
					<div class="dplu5-contactInfoType1-info-icon-svg"><?php readfile($param['address-iconPath'])?></div>
				</div>
				<div class="dplu5-contactInfoType1-info-text dplu5-contactInfoType1-address-text">
					<div class="dplu5-contactInfoType1-info-title">
						<?=(isset($param['address-title']) ? $param['address-title'] : '')?>
					</div>
					<a class="dplu5-contactInfoType1-address-content" href="<?=(isset($param['address-googleMapUrl']) ? $param['address-googleMapUrl'] : '')?>" target="_blank">
						<?=(isset($param['address-line1']) ? $param['address-line1'] : '')?>
						<br><?=(isset($param['address-line2']) ? $param['address-line2'] : '')?>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="dplu5-contactInfoType1-block dplu5-contactInfoType1-block1">
		<div class="dplu5-contactInfoType1-block-inner">
			<div class="dplu5-contactInfoType1-info dplu5-contactInfoType1-phone">
				<div class="dplu5-contactInfoType1-info-icon dplu5-contactInfoType1-phone-icon">
					<div class="dplu5-contactInfoType1-info-icon-svg"><?php readfile($param['phone-iconPath'])?></div>
				</div>
				<div class="dplu5-contactInfoType1-info-text dplu5-contactInfoType1-phone-text">
					<div class="dplu5-contactInfoType1-info-title">
						<?=(isset($param['phone-title']) ? $param['phone-title'] : '')?>
					</div>
					<div>
						<a href="tel: <?=(isset($param['phone1-number']) ? $param['phone1-number'] : '')?>"><?=(isset($param['phone1-label']) ? $param['phone1-label'] : '')?> <?=(isset($param['phone1-number']) ? $param['phone1-number'] : '')?></a>
						<?php if ( isset($param['phone2-number']) ) { ?>
						<br><a href="tel: <?=(isset($param['phone2-number']) ? $param['phone2-number'] : '')?>"><?=(isset($param['phone2-label']) ? $param['phone2-label'] : '')?> <?=(isset($param['phone2-number']) ? $param['phone2-number'] : '')?></a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="dplu5-contactInfoType1-block dplu5-contactInfoType1-block1">
		<div class="dplu5-contactInfoType1-block-inner">
			<div class="dplu5-contactInfoType1-info dplu5-contactInfoType1-email">
				<div class="dplu5-contactInfoType1-info-icon dplu5-contactInfoType1-email-icon">
					<div class="dplu5-contactInfoType1-info-icon-svg"><?php readfile($param['email-iconPath'])?></div>
				</div>
				<div class="dplu5-contactInfoType1-info-text dplu5-contactInfoType1-email-text">
					<div class="dplu5-contactInfoType1-info-title">
						<?=(isset($param['email-title']) ? $param['email-title'] : '')?>
					</div>
					<div>
						<a href="mailto: <?=(isset($param['email-address1']) ? $param['email-address1'] : '')?>">
							<?=(isset($param['email-address1']) ? $param['email-address1'] : '')?>
						</a><br>
						<?php if ( isset($param['email-address2']) ) { ?>
						<a href="mailto: <?=(isset($param['email-address2']) ? $param['email-address2'] : '')?>">
							<?=(isset($param['email-address2']) ? $param['email-address2'] : '')?>
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
return ob_get_clean();
}