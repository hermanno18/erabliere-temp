<?php
/**
 * Slider type 1
 *
 * @version 2020-10-14
 *
 * @uses The plugin Slick
 *
 * @param array $slidesBgImg An array of URLs
 * @param array $opt Optional parameters
 * @param array $opt['slidesContent'] An array of html contents
 * @param string $opt['class'] CSS Class
 *
 * @return The HTML source
 *
 */

function dplu5_sliderType1($slideBgImg, $opt = array()) {

$opt['slidesContent'] = isset($opt['slidesContent']) ? $opt['slidesContent'] : '';
$opt['class'] = isset($opt['class']) ? $opt['class'] : '';

ob_start();
?>
<div class="dplu5-sliderType1 <?=$opt['class']?>">
	<div class="dplu5-sliderType1-slides">
		<?php foreach ( $slideBgImg as $key => $val ) { ?>
		<div class="dplu5-sliderType1-slide js-lazyload" style="background-image: url(<?=$val?>)">
			<?php if ( isset($opt['slidesContent'][$key] ) ) { ?>
			<div class="dplu5-sliderType1-slide-content-table">
				<div class="dplu5-sliderType1-slide-content-cell">
					<div class="dplu5-sliderType1-slide-content-inner">
						<?=$opt['slidesContent'][$key]?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
	<button type="button" class="slick-prev slick-arrow dplu5-sliderType1-prev"></button>
	<button type="button" class="slick-next slick-arrow dplu5-sliderType1-next"></button>
</div>
<?php
return ob_get_clean();

}