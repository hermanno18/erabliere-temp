$('.dplu5-sliderType1-slides').not('.slick-initialized').slick({
	rows: 0,
	autoplay: true,
	autoplaySpeed: 5000,
	speed: 1000,
	infinite: true,
	pauseOnFocus: true,
	pauseOnHover: true,
	fade: true,
	prevArrow: $('.dplu5-sliderType1-prev'),
	nextArrow: $('.dplu5-sliderType1-next')
});