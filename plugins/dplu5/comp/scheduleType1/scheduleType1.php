<?php
/**
 * Schedule type 1
 *
 * @version 2020-10-22
 *
 * @param array $slidesBgImg An array of URLs
 * @param array $opt Optional parameters
 * @param array $opt['slidesContent'] An array of html contents
 *
 * @return The HTML source
 *
 */

function dplu5_scheduleType1($param) {

$param['slidesContent'] = isset($param['slidesContent']) ? $param['slidesContent'] : '';

ob_start();
?>
<div class="dplu5-sliderType1">
	<div class="dplu5-sliderType1-slider">
		<?php foreach ( $slideBgImg as $key => $val ) { ?>
		<div class="dplu5-sliderType1-slide" style="background-image: url(<?=$val?>)">
			<?php if ( isset($opt['slidesContent'][$key] ) ) { ?>
			<div class="dplu5-sliderType1-slide-content-table">
				<div class="dplu5-sliderType1-slide-content-cell">
					<div class="dplu5-sliderType1-slide-content-inner">
						<span><?=$opt['slidesContent'][$key]?></span>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
	<button type="button" class="slick-prev slick-arrow dplu5-sliderType1-prev"><?php readfile(ROOTPATH . 'plugins/dplu5/comp/sliderType1/svg/chevron-left.svg')?></button>
	<button type="button" class="slick-next slick-arrow dplu5-sliderType1-next"><?php readfile(ROOTPATH . 'plugins/dplu5/comp/sliderType1/svg/chevron-left.svg')?></button>
</div>
<?php
return ob_get_clean();

}