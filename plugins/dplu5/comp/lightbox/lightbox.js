/**
 * Lightbox
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2021-03-03
 *
 */

function dplu5_lightbox_open(selector, url, callback) {
		$(selector).css({
			'visibility': 'visible',
			'opacity': 1
		});
		$(selector).load(url, callback);
}

function dplu5_lightbox_close(selector) {
	$(selector).css({
		'visibility': 'hidden',
		'opacity': 0
	});
	$(selector).empty();
}


