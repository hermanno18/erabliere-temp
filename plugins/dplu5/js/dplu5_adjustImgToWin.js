/**
 * Images inside the selector are adjusted to fit the window
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2021-03-03
 *
 * @param {string} selector
 *
 * @return null
 *
 */

function dplu5_adjustImgToWin(selector) {

	var maxWidth = $(window).width() - 30;
	var maxHeight = $(window).height() - 30;

	$(selector + ' img').each(function() {

		$(this).on('load', function() {

			if ( $(window).width() < $(window).height() ) {
				if ( $(this)[0].naturalWidth > maxWidth ) {
					$(this).css({
						'width': maxWidth+'px',
						'height': 'auto'
					});
				}
			} else {
				if ( $(this)[0].naturalHeight > maxHeight ) {
					$(this).css({
						'height': maxHeight+'px',
						'width': 'auto'
					});
				}
			}
		});
	});
}