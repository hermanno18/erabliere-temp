/**
 * Selector will fade in after all the images inside are loaded
 *
 * @author Alexandre Perron <alex@domaineplus.com>
 *
 * @version 2021-03-03
 *
 * @param {string} selector
 *
 * @return null
 *
 */

function dplu5_fadeInImg(selector) {
	$(selector + ' img').each(function() {
		if ( !$(this).prop('complete') ) {
			$(this).on('load', function() {
				$(this).closest(selector).css({opacity: 1});
			});
		} else {
			$(this).closest(selector).css({opacity: 1});
		}
	});
}