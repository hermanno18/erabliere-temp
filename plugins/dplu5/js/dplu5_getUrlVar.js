function dplu5_getUrlVar(name) {
	var url = window.location.search.substring(1);
	var variables = url.split('&');
	for (var i = 0; i < variables.length; i++) {
		var varName = variables[i].split('=');
		if (varName[0] === name) {
			return varName[1];
		}
	}
}