$(function() {

	// Return to the same scroll position on submit
	var ypos = parseInt(Cookies.get('ypos'));
	if ( ypos ) $(document).scrollTop(ypos);
	$(document).scroll(function () {
		var ypos = $(document).scrollTop();
		Cookies.set('ypos', ypos);
	});

	// Initiate the tooltipster plugin and assign it to the class "tooltip-error"
	$('.tooltip-error').tooltipster({
		animation: 'grow',
		trigger: 'click'
	});

	// If the tooltip-error class exist in the dom, the tooltip will show on the first element with the class
	// For an unknown reason, we need the setTimeout to prevent the tooltip from closing
	setTimeout(function() {
		$('.tooltip-error').eq(0).focus().trigger('click');
	}, 750);


	$('.submit').click(function(){

		$(this).css({
			'background-color': '#acabab',
			'cursor': 'not-allowed'
		});
		$(this).prop("disabled",true);
		$(this).find('span').html('En cours...');
		$(this).parents('form:first').submit();
	});
	
});