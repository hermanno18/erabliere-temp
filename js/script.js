$(function () {

	$('.js-lazyload').lazy();

	// Hide header on scroll
	var lastScrollPos = 0;
	$(window).scroll(function () {
		if ($(this).scrollTop() > $('.js-hideOnScroll').outerHeight()) {
			var currScrollPos = $(this).scrollTop();
			if (currScrollPos > lastScrollPos) {
				$('.js-hideOnScroll').css({ 'top': -($('.js-hideOnScroll').outerHeight()) });
			} else {
				$('.js-hideOnScroll').css({ 'top': 0 });
			}
			lastScrollPos = currScrollPos;
		}
	});

	$('.js-toggle-nav').click(function () {
		if ($('.nav-primary').hasClass('b1-position-left-minus300')) {
			$('.nav-primary').removeClass('b1-position-left-minus300');
			$('.nav-mobile').addClass('b1-position-left-300');
			$('.main').addClass('b1-position-left-300');
			$('.main .overlay').addClass('b1-visibility-visible b1-opacity-1');
		} else {
			$('.nav-primary').addClass('b1-position-left-minus300');
			$('.nav-mobile').removeClass('b1-position-left-300');
			$('.main').removeClass('b1-position-left-300');
			$('.main .overlay').removeClass('b1-visibility-visible b1-opacity-1');
		}
	});

	$('.dplu5-lightbox-js-open-1').click(function () {
		dplu5_lightbox_open('.dplu5-lightbox', $(this).data('page'), function () {

			dplu5_adjustImgToWin('.dplu5-lightbox');

			$('.gallery-slider').slick({
				speed: 1000,
				arrows: true,
				rows: 0
			});

			dplu5_fadeInImg('.js-fadeIn');

			$('.dplu5-lightbox-canvas, .slick-arrow').click(function (e) {
				e.stopPropagation();
			});
		});
	});

	$('.dplu5-lightbox-js-open-2').click(function () {
		dplu5_lightbox_open('.dplu5-lightbox', '/fr/youtube', function () {

			$('.youtube-slider').slick({
				speed: 1000,
				arrows: true,
				rows: 0
			});

			$('.dplu5-lightbox-canvas, .slick-arrow').click(function (e) {
				e.stopPropagation();
			});
		});
	});

	$('.dplu5-lightbox-js-close').click(function () {
		dplu5_lightbox_close('.dplu5-lightbox');
	});

	$(window).on('resize orientationchange', function () {
		dplu5_adjustImgToWin('.dplu5-lightbox');
	});

	$('.js-open360').click(function () {
		window.open('/360/');
	});

	$('.js-open360-2').click(function () {
		window.open('/360-2/index.htm');
	});

	$('.editText-js-open').on('click', function (e) {
		e.stopPropagation();
		$(this).parent().find('.editText').addClass('editText-opened');
	});

	$(document).on('click', function (event) {
		if ($(event.target).closest(".editText.editText-opened").length === 0) {
			$('.editText-opened').find('input[type="number"]').each(function () {

				let editText_content = $(this).parent().parent().find('.editText-content');
				let currPrice = $(this).prev('input[type="hidden"]').val();
				let newPrice = $(this).val();

				editText_content.html(editText_content.html().replace(currPrice.replace('.', ','), newPrice.replace('.', ',')));
				$(this).prev('input[type="hidden"]').val(newPrice);

				let data = {};
				data[$(this).attr('name')] = $(this).val();

				$.post("ajax?a=updatePrice", { data }, (response) => {
					// response from PHP back-end
				});
			});
			$('.editText-opened').removeClass('editText-opened');
		}
	});
	$('.editOpeningHours-js-open').on('click', function (e) {
		e.stopPropagation();
		$(this).parent().find('.editOpeningHours').addClass('editOpeningHours-opened');
	});

	$(document).on('click', function (event) {
		if ($(event.target).closest(".editOpeningHours.editOpeningHours-opened").length === 0) {
			$('.editOpeningHours-opened').find('input[type="text"]').each(function () {

				let schedule = $(this).prev('input[type="hidden"]').val();

				let editOpeningHours_content = $(this).parent().parent().find('.editOpeningHours-content');
				editOpeningHours_content.html($(this).val());

				let data = {};
				data[$(this).attr('name')] = $(this).val();

				$.post('ajax?a=updateOpeningHours&s=' + schedule, { data }, (response) => {
					// response from PHP back-end
				});
			});
			$('.editOpeningHours-opened').removeClass('editOpeningHours-opened');
		}
	});

	$('.Wedding-menus-tab .slides').slick({
		speed: 500,
		draggable: false,
		adaptiveHeight: true,
		autoPlay: false,
		infinite: false,
		arrows: false,
	});
	$('button[data-slide]').click(function (e) {
		var slideno = $(this).data('slide');
		$('.Wedding-menus-tab .slides').slick('slickGoTo', slideno - 1);
		$('button[data-slide]').addClass('background-color-darkgrey');
		$('button[data-slide]').removeClass('color-darkgrey');
		this.classList.remove('background-color-darkgrey');
		this.classList.add('color-darkgrey');
	});

	$('.testimmonials-slider').slick({
		speed: 1000,
		autoPlay: true,
		infinite: true,
		arrows: false,
	});

	$("#wedding-form").submit(function (event) {

		event.preventDefault();

		let nom = $("#wedding-form #name").val();
		let prenom = $("#wedding-form #prenom").val();
		let email = $("#wedding-form #email").val();
		let phone = $("#wedding-form #phone").val();
		let date = $("#wedding-form #date").val();
		let nb_invites = $("#wedding-form #nb_invites").val();
		let message = $("#wedding-form #message").val();

		const data = { nom, prenom, email, phone, date, nb_invites, message }
		$("#wedding-form-response").html(`
		<svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><style>.spinner_GuJz{transform-origin:center;animation:spinner_STY6 1.5s linear infinite; color: #fff; stroke: #fff; fill: #fff}@keyframes spinner_STY6{100%{transform:rotate(360deg)}}</style><g class="spinner_GuJz"><circle cx="3" cy="12" r="2"/><circle cx="21" cy="12" r="2"/><circle cx="12" cy="21" r="2"/><circle cx="12" cy="3" r="2"/><circle cx="5.64" cy="5.64" r="2"/><circle cx="18.36" cy="18.36" r="2"/><circle cx="5.64" cy="18.36" r="2"/><circle cx="18.36" cy="5.64" r="2"/></g></svg>
		`);

		$.post("ajax?wedding_contact=true", data, (response) => {
			resp = JSON.parse(response)
			$("#wedding-form-response").html(`
				<span style="color: ${resp.status == 'success' ? '': 'red'} "> ${resp.message} </span>
			`);
		});

	});

});

