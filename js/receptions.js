function adjustMenuHeight() {

	$('[id^="receptionMenu_"]').css('height', 'auto');

	var elemParts = ['title','content','price'];

	elemParts.forEach(function(part) {

		var rowCnt = 1;
		var rowPos = $('.receptionMenu_' + part).first().offset().top;
		var newRowPos = 0;
		var maxHeight = [];

		maxHeight[rowCnt - 1] = $('.receptionMenu_' + part).first().height();

		$('.receptionMenu_' + part).first().addClass('row_' + part + '_' + rowCnt);

		$('.receptionMenu_' + part).each(function (i) {

			if ( i !== 0 ) {
				newRowPos = $(this).offset().top;

				if ( newRowPos > rowPos ) {
					rowCnt++;
					rowPos = newRowPos;
					maxHeight[rowCnt - 1] = $(this).height();
				}

				if ( $(this).height() > maxHeight[rowCnt - 1] ) {
					maxHeight[rowCnt - 1] = $(this).height();
				}

				$(this).addClass('row_' + part + '_' + rowCnt);
			}

		});

		maxHeight.forEach(function(rowHeight, i) {
			$('.row_' + part + '_' + (i + 1 )).height(rowHeight);
		});

	});
}

$(function() {
	adjustMenuHeight();

	$(window).on('resize orientationchange', function() {
		adjustMenuHeight();
	});
});