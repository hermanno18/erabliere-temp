<?php
/**
 * Retrieve the meta data from the DB
 *
 * @version 2020-09-14
 *
 * @use dplu5_util_lng
 *
 * @param $pageName string The page identifier
 * @param $metaKey string The meta identifier
 *
 * @return array Image data
 */

function mod_pageMeta_get($pageName, $metaKey) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$pageName][$metaKey][$lng]) ) {
		$cache[$pageName][$metaKey][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_pageMeta',
			[
				'page_name' => $pageName,
				'meta_key' => $metaKey,
				'lng' => $lng
			]
		)['meta_value'];

		if ( $cache[$pageName][$metaKey][$lng] === false ) {
			$cache[$pageName][$metaKey][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_pageMeta',
				[
					'page_name' => $pageName,
					'meta_key' => $metaKey,
					'lng' => getSetting('system', 'defaultLng')
				]
			);
		}
	}

	return $cache[$pageName][$metaKey][$lng];
}