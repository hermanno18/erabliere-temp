<?php
/**
 * Retrieve the contact data from the DB
 *
 * @version 2020-09-10
 *
 * @use dplu5_util_lng
 *
 * @param $name string The contact identifier
 *
 * @return array Image data
 */

function mod_contact_get($name) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_contact',
			['name' => $name, 'lng' => $lng]
		);

		if ( $cache[$name][$lng] === false ) {
			$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_contact',
				['name' => $name, 'lng' => getSetting('system', 'defaultLng')]
			);
		}
	}

	return $cache[$name][$lng];
}