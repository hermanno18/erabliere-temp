<?php
/**
 * Retrieve the text bloc data from the DB
 *
 * @version 2020-04-26
 *
 * @use dplu5_util_lng
 *
 * @param $name string The text block identifier
 * @param $lng string The language
 *
 * @return string The text block
 */

function mod_text_get($name, $lng = null) {

	static $cache = [];

	$lng = is_string($lng) ? $lng : dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_text',
			['name' => $name, 'lng' => $lng]
		)['value'];

		if ( $cache[$name][$lng] === null ) {
			$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_text',
				['name' => $name, 'lng' => getSetting('system', 'defaultLng')]
			)['value'];
		}
	}

	return $cache[$name][$lng];
}