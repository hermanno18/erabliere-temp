<?php
/**
 * Retrieve the data from the DB
 *
 * @version 2021-03-10
 *
 * @use dplu5_util_lng
 *
 * @param $name string The product identifier
 *
 * @return array Product data
 */

function mod_get($moduleName, $elemName, $colName) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$moduleName][$elemName][$colName][$lng]) ) {
		$cache[$moduleName][$elemName][$colName][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_' . $moduleName,
			['name' => $elemName, 'lng' => $lng]
		)[$colName];

		if ( $cache[$moduleName][$elemName][$colName][$lng] === false ) {
			$cache[$moduleName][$elemName][$colName][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_' . $moduleName,
				['name' => $elemName, 'lng' => getSetting('system', 'defaultLng')]
			)[$colName];
		}

		// Format prices
		if ( preg_match('/^[0-9]{1,}[\.\,]{1}[0-9]{2}$/', $cache[$moduleName][$elemName][$colName][$lng]) ) {
			$cache[$moduleName][$elemName][$colName][$lng] = numfmt_format_currency(numfmt_create( $lng . '_CA', NumberFormatter::CURRENCY ), $cache[$moduleName][$elemName][$colName][$lng], 'CAD');
		}
	}

	return $cache[$moduleName][$elemName][$colName][$lng];
}