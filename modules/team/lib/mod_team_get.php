<?php
/**
 * Retrieve the team member
 *
 * @version 2020-10-06
 *
 * @use dplu5_util_lng
 *
 * @param $name string The team member identifier
 *
 * @return array Image data
 */

function mod_team_get($name) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_team',
			['name' => $name, 'lng' => $lng]
		);
	}

	return $cache[$name][$lng];
}