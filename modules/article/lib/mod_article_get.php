<?php
/**
 * Retrieve the article data from the DB
 *
 * @version 2020-09-24
 *
 * @use dplu5_util_lng
 *
 * @param $name string The article identifier
 *
 * @return array Image data
 */

function mod_article_get($name) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_article',
			['name' => $name, 'lng' => $lng]
		);
	}

	return $cache[$name][$lng];
}