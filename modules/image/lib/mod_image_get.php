<?php
/**
 * Retrieve the image data from the DB
 *
 * @version 2020-09-14
 *
 * @use dplu5_util_lng
 *
 * @param $name string The image identifier
 * @param $col string The column name (optional)
 *
 * @return array Image data
 */

function mod_image_get($name, $col = null) {

	static $cache = [];

	$lng = dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_image',
			['name' => $name, 'lng' => $lng]
		);

		if ( $cache[$name][$lng] === false ) {
			$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_image',
				['name' => $name, 'lng' => getSetting('system', 'defaultLng')]
			);
		}
	}

	if ( $col !== null ) {
		if ( $col === 'src' ) {
			return $cache[$name][$lng]['src'] . '?v=' . getSetting('system', 'lastUpdate');
		} else {
			return $cache[$name][$lng][$col];
		}
	}

	return $cache[$name][$lng];
}