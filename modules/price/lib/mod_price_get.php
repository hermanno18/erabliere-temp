<?php
/**
 * Retrieve a price from the DB
 *
 * @version 2021-03-12
 *
 * @use dplu5_util_lng
 *
 * @param $name string The price identifier
 * @param $raw bolean Do not format
 *
 * @return string The price
 */

function mod_price_get($name, $raw = false) {

	static $cache = [];

	if ( !isset($cache[$name]) ) {
		$cache[$name] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_price',
			['name' => $name]
		)['price'];
	}

	if ( !$raw ) {
		return dplu5_format_price($cache[$name], dplu5_util_lng() . '_CA', 'CAD');
	}

	return $cache[$name];
}