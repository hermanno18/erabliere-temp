<?php
/**
 * Retrieve the link data from the DB
 *
 * @version 2020-09-26
 *
 * @use dplu5_util_lng
 *
 * @param $name string The link identifier
 * @param $lng string The language
 *
 * @return array Image data
 */

function mod_link_get($name, $lng = null) {

	static $cache = [];

	$lng = is_string($lng) ? $lng : dplu5_util_lng();

	if ( !isset($cache[$name][$lng]) ) {
		$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
			dbLink(),
			'module_link',
			['name' => $name, 'lng' => $lng]
		);

		if ( $cache[$name][$lng] === false ) {
			$cache[$name][$lng] = dplu5_mysql_simple_selectOne(
				dbLink(),
				'module_link',
				['name' => $name, 'lng' => getSetting('system', 'defaultLng')]
			);
		}
	}

	return $cache[$name][$lng];
}