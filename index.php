<?php
/**
 * Dispatcher
 *
 * PHP >= 7
 *
 * @version 2021-02-16
 *
 */
require 'init.php';

// Redirect old URI
$redirection = dplu5_mysql_simple_selectOne(dbLink(), 'module_redirection', ['oldUri' =>  $_SERVER['REQUEST_URI']]);

if ( isset($redirection['newUri']) ) {
	dplu5_url_redirect301($redirection['newUri']);
}

if ( !isset($_GET['lng']) || empty($_GET['lng']) ) {
	dplu5_url_redirect('/' . getSetting('system', 'defaultLng') . '/' . getSetting('system', 'defaultPage'));
}
if ( (isset($_GET['lng']) && !empty($_GET['lng'])) && (!isset($_GET['page']) || empty($_GET['page'])) ) {
	dplu5_url_redirect('/' . $_GET['lng'] . '/' . getSetting('system', 'defaultPage'));
}

$page = isset($_GET['page'])  && !empty($_GET['page']) ? $_GET['page'] : getSetting('system', 'defaultPage');

if ( file_exists(ROOTPATH . 'lib/page/page_' . dplu5_str_hyphen2camel($page) . '.php') ) {
	$pageFunction = 'page_' . dplu5_str_hyphen2camel($page);
} elseif ( file_exists(ROOTPATH . 'lib/page/page_' . getSetting('pageAlias', $page) . '.php') ) {
	$pageFunction = 'page_' . getSetting('pageAlias', $page);
} else {
	$pageFunction = 'page_notfound';
}

if (isset($_GET['gestion'])) {

	$_SESSION['isLoggedIn'] = isset($_SESSION['isLoggedIn']) ? $_SESSION['isLoggedIn'] : false;

	if ( isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_USER']) ) {
		foreach ( $EDIT_USERS as $user ) {
			if ( $_SERVER['PHP_AUTH_USER'] === $user['usr'] && $_SERVER['PHP_AUTH_PW'] === $user['pwd'] ) {
				$_SESSION['isLoggedIn'] = true;
			}
		}
	}

	if ( !$_SESSION['isLoggedIn'] ) {
		header('WWW-Authenticate: Basic realm="My Realm"');
		header('HTTP/1.0 401 Unauthorized');
		die ("Not authorized");
	}
}

$pageFunction();